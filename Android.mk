LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_CFLAGS := $(CFG_CFLAGS)
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../msp_base/common/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../msp_base/mpi/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../msp_base/ha_codec/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../msp/graphics/fb/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../msp/component/hiflash/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../common
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../msp_base/ecs/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../../../packages/apps/SettingTV/jni
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../msp/ha_codec/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../msp_base/mpi/api/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../msp_base/mpi/drv/include
LOCAL_SHARED_LIBRARIES :=libsample_common libhi_common libhi_mpi libc libhi_ecs libtde liblog
LOCAL_MODULE:=u4t

LOCAL_SRC_FILES := sw_fb_handle.c
LOCAL_SRC_FILES += sw_fb_mm.c
LOCAL_SRC_FILES += sw_fb_surface.c
LOCAL_SRC_FILES += dvb_his_fe.c
LOCAL_SRC_FILES += config.c
LOCAL_SRC_FILES += storage.c
LOCAL_SRC_FILES += ui.c
LOCAL_SRC_FILES += ir.c
LOCAL_SRC_FILES += smartcard.c
LOCAL_SRC_FILES += channel.c
LOCAL_SRC_FILES += usb.c
LOCAL_SRC_FILES += ip.c
LOCAL_SRC_FILES += front_panel.c
LOCAL_SRC_FILES += serial.c
LOCAL_SRC_FILES += main.c
#LOCAL_SRC_FILES += $(wildcard $(LOCAL_PATH)/client/*.c)
LOCAL_SRC_FILES += client/fast_crc.c
LOCAL_SRC_FILES += des/des.c
LOCAL_SRC_FILES += client/command.c
LOCAL_SRC_FILES += client/client.c
LOCAL_SRC_FILES += ezxml/ezxml.c
LOCAL_SRC_FILES += emmc_check.c
LOCAL_SRC_FILES += getline.c

LOCAL_MODULE_TAGS := eng
include $(BUILD_EXECUTABLE)

