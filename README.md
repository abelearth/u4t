**u4t** is the test program for U4 Android STB.

**Main features**

1. Connectors test (IP, RS232, USB, SCI, HDMI, RCA, Coaxial S/PDIF)
2. Tuner test (0/13/18V, 22KHz, IF IN/OUT)
3. Front panel test (Display with two brightness, Buttons, power light, IR)
4. Channel playing
5. Program ID, MAC, and HDCP key
6. Import eeprom data.
