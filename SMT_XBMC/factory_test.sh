#!/system/bin/sh
#set -x

echo "======================="
echo "      U4 Test Tool     "
echo "======================="

export LD_LIBRARY_PATH=/vendor/lib:/system/lib

SRC_PATH="unexistdir"
RUN_PATH="/var/SMT_XBMC"

if [ -f /var/SMT_XBMC/factory_test.sh ]; then
	SRC_PATH="/var/SMT_XBMC" 
else
	if [ -f /sdcard/SMT_XBMC/factory_test.sh ]; then
		SRC_PATH="/sdcard/SMT_XBMC"
	fi
fi

echo $SRC_PATH 1>>/var/.debug 2>&1

if [ ! -d $SRC_PATH ]; then
exit 1;
fi


#Copy program resource to var
if [ $SRC_PATH != $RUN_PATH ]; then
mkdir -p /var/SMT_XBMC
cp -rf $SRC_PATH/* $RUN_PATH/ 1>>/var/.debug 2>&1
cp -rf $SRC_PATH/Image/* $RUN_PATH/Image/ 1>>/var/.debug 2>&1
cp -rf $SRC_PATH/Driver/* $RUN_PATH/Driver/ 1>>/var/.debug 2>&1
fi

$RUN_PATH/u4t

