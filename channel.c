#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <pthread.h>
#include <assert.h>
#include <linux/fb.h>
#include <fcntl.h>

#include "hi_unf_ecs.h"

#include "hi_adp.h"
#include "hi_adp_audio.h"
#include "hi_adp_hdmi.h"
#include "hi_adp_boardcfg.h"
#include "hi_adp_mpi.h"
#include "hi_adp_tuner.h"
#include "hi_adp_pvr.h"

#include "hifb.h"
#include "hi_unf_disp.h"
#include "hi_adp_data.h"
#include "hi_unf_ecs.h"

#include "dvb_his_fe.h"

#include "HA.AUDIO.MP3.decode.h"
#include "HA.AUDIO.PCM.decode.h"
#include "HA.AUDIO.MP2.decode.h"
#include "HA.AUDIO.AAC.decode.h"
#include "HA.AUDIO.AMRNB.codec.h"
#include "HA.AUDIO.AMRWB.codec.h"
#include "HA.AUDIO.AC3PASSTHROUGH.decode.h"
#include "HA.AUDIO.DRA.decode.h"
#include "HA.AUDIO.BLURAYLPCM.decode.h"
#include "HA.AUDIO.DTSHD.decode.h"
#include "HA.AUDIO.TRUEHDPASSTHROUGH.decode.h"
#include "HA.AUDIO.COOK.decode.h"
#include "HA.AUDIO.DTSPASSTHROUGH.decode.h"
#include "HA.AUDIO.WMA9STD.decode.h"
#include "HA.AUDIO.DOLBYPLUS.decode.h"
#include "HA.AUDIO.FFMPEG_DECODE.decode.h"

#include "sw_debug.h"
#include "config.h"
#include "sw_fb_handle.h"
#include "ui.h"
#include "storage.h"
#include "channel.h"

static unsigned int g_ch_num = 0;
HI_U8 u8DecOpenBuf[1024];

#if defined (DOLBYPLUS_HACODEC_SUPPORT)
DOLBYPLUS_STREAM_INFO_S g_stDDpStreamInfo;
/*dolby Dual Mono type control*/
HI_U32  g_u32DolbyAcmod = 0;
HI_BOOL g_bDrawChnBar = HI_TRUE;
#endif

int g_22khzTone = 0;	// 0=off, 1=on
int g_lnb = 0;		// 0=off, 1=13V, 2=18V
int g_antenna_power = 0;		// 0=off, 1=on

static HI_UNF_ENC_FMT_E s_enDefaultFmt = HI_UNF_ENC_FMT_1080i_50;
static HI_HANDLE phWin;
static HI_HANDLE hAvplay;

int get_channel_info(int num, CHANNEL_INFO* ch)
{
	CHANNEL_INFO* c;

	if(num >= g_factory_conf.channel_db.num){
		return -1;
	}

	*ch = g_factory_conf.channel_db.info[num];

	return 0;
}


HI_S32 dev_init()
{
	HI_S32 s32Ret = 0;
	HI_UNF_TUNER_ATTR_S stTunerAttr;
	HI_UNF_DMX_PORT_ATTR_S PortAttr;

	/*sys init */
	HI_SYS_Init();
	HI_SYS_PreAV(NULL);

	/*sound init */
	s32Ret = HIADP_Snd_Init();
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HIADP_Snd_Init failed.\n");
		return s32Ret;
	}

	/*display init */
	s32Ret = HIADP_Disp_Init(s_enDefaultFmt);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HIADP_Disp_DeInit failed.\n");
		return s32Ret;
	}

	/*vo init */
	s32Ret = HIADP_VO_Init(HI_UNF_VO_DEV_MODE_NORMAL);
	s32Ret |= HIADP_VO_CreatWin(HI_NULL, &phWin);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HIADP_VO_Init failed.\n");
		HIADP_VO_DeInit();
		return s32Ret;
	}
	return HI_SUCCESS;
}

HI_S32 dev_deinit()
{
	HI_S32 s32Ret = 0;
	HI_UNF_SND_INTERFACE_S stSndIntf;

	s32Ret = HI_UNF_VO_DestroyWindow(phWin);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_VO_DestroyWindow failed.\n");
		return s32Ret;
	}

	s32Ret = HI_UNF_VO_Close(HI_UNF_VO_HD0);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_VO_Close failed.\n");
		return s32Ret;
	}

	s32Ret = HI_UNF_VO_DeInit();
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_VO_DeInit failed.\n");
		return s32Ret;
	}

	s32Ret = HI_UNF_DISP_Close(HI_UNF_DISP_SD0);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_DISP_Close failed.\n");
		return s32Ret;
	}

	s32Ret = HI_UNF_DISP_Close(HI_UNF_DISP_HD0);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_DISP_Close failed.\n");
		return s32Ret;
	}

	s32Ret = HI_UNF_DISP_Detach(HI_UNF_DISP_SD0, HI_UNF_DISP_HD0);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_DISP_Detach failed.\n");
		return s32Ret;
	}

	s32Ret = HI_UNF_DISP_DeInit();
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_DISP_DeInit failed.\n");
		return s32Ret;
	}

	/*aaa hdmiDeInit(); */

	stSndIntf.enInterface = HI_UNF_SND_INTERFACE_I2S;
	stSndIntf.bEnable = HI_FALSE;
	s32Ret = HI_UNF_SND_SetInterface(HI_UNF_SND_0, &stSndIntf);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_SND_SetInterface failed.\n");
		return s32Ret;
	}

	s32Ret = HI_UNF_SND_Close(HI_UNF_SND_0);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_SND_Close failed.\n");
		return s32Ret;
	}

	s32Ret = HI_UNF_SND_DeInit();
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_SND_DeInit failed.\n");
		return s32Ret;
	}

	HI_SYS_DeInit();

	return HI_SUCCESS;
}

HI_S32 DmxInit(HI_VOID)
{
	HI_S32 Ret;

	Ret = HI_UNF_DMX_Init();
	if (Ret != HI_SUCCESS) {
		SW_LDBG("call HI_UNF_DMX_Init failed.\n");
		return Ret;
	}

	Ret = HI_UNF_DMX_AttachTSPort(PVR_DMX_ID_LIVE, 1);
	//Ret = HI_UNF_DMX_AttachTSPort(PVR_DMX_ID_REC, PVR_DMX_PORT_ID_DVB);
	if (Ret != HI_SUCCESS) {
		SW_LDBG("call HI_UNF_DMX_AttachTSPort for REC failed.\n");
		HI_UNF_DMX_DeInit();
		return Ret;
	}

	return HI_SUCCESS;
}

HI_S32 DmxDeInit(HI_VOID)
{
	HI_S32 Ret;

	Ret = HI_UNF_DMX_DetachTSPort(PVR_DMX_ID_LIVE);
	if (Ret != HI_SUCCESS) {
		SW_ERR("call HI_UNF_DMX_DetachTSPort failed.\n");
		return Ret;
	}

	Ret = HI_UNF_DMX_DeInit();
	if (Ret != HI_SUCCESS) {
		SW_ERR("call HI_UNF_DMX_DeInit failed.\n");
		return Ret;
	}

	return HI_SUCCESS;
}

HI_S32 HIADP_AVPlay_SetVdecAttr(HI_HANDLE hAvplay,HI_UNF_VCODEC_TYPE_E enType,HI_UNF_VCODEC_MODE_E enMode)
{
    HI_S32 Ret;
    HI_UNF_VCODEC_ATTR_S        VdecAttr;
    
    Ret = HI_UNF_AVPLAY_GetAttr(hAvplay, HI_UNF_AVPLAY_ATTR_ID_VDEC, &VdecAttr);
    if (HI_SUCCESS != Ret)
    {
        SW_ERR("HI_UNF_AVPLAY_GetAttr failed:%#x\n",Ret);        
        return Ret;
    }

    VdecAttr.enType = enType;
    VdecAttr.enMode = enMode;
    VdecAttr.u32ErrCover = 100;
    VdecAttr.u32Priority = 3;

    Ret = HI_UNF_AVPLAY_SetAttr(hAvplay, HI_UNF_AVPLAY_ATTR_ID_VDEC, &VdecAttr);
    if (Ret != HI_SUCCESS)
    {
        SW_ERR("call HI_UNF_AVPLAY_SetAttr failed.\n");
        return Ret;
    }

    return Ret;
}

#if defined (DOLBYPLUS_HACODEC_SUPPORT)
static HI_VOID DDPlusCallBack(DOLBYPLUS_EVENT_E Event, HI_VOID *pUserData)
{
    DOLBYPLUS_STREAM_INFO_S *pstInfo = (DOLBYPLUS_STREAM_INFO_S *)pUserData;
#if 0
        SW_DBG( "DDPlusCallBack show info:\n \
                s16StreamType          = %d\n \
                s16Acmod               = %d\n \
                s32BitRate             = %d\n \
                s32SampleRateRate      = %d\n \
                Event                  = %d\n", 
                pstInfo->s16StreamType, pstInfo->s16Acmod, pstInfo->s32BitRate, pstInfo->s32SampleRateRate,Event);
#endif
        g_u32DolbyAcmod = pstInfo->s16Acmod;

        if (HA_DOLBYPLUS_EVENT_SOURCE_CHANGE == Event)
    {
                g_bDrawChnBar = HI_TRUE;
                SW_DBG("DDPlusCallBack enent !\n");
        }
    return;
}

#endif

HI_S32 HIADP_AVPlay_SetAdecAttr(HI_HANDLE hAvplay, HI_U32 enADecType, HI_HA_DECODEMODE_E enMode, HI_S32 isCoreOnly)
{
    HI_UNF_ACODEC_ATTR_S AdecAttr;
    WAV_FORMAT_S stWavFormat;

    HIAPI_RUN_RETURN(HI_UNF_AVPLAY_GetAttr(hAvplay, HI_UNF_AVPLAY_ATTR_ID_ADEC, &AdecAttr));
    AdecAttr.enType = enADecType;

    if (HA_AUDIO_ID_PCM == AdecAttr.enType)
    {
        HI_BOOL isBigEndian;
        /* set pcm wav format here base on pcm file */
        stWavFormat.nChannels = 1;
        stWavFormat.nSamplesPerSec = 48000;
        stWavFormat.wBitsPerSample = 16;
        /* if big-endian pcm */
        isBigEndian = HI_FALSE;
        if(HI_TRUE == isBigEndian)
        {
        	stWavFormat.cbSize =4;
        	stWavFormat.cbExtWord[0] = 1;
        }
        HA_PCM_DecGetDefalutOpenParam(&(AdecAttr.stDecodeParam),&stWavFormat);
        SW_DBG("please make sure the attributes of PCM stream is tme same as defined in function of \"HIADP_AVPlay_SetAdecAttr\"? \n");
        SW_DBG("(nChannels = 1, wBitsPerSample = 16, nSamplesPerSec = 48000, isBigEndian = HI_FALSE) \n");
    }
#if 0    
    else if (HA_AUDIO_ID_G711 == AdecAttr.enType)
    {
         HA_G711_GetDecDefalutOpenParam(&(AdecAttr.stDecodeParam));
    }
#endif    
    else if (HA_AUDIO_ID_MP2 == AdecAttr.enType)
    {
    	 HA_MP2_DecGetDefalutOpenParam(&(AdecAttr.stDecodeParam));
    }
    else if (HA_AUDIO_ID_AAC == AdecAttr.enType)
    {
    	 HA_AAC_DecGetDefalutOpenParam(&(AdecAttr.stDecodeParam));
    }
    else if (HA_AUDIO_ID_MP3 == AdecAttr.enType)
    {
    	 HA_MP3_DecGetDefalutOpenParam(&(AdecAttr.stDecodeParam));
    }
    else if (HA_AUDIO_ID_AMRNB== AdecAttr.enType)
    {
        AMRNB_DECODE_OPENCONFIG_S *pstConfig = (AMRNB_DECODE_OPENCONFIG_S *)u8DecOpenBuf;        
        HA_AMRNB_GetDecDefalutOpenParam(&(AdecAttr.stDecodeParam), pstConfig);
        pstConfig->enFormat = AMRNB_MIME;		
    }
    else if (HA_AUDIO_ID_AMRWB== AdecAttr.enType)
    {
        AMRWB_DECODE_OPENCONFIG_S *pstConfig = (AMRWB_DECODE_OPENCONFIG_S *)u8DecOpenBuf;
        HA_AMRWB_GetDecDefalutOpenParam(&(AdecAttr.stDecodeParam), pstConfig);
        pstConfig->enFormat = AMRWB_FORMAT_MIME;
    }
    else if (HA_AUDIO_ID_AC3PASSTHROUGH== AdecAttr.enType)
    {
        HA_AC3PASSTHROUGH_DecGetDefalutOpenParam(&(AdecAttr.stDecodeParam));
        AdecAttr.stDecodeParam.enDecMode = HD_DEC_MODE_THRU;
    }
    else if(HA_AUDIO_ID_DTSPASSTHROUGH ==  AdecAttr.enType)
    {
    	        HA_DTSPASSTHROUGH_DecGetDefalutOpenParam(&(AdecAttr.stDecodeParam));
       		 AdecAttr.stDecodeParam.enDecMode = HD_DEC_MODE_THRU;
    }
    else if (HA_AUDIO_ID_TRUEHD == AdecAttr.enType)
    {
        HA_TRUEHD_DecGetDefalutOpenParam(&(AdecAttr.stDecodeParam));
        if (HD_DEC_MODE_THRU != enMode)
        {
            SW_ERR(" MLP decoder enMode(%d) error (mlp only support hbr Pass-through only).\n", enMode);
            return -1;
        }

        AdecAttr.stDecodeParam.enDecMode = HD_DEC_MODE_THRU;        /* truehd just support pass-through */
        SW_DBG(" TrueHD decoder(HBR Pass-through only).\n");
    }
    else if (HA_AUDIO_ID_DTSHD == AdecAttr.enType)
    {
        DTSHD_DECODE_OPENCONFIG_S *pstConfig = (DTSHD_DECODE_OPENCONFIG_S *)u8DecOpenBuf;        
    	HA_DTSHD_DecGetDefalutOpenConfig(pstConfig);
        HA_DTSHD_DecGetDefalutOpenParam(&(AdecAttr.stDecodeParam), pstConfig);
		AdecAttr.stDecodeParam.enDecMode = HD_DEC_MODE_SIMUL;

    }
#if defined (DOLBYPLUS_HACODEC_SUPPORT)
	else if (HA_AUDIO_ID_DOLBY_PLUS == AdecAttr.enType)
	{
		DOLBYPLUS_DECODE_OPENCONFIG_S *pstConfig = (DOLBYPLUS_DECODE_OPENCONFIG_S *)u8DecOpenBuf;
		HA_DOLBYPLUS_DecGetDefalutOpenConfig(pstConfig);
		pstConfig->pfnEvtCbFunc[HA_DOLBYPLUS_EVENT_SOURCE_CHANGE] = DDPlusCallBack;
		pstConfig->pAppData[HA_DOLBYPLUS_EVENT_SOURCE_CHANGE] = &g_stDDpStreamInfo;
		/* Dolby DVB Broadcast default settings */
		pstConfig->enDrcMode = DOLBYPLUS_DRC_RF; 
		pstConfig->enDmxMode = DOLBYPLUS_DMX_SRND; 
		HA_DOLBYPLUS_DecGetDefalutOpenParam(&(AdecAttr.stDecodeParam), pstConfig);
		AdecAttr.stDecodeParam.enDecMode = HD_DEC_MODE_SIMUL;
	}
#endif
    else
    {
//	     HA_DRA_DecGetDefalutOpenParam(&(AdecAttr.stDecodeParam));
         HA_DRA_DecGetOpenParam_MultichPcm(&(AdecAttr.stDecodeParam));
    }

    HIAPI_RUN_RETURN(HI_UNF_AVPLAY_SetAttr(hAvplay, HI_UNF_AVPLAY_ATTR_ID_ADEC, &AdecAttr));

    return HI_SUCCESS;
}
HI_S32 avplay_Init()
{
	HI_S32 s32Ret = 0;

	HI_UNF_AVPLAY_ATTR_S stAvplayAttr;
	HI_UNF_SYNC_ATTR_S stSyncAttr;
	HI_UNF_VCODEC_ATTR_S stVdecAttr;
	HI_UNF_ACODEC_ATTR_S stAdecAttr;

	/*register lib */
	s32Ret = HIADP_AVPlay_RegADecLib();
	s32Ret |= HI_UNF_AVPLAY_Init();
	if (HI_SUCCESS != s32Ret) {
		SW_WARN("call HI_UNF_AVPLAY_Init failed.\n");
		dev_deinit();
	}

	s32Ret =
	    HI_UNF_AVPLAY_GetDefaultConfig(&stAvplayAttr,
					   HI_UNF_AVPLAY_STREAM_TYPE_TS);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_AVPLAY_GetDefaultConfig failed.\n");
		HI_UNF_AVPLAY_DeInit();
		dev_deinit();
		return s32Ret;
	}

	s32Ret = HI_UNF_AVPLAY_Create(&stAvplayAttr, &hAvplay);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_AVPLAY_Create failed.\n");
		HI_UNF_AVPLAY_DeInit();
		dev_deinit();
		return s32Ret;
	}

	s32Ret =
	    HI_UNF_AVPLAY_ChnOpen(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_VID,
				  HI_NULL);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_AVPLAY_ChnOpen failed.\n");
		HI_UNF_AVPLAY_Destroy(hAvplay);
		HI_UNF_AVPLAY_DeInit();
		dev_deinit();
		return s32Ret;
	}

	s32Ret =
	    HI_UNF_AVPLAY_ChnOpen(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_AUD,
				  HI_NULL);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_AVPLAY_ChnOpen failed.\n");
		HI_UNF_AVPLAY_ChnClose(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_VID);
		HI_UNF_AVPLAY_Destroy(hAvplay);
		dev_deinit();
		return s32Ret;
	}

	s32Ret = HI_UNF_VO_AttachWindow(phWin, hAvplay);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_VO_AttachWindow failed.\n");
		HI_UNF_AVPLAY_ChnClose(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_AUD);
		HI_UNF_AVPLAY_ChnClose(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_VID);
		HI_UNF_AVPLAY_Destroy(hAvplay);
		HI_UNF_AVPLAY_DeInit();
		dev_deinit();
		return s32Ret;
	}

	s32Ret = HI_UNF_VO_SetWindowEnable(phWin, HI_TRUE);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_VO_SetWindowEnable failed.\n");
		HI_UNF_VO_DetachWindow(phWin, hAvplay);
		HI_UNF_AVPLAY_ChnClose(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_AUD);
		HI_UNF_AVPLAY_ChnClose(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_VID);
		HI_UNF_AVPLAY_Destroy(hAvplay);
		HI_UNF_AVPLAY_DeInit();
		dev_deinit();
		return s32Ret;
	}

	s32Ret =
	    HI_UNF_SND_Attach(HI_UNF_SND_0, hAvplay, HI_UNF_SND_MIX_TYPE_MASTER,
			      100);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_SND_Attach failed.\n");
		HI_UNF_VO_SetWindowEnable(phWin, HI_FALSE);
		HI_UNF_VO_DetachWindow(phWin, hAvplay);
		HI_UNF_AVPLAY_ChnClose(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_AUD);
		HI_UNF_AVPLAY_ChnClose(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_VID);
		HI_UNF_AVPLAY_Destroy(hAvplay);
		HI_UNF_AVPLAY_DeInit();
		dev_deinit();
		return s32Ret;
	}

	stAdecAttr.enType = HA_AUDIO_ID_MP3;
	stAdecAttr.stDecodeParam.enDecMode = HD_DEC_MODE_RAWPCM;
	stAdecAttr.stDecodeParam.pCodecPrivateData = HI_NULL;
	stAdecAttr.stDecodeParam.u32CodecPrivateDataSize = 0;
	stAdecAttr.stDecodeParam.sPcmformat.u32DesiredOutChannels = 2;
	stAdecAttr.stDecodeParam.sPcmformat.bInterleaved = HI_TRUE;
	stAdecAttr.stDecodeParam.sPcmformat.u32BitPerSample = 16;
	stAdecAttr.stDecodeParam.sPcmformat.u32DesiredSampleRate = 48000;
	s32Ret =
	    HI_UNF_AVPLAY_SetAttr(hAvplay, HI_UNF_AVPLAY_ATTR_ID_ADEC,
				  &stAdecAttr);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_AVPLAY_SetAttr_ADEC failed.\n");
		HI_UNF_AVPLAY_Destroy(hAvplay);
		HI_UNF_AVPLAY_DeInit();
		dev_deinit();
		return s32Ret;
	}

	HI_UNF_AVPLAY_GetAttr(hAvplay, HI_UNF_AVPLAY_ATTR_ID_VDEC, &stVdecAttr);
	stVdecAttr.enMode = HI_UNF_VCODEC_MODE_NORMAL;
	stVdecAttr.enType = HI_UNF_VCODEC_TYPE_MPEG2;
	stVdecAttr.u32ErrCover = 80;
	stVdecAttr.u32Priority = HI_UNF_VCODEC_MAX_PRIORITY;
	stVdecAttr.bOrderOutput = HI_FALSE;
	s32Ret =
	    HI_UNF_AVPLAY_SetAttr(hAvplay, HI_UNF_AVPLAY_ATTR_ID_VDEC,
				  &stVdecAttr);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_AVPLAY_SetAttr_VDEC failed.\n");
		HI_UNF_AVPLAY_Destroy(hAvplay);
		HI_UNF_AVPLAY_DeInit();
		dev_deinit();
		return s32Ret;
	}

	s32Ret =
	    HI_UNF_AVPLAY_GetAttr(hAvplay, HI_UNF_AVPLAY_ATTR_ID_SYNC,
				  &stSyncAttr);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_AVPLAY_SetAttr failed.\n");
		HI_UNF_SND_Detach(HI_UNF_SND_0, hAvplay);
		HI_UNF_VO_SetWindowEnable(phWin, HI_FALSE);
		HI_UNF_VO_DetachWindow(phWin, hAvplay);
		HI_UNF_AVPLAY_ChnClose(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_AUD);
		HI_UNF_AVPLAY_ChnClose(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_VID);
		HI_UNF_AVPLAY_Destroy(hAvplay);
		HI_UNF_AVPLAY_DeInit();
		dev_deinit();
		return s32Ret;
	}
	stSyncAttr.enSyncRef = HI_UNF_SYNC_REF_PCR;
	stSyncAttr.stSyncStartRegion.s32VidPlusTime = 60;
	stSyncAttr.stSyncStartRegion.s32VidNegativeTime = -20;
	stSyncAttr.bQuickOutput = HI_FALSE;
	s32Ret =
	    HI_UNF_AVPLAY_SetAttr(hAvplay, HI_UNF_AVPLAY_ATTR_ID_SYNC,
				  &stSyncAttr);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_AVPLAY_SetAttr failed.\n");
		HI_UNF_SND_Detach(HI_UNF_SND_0, hAvplay);
		HI_UNF_VO_SetWindowEnable(phWin, HI_FALSE);
		HI_UNF_VO_DetachWindow(phWin, hAvplay);
		HI_UNF_AVPLAY_ChnClose(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_AUD);
		HI_UNF_AVPLAY_ChnClose(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_VID);
		HI_UNF_AVPLAY_Destroy(hAvplay);
		HI_UNF_AVPLAY_DeInit();
		dev_deinit();
		return s32Ret;
	}

	/*aaa AudioLineOutMuteCntrDisable(); */

	s32Ret =
	    HI_UNF_AVPLAY_Start(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_AUD, NULL);
	if (HI_SUCCESS != s32Ret) {
		SW_LDBG("call HI_UNF_AVPLAY_Start_AUD failed.\n");
		/*return ret; */
	}

	s32Ret =
	    HI_UNF_AVPLAY_Start(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_VID, NULL);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("call HI_UNF_AVPLAY_Start_VID failed.\n");
		return s32Ret;
	}

	s32Ret = HI_UNF_SND_SetVolume(HI_UNF_SND_0, 100);
	return HI_SUCCESS;
}

HI_S32 avplay_deinit()
{
	HI_S32 s32Ret = 0;
	HI_UNF_AVPLAY_STOP_OPT_S stStop;

	stStop.enMode = HI_UNF_AVPLAY_STOP_MODE_BLACK;
	stStop.u32TimeoutMs = 0;

	s32Ret =
	    HI_UNF_AVPLAY_Stop(hAvplay,
			       HI_UNF_AVPLAY_MEDIA_CHAN_VID |
			       HI_UNF_AVPLAY_MEDIA_CHAN_AUD, &stStop);
	if (HI_SUCCESS != s32Ret) {
		SW_WARN("call HI_UNF_AVPLAY_Stop failed.\n");
	}

	s32Ret = HI_UNF_SND_Detach(HI_UNF_SND_0, hAvplay);
	if (HI_SUCCESS != s32Ret) {
		SW_WARN("call HI_UNF_SND_Detach failed.\n");
	}

	s32Ret = HI_UNF_VO_DetachWindow(phWin, hAvplay);
	if (HI_SUCCESS != s32Ret) {
		SW_WARN("call HI_UNF_VO_DetachWindow failed.\n");
	}

	s32Ret = HI_UNF_AVPLAY_ChnClose(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_VID);
	if (HI_SUCCESS != s32Ret) {
		SW_WARN("call HI_UNF_AVPLAY_ChnClose failed.\n");
	}

	s32Ret = HI_UNF_AVPLAY_ChnClose(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_AUD);
	if (HI_SUCCESS != s32Ret) {
		SW_WARN("call HI_UNF_AVPLAY_ChnClose failed.\n");
	}

	s32Ret = HI_UNF_AVPLAY_Destroy(hAvplay);
	if (HI_SUCCESS != s32Ret) {
		SW_WARN("call HI_UNF_AVPLAY_Destroy failed.\n");
	}

	s32Ret = HI_UNF_AVPLAY_DeInit();
	if (HI_SUCCESS != s32Ret) {
		SW_WARN("call HI_UNF_AVPLAY_DeInit failed.\n");
	}

	return HI_SUCCESS;
}

HI_VOID pid_select_play(HI_U32 u32PcrPid, HI_U32 u32AudType, HI_U32 u32Apid, HI_U32 u32VidType, HI_U32 u32Vpid)
{
	HI_S32 s32Ret;
	HI_UNF_AVPLAY_STOP_OPT_S stStop;

	stStop.enMode = HI_UNF_AVPLAY_STOP_MODE_STILL;
	stStop.u32TimeoutMs = 0;

	s32Ret =
	    HI_UNF_AVPLAY_Stop(hAvplay,
			       HI_UNF_AVPLAY_MEDIA_CHAN_VID |
			       HI_UNF_AVPLAY_MEDIA_CHAN_AUD, &stStop);
	if (s32Ret != HI_SUCCESS) {
		SW_ERR("call HI_UNF_AVPLAY_Stop failed.\n");
		return;
	}

    s32Ret = HI_UNF_AVPLAY_SetAttr(hAvplay,HI_UNF_AVPLAY_ATTR_ID_PCR_PID,&u32PcrPid);
	if (s32Ret != HI_SUCCESS) {
		SW_ERR("call HI_UNF_AVPLAY_SetAttr failed.\n");
		return;
	}

	s32Ret = HIADP_AVPlay_SetAdecAttr(hAvplay, u32AudType, HD_DEC_MODE_RAWPCM, 1);
	s32Ret |=
	    HI_UNF_AVPLAY_SetAttr(hAvplay, HI_UNF_AVPLAY_ATTR_ID_AUD_PID,
				  &u32Apid);
	if (s32Ret != HI_SUCCESS) {
		SW_ERR("call HI_UNF_AVPLAY_SetAttr failed.\n");
		return;
	}

	s32Ret = HIADP_AVPlay_SetVdecAttr(hAvplay, u32VidType, HI_UNF_VCODEC_MODE_NORMAL);
	s32Ret |=
	    HI_UNF_AVPLAY_SetAttr(hAvplay, HI_UNF_AVPLAY_ATTR_ID_VID_PID,
				  &u32Vpid);
	if (s32Ret != HI_SUCCESS) {
		SW_ERR("call HI_UNF_AVPLAY_SetAttr failed.\n");
		return;
	}

	HI_UNF_AVPLAY_Start(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_AUD, NULL);
	if (s32Ret != HI_SUCCESS) {
		SW_WARN("call HI_UNF_AVPLAY_Start_AUD failed.\n");
        return;
	}

	HI_UNF_AVPLAY_Start(hAvplay, HI_UNF_AVPLAY_MEDIA_CHAN_VID, NULL);
	if (s32Ret != HI_SUCCESS) {
		SW_ERR("call HI_UNF_AVPLAY_Start_VID failed.\n");
		return;
	}

	SW_LDBG("Play channel (pcr=%d, v=%d, a=%d)\n", u32PcrPid, u32Vpid, u32Apid);
}

void open_channel(void)
{
	HI_S32 Ret;
	CHANNEL_INFO ch;


	get_channel_info(g_ch_num, &ch);

	dev_init();
    if(g_factory_conf.fun.fe == DVB_S2){
        his_tuner_init(0);
    }else if(g_factory_conf.fun.fe == DVB_T){
        his_tuner_init(1);
    }else if(g_factory_conf.fun.fe == DVB_T2){
        his_tuner_init(2);
    }else{
        SW_ERR("Wrong frontend config!\n");
        assert(0);
    }
	Ret = DmxInit();
	if (Ret != HI_SUCCESS) {
		SW_ERR("call DmxInit failed.\n");
	}

	Ret = avplay_Init();
	if (HI_SUCCESS != Ret) {
		SW_ERR("avplay_Init err. ErrorCode=0x%x\n",  Ret);
	}


	return;
}

void close_channel(void)
{
	HI_S32 Ret;

	avplay_deinit();

	Ret = DmxDeInit();
	if (Ret != HI_SUCCESS) {
		SW_ERR("call DmxDeInit failed.\n");
	}

	his_tuner_exit();

	dev_deinit();
}

void channel_change(int plus)	
{
    CHANNEL_INFO ch; 

    g_ch_num = (g_ch_num + g_factory_conf.channel_db.num + plus)%g_factory_conf.channel_db.num;
    get_channel_info(g_ch_num, &ch);
    ui_update_ch_text(ch.name);

    if (g_factory_conf.fun.fe == DVB_S2){
        //change tuner options according to playing channel
        if(ch.lo == 0){
            g_22khzTone = 0;
            his_tuner_set_22khz(0);
        }else{
            g_22khzTone = 1;
            his_tuner_set_22khz(1);
        }

        ui_update_22k_text(g_22khzTone);

        if (ch.pol == 0) {
            //Horizontal
            g_lnb = 2;	//18V
            his_tuner_set_lnb_power(1, 0);
        } else {
            //vertical
            g_lnb = 1;	//13V
            his_tuner_set_lnb_power(1, 1);
        }

        ui_update_lnb_text(g_lnb);

        if(ch.lo == 0){
            his_tuner_set_lnb(5150 * 1000, 0, 0);
        }else{
            his_tuner_set_lnb(9750 * 1000, 10600*1000, 0);
        }
        his_tuner_tune(ch.freq * 1000, ch.sr * 1000, ch.pol, 1000);
    }else if (g_factory_conf.fun.fe == DVB_T || g_factory_conf.fun.fe == DVB_T2){

        //Init tuner power.
        his_tuner_ter_antennapower(g_antenna_power);
        his_tuner_ter_tune(ch.freq, ch.bw, 0);
    }

    if (!his_tuner_get_status()) {
        SW_ERR("tuner unlocked!\n");
    }else{
        SW_LDBG("tuner locked\n");
    }

    pid_select_play(ch.p_pid, ch.aenc_fmt, ch.a_pid, ch.venc_fmt, ch.v_pid);
}
