#ifndef _CHANNEL_H_
#define _CHANNEL_H_

extern int g_22khzTone;
extern int g_lnb;
extern int g_antenna_power;

void close_channel(void);
void open_channel(void);
void channel_change(int plus);
#endif
