#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <pthread.h>

#include <assert.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <net/if.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <arpa/inet.h>

#include "hi_unf_ecs.h"
#include "hi_adp.h"
#include "hi_adp_audio.h"
#include "hi_adp_hdmi.h"
#include "hi_adp_boardcfg.h"
#include "hi_adp_mpi.h"
#include "hi_adp_tuner.h"
#include "hi_adp_pvr.h"

#include "hifb.h"
#include "hi_unf_disp.h"
#include "hi_adp_data.h"
#include "hi_unf_ecs.h"

#include "dvb_his_fe.h"

#include "sw_debug.h"
#include "sw_fb_handle.h"
#include "sign_api.h"
#include "fast_crc.h"
#include "../config.h"
#include "ui.h"
#include "command.h"
#include "../storage.h"
#include "../emmc_check.h"

static unsigned int g_hdcp_key_no = 0;
static char g_device_id[STOR_DEVICE_ID_LEN + 1];
static unsigned char g_mac[STOR_MAC_LEN];

#ifdef DISABLE_SERIAL
int g_got_serial_n = 1;
char g_serial_number[PL_SN_LEN + 1] = "U4-TEST140300015";
#else
int g_got_serial_n = 0;
char g_serial_number[PL_SN_LEN + 1];
#endif

#define MAX_DB_COMMAND_DATA_LEN 1024
#define MAX_DB_COMMAND_LEN (TRANSMISSION_HEAD_LEN + MAX_DB_COMMAND_DATA_LEN)
#define MAX_SOCKET_READ_DATA_LEN 2048
static char command_buffer[MAX_DB_COMMAND_LEN];
static char socket_read_buffer[MAX_SOCKET_READ_DATA_LEN];

static char command_data_buffer[MAX_DB_COMMAND_DATA_LEN];

static int g_hdcp_result = 0;
static int g_mac_result = 0;

//PLCS38
static int create_PLCS38_data(char *data)
{
	int lenth = 0;
	int order_no_len;
	int i;

	for (i = 0; i < PL_SN_LEN; i++) {
		data[lenth++] = g_serial_number[i];	// S/N Number   (16Byte)
	}

	order_no_len = strlen(g_factory_conf.prog.order_no);
	data[lenth++] = order_no_len;	// Order Length (1byte)
	for (i = 0; i < order_no_len; i++) {
		data[lenth++] = g_factory_conf.prog.order_no[i];	// Order Number (10byte)
	}

	data[lenth++] = 0;	// OTP Flag     (1byte)
	data[lenth++] = 1;	// MAC Flag     (1byte)
#ifdef ENABLE_HDCP_PROGRAM
	if (g_factory_conf.prog.hdcp == 1) {
        data[lenth++] = 1;	// HDCP Flag    (1byte)
    }else{
        data[lenth++] = 0;	// HDCP Flag    (1byte)
    }
#else
	data[lenth++] = 0;	// HDCP Flag    (1byte)
#endif

	return lenth;
}

//PLCS36
static int create_PLCS36_data(char *data)
{
	int i;
	int offset = 0;

	for (i = 0; i < STOR_DEVICE_ID_LEN; i++) {
		data[offset++] = g_device_id[i];	// Device ID    (16byte)
	}

	data[offset++] = 0x00;	// OTP Flag     (1byte)
	data[offset++] = 0x01;	// MAC Flag     (1byte)
#ifdef ENABLE_HDCP_PROGRAM
	if (g_factory_conf.prog.hdcp == 1) {
        data[offset++] = 0x01;	// HDCP Flag    (1byte)
    }else{
        data[offset++] = 0x00;	// HDCP Flag    (1byte)
    }
#else
	data[offset++] = 0x00;	// HDCP Flag    (1byte)
#endif

	data[offset++] = (g_factory_conf.prog.model & 0x000000FF);
	data[offset++] = ((g_factory_conf.prog.model >> 8) & 0x000000FF);

	return offset;
}

//PLCS30
static int create_PLCS30_data(char *data)
{
	int lenth = 0;
	int i;
	int order_no_len;

	for (i = 0; i < STOR_DEVICE_ID_LEN; i++) {
		data[lenth++] = g_device_id[i];	// Device ID    (16byte)
	}
	data[lenth++] = 0x00;	// OTP Flag     (1byte)
	data[lenth++] = 0x01;	// MAC Flag     (1byte)
#ifdef ENABLE_HDCP_PROGRAM
	if (g_factory_conf.prog.hdcp == 1) {
        data[lenth++] = 0x01;	// HDCP Flag    (1byte)
    }else{
        data[lenth++] = 0x00;	// HDCP Flag    (1byte)
    }
#else
	data[lenth++] = 0x00;	// HDCP Flag    (1byte)
#endif
	data[lenth++] = (g_factory_conf.prog.model & 0x000000FF);	// MODEL ID2    (1byte)  0x03
	data[lenth++] = ((g_factory_conf.prog.model >> 8) & 0x000000FF);	// MODEL ID1    (1byte)  0x8b
	for (i = 0; i < PL_SN_LEN; i++) {
		data[lenth++] = g_serial_number[i];	// S/N Number   (16byte)
	}

	order_no_len = strlen(g_factory_conf.prog.order_no);
	data[lenth++] = order_no_len;	// Order Length (1byte)
	for (i = 0; i < order_no_len; i++) {
		data[lenth++] = g_factory_conf.prog.order_no[i];	// Order Number (10byte)
	}

	for (i = 0; i < 4; i++) {
		data[lenth++] = 0x00;	// Server ID    (4byte)
	}

	return lenth;
}

//PLCS35
static int create_PLCS35_data(int mac, int hdcp, char *data)
{
	int offset = 0;
	int i;

	for (i = 0; i < STOR_DEVICE_ID_LEN; i++) {
		data[offset++] = g_device_id[i];	// Device ID     (16byte)
	}
	data[offset++] = 0x00;	// OTP Flag      (1byte)
	data[offset++] = 0x01;	// MAC Flag      (1byte)
#ifdef ENABLE_HDCP_PROGRAM
	if (g_factory_conf.prog.hdcp == 1) {
        data[offset++] = 0x01;	// HDCP Flag     (1byte)
    }else{
        data[offset++] = 0x00;	// HDCP Flag     (1byte)
    }
#else
	data[offset++] = 0x00;	// HDCP Flag     (1byte)
#endif
	data[offset++] = 0x00;	// OTP Result    (1byte)
	data[offset++] = mac;	// MAC Result    (1byte)
#ifdef ENABLE_HDCP_PROGRAM
	data[offset++] = hdcp;	// HDCP Result   (1byte)
#else
	data[offset++] = 0x00;	// HDCP Result   (1byte)
#endif
	data[offset++] = (g_factory_conf.prog.model & 0x000000FF);	// MODEL ID2     (1byte)
	data[offset++] = ((g_factory_conf.prog.model >> 8) & 0x000000FF);	// MODEL ID1     (1byte)
	for (i = 0; i < PL_SN_LEN; i++) {
		data[offset++] = g_serial_number[i];	// S/N Number    (16byte)
	}

	return 40;
}

//PLCS37: send all box info
static int create_PLCS37_data(char *data)
{
	int lenth = 0;
	int i;
	int order_no_len;

	/*Device ID (16 Byte) */
	for (i = 0; i < STOR_DEVICE_ID_LEN; i++) {
		data[lenth++] = g_device_id[i];	// Device ID
	}

	/*Model Number (2 Byte) */
	data[lenth++] = (g_factory_conf.prog.model & 0x000000FF);	// MODEL ID2     (1byte)
	data[lenth++] = ((g_factory_conf.prog.model >> 8) & 0x000000FF);	// MODEL ID1     (1byte)

	/*Chip Type (32 Byte) */
	for (i = 0; i < 32; i++) {
		data[lenth++] = g_factory_conf.prog.chip_type[i];	// Chip Type
	}

	/*Mac Info (6 Byte) */
	for (i = 0; i < 6; i++) {
		data[lenth++] = g_mac[i];	// Mac Info
	}

	/*HDCP Name Info (4 Byte) */
#ifdef ENABLE_HDCP_PROGRAM
	data[lenth++] = g_hdcp_key_no & 0xFF;
	data[lenth++] = (g_hdcp_key_no >> 8) & 0xFF;
	data[lenth++] = (g_hdcp_key_no >> 16) & 0xFF;
	data[lenth++] = (g_hdcp_key_no >> 24) & 0xFF;
#else
	data[lenth++] = 0; 
	data[lenth++] = 0; 
	data[lenth++] = 0; 
	data[lenth++] = 0; 
#endif

	/*OTP Key (16Byte) */
	for (i = 0; i < 16; i++) {
		data[lenth++] = 0x00;	// OTP Info
	}
	/*Board SN Key (18Byte) */
	for (i = 0; i < 18; i++) {
		data[lenth++] = 0x00;	// Board SN Info
	}
	/*Wifi Info (6 Byte) */
	for (i = 0; i < 6; i++) {
		data[lenth++] = 0x00;	// Wifi Info
	}
	/*S/N Number (16byte) */
	for (i = 0; i < PL_SN_LEN; i++) {
		data[lenth++] = g_serial_number[i];	// S/N Number
	}
	/*Order Number (10byte) */
	order_no_len = strlen(g_factory_conf.prog.order_no);
	data[lenth++] = order_no_len;	// Order Length (1byte)
	for (i = 0; i < order_no_len; i++) {
		data[lenth++] = g_factory_conf.prog.order_no[i];	// Order Number (10byte)
	}

	data[lenth++] = 0;	/*Null terminate of order number */

	return lenth;
}

static int command_pre_process(COMMAND_TYPE type, char *data, int len,
			       COMMAND_TYPE prefer)
{

	int shot = 0;
	char message[128];

	if (type == Error_Type && len == 1) {
		sprintf(message, "Get error report from server: %d", data[0]);
		ui_update_client_status(TS_BAD, message);
	} else if (type == prefer) {
		shot = 1;
	} else {
		ui_update_client_status(TS_BAD, "Gould not get ACK from server!");
	}

	return shot;
}

static int PLSS30_command_process(char *data, int len)
{
	int i;
	int ret;
	int offset = 0;
	unsigned char* key;
	char hdcpname[17 + 1];
	unsigned char mac[6];
	char device_id[16 + 1];
	int key_no = 0;
	unsigned short customer;
	unsigned short model;
    EEPROM_CONFIG conf;

	//skip otp_key
	offset += 16;

	//get MAC 
	for (i = 0; i < 6; i++) {
		mac[i] = data[offset++];
	}

#ifdef ENABLE_HDCP_PROGRAM

#if (HDCP_KEY_MODE == HDCP_KEY_MODE_IDS_ENCRYPT)
#define HDCP_KEY_PACK_SIZE 286
#else
#error "Only support IDS encrypt HDCP key right now!"
#endif

	//get HDCP Key
    key = (unsigned char*)data + offset;
    offset += HDCP_KEY_PACK_SIZE;

	if (g_factory_conf.prog.hdcp == 1) {
		ret = otp_check_hdcp_key();
		SW_DBG("OTP check hdcp, ret = %d\n", ret);
		if (ret == 0) {
			ret = otp_write_hdcp_key(key, HDCP_KEY_PACK_SIZE);

			if (ret == 0) {
				g_hdcp_result = 1;
				ui_update_client_status(TS_NORMAL, "Write HDCP succussfully");
                ui_update_otp(1);
                u4t_enable_hdcp();
                {
                    char* dvb;
                    char* sdk;
                    int v_dvb;
                    int v_sdk;
                    int v_chunks;
                    int v_files;

                    ret = firmware_check(&dvb, &v_dvb, &sdk, &v_sdk, &v_chunks, &v_files);
                    if (ret == 0) {
                        ui_update_firmware_info(dvb, v_dvb, sdk, v_sdk, v_chunks, v_files);
                    }
                }
			} else {
				g_hdcp_result = 0;
				ui_update_client_status(TS_BAD,
						     "Write HDCP failed. !!!OTP ERROR!!!");
				return -1;
			}
		} else if (ret == 1) {
			ui_update_client_status(TS_BAD, "HDCP has been written!!!");
			return -1;
		} else {
			ui_update_client_status(TS_BAD, "HDCP OTP check error!!!");
			return -1;
		}

	} else {

		g_hdcp_result = 0;
	}

	//TODO: show HDCP name 
	for (i = 0; i < 17; i++) {
		hdcpname[i] = data[offset++];
	}
	hdcpname[17] = 0;
	SW_DBG("%s\n", hdcpname);

	hdcpname[6 + 7] = 0;
	key_no = atoi(hdcpname + 6);
	SW_DBG("key_no = %d\n", key_no);

#endif

    conf.customer = g_factory_conf.prog.customer;
    conf.model = g_factory_conf.prog.model;
    memcpy(conf.mac, mac, STOR_MAC_LEN);
    memcpy(conf.device_id, g_device_id, STOR_DEVICE_ID_LEN);
    conf.hdcp_key_no = key_no;
    if(g_factory_conf.fun.front_type == 0){
        //fnd
        conf.front_panel = 1;
    }else{
        //vfd
        conf.front_panel = 2;
    }
    conf.skin = g_factory_conf.prog.skin;
	ret = store_factory_data(&conf);
	if (ret == 0) {
		g_mac_result = 1;
		ui_update_client_status(TS_NORMAL, "Write eeprom successfully");
	} else {

		g_mac_result = 0;
		ui_update_client_status(TS_BAD, "Write eeprom failed");
		return -2;
	}

    //load new data and display
    if(0 != load_factory_data(&conf)){
        SW_DBG("Load storage data failed!!!\n");
        ui_update_client_status(TS_BAD, "Load storage data failed!!!");
        return -2;
    }
    //TODO:fixme
    g_hdcp_key_no = conf.hdcp_key_no;
    memcpy(g_device_id, conf.device_id, STOR_DEVICE_ID_LEN);
    g_device_id[STOR_DEVICE_ID_LEN] = 0;
    memcpy(g_mac, conf.mac, STOR_MAC_LEN);
	ui_update_factory_data(&conf);

	return 0;
}

static int TalkToDBServer(void)
{
	int count;
	int ssock;
	int clen;
	struct sockaddr_in server_addr;
	int readbytes;
	char *pRevDataAll;
	char status_msg[STATUS_MSG_MAX_LEN + 1];
	int len;
	int parser_ret;
	char parsered_data[DATA_LEN];
	unsigned int data_len;
	COMMAND_TYPE type;
	int shot;
	int ret;
	int burn_result;

	MG_Setup_CRC_Table();

	SW_DBG("TalkToDBServer START !!\n");

	if ((ssock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		ui_update_client_status(TS_BAD, "Create socket failed!!!");
		return -1;
	}
	clen = sizeof(server_addr);
	memset(&server_addr, 0, clen);

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(g_factory_conf.server.db_ip);
	server_addr.sin_port = htons(g_factory_conf.server.db_port);
	sprintf(status_msg, "Connecting db server %s:%d", g_factory_conf.server.db_ip, g_factory_conf.server.db_port);
	while (1) {
		if (connect(ssock, (struct sockaddr *)&server_addr, clen) < 0) {
			ui_update_client_status(TS_NORMAL, status_msg);
			sleep(3);
		} else {
			break;
		}
	}

#if 1
	//PLCS38: Send Serial Number
	len = create_PLCS38_data(command_data_buffer);
	memset(command_buffer, 0, MAX_DB_COMMAND_LEN);
	combine_data(PLCS38_Send_SN_TO_Check, command_data_buffer, len,
		     command_buffer);

	if (write(ssock, command_buffer, TRANSMISSION_HEAD_LEN + len) < 0) {
		ui_update_client_status(TS_BAD, "Send command to server failed");
		close(ssock);
		return -1;
	}

	memset(socket_read_buffer, 0, MAX_SOCKET_READ_DATA_LEN);
	//sleep(1);
	if ((readbytes =
	     read(ssock, socket_read_buffer, MAX_SOCKET_READ_DATA_LEN)) <= 0) {
		ui_update_client_status(TS_BAD, "Read command from server failed");
		close(ssock);
		return -1;
	}

	parser_ret =
	    parser_data(&type, socket_read_buffer, parsered_data, &data_len,
			readbytes);

	shot = 0;
	if (parser_ret == RET_OK) {
		shot =
		    command_pre_process(type, parsered_data, data_len,
					PLSS37_Check_SN_OK);
	} else {
		ui_update_client_status(TS_BAD, "Gould not get ACK");
	}

	if (shot == 1) {
		ui_update_client_status(TS_NORMAL, "Get command PLSS37 succeccfully");
	} else {
		ui_update_client_status(TS_BAD, "Failed to get commdnad PLSS37");
		close(ssock);
		return -1;
	}
#endif

	//Attention: Device id should be stored after serial number checking!!. As it is from serial number.
	//device id will be written to eeprom in following step.

#if 1
	//PLCS36: Send device ID and model ID
	len = create_PLCS36_data(command_data_buffer);
	memset(command_buffer, 0, MAX_DB_COMMAND_LEN);
	combine_data(PLCS36_Send_C_to_Check, command_data_buffer, len,
		     command_buffer);

	if (write(ssock, command_buffer, TRANSMISSION_HEAD_LEN + len) < 0) {
		ui_update_client_status(TS_BAD, "Send command to server failed");
		close(ssock);
		return -1;
	}

	memset(socket_read_buffer, 0, MAX_SOCKET_READ_DATA_LEN);
	//sleep(1);
	if ((readbytes =
	     read(ssock, socket_read_buffer, MAX_SOCKET_READ_DATA_LEN)) <= 0) {
		ui_update_client_status(TS_BAD, "Read command from server failed");
		close(ssock);
		return -1;
	}

	parser_ret =
	    parser_data(&type, socket_read_buffer, parsered_data, &data_len,
			readbytes);

	shot = 0;
	if (parser_ret == RET_OK) {
		shot =
		    command_pre_process(type, parsered_data, data_len,
					PLSS36_Send_Check_OK);
	} else {
		ui_update_client_status(TS_BAD, "Gould not get ACK");
	}

	if (shot == 1) {
		ui_update_client_status(TS_NORMAL, "Get command PLSS36 succeccfully");
	} else {
		ui_update_client_status(TS_BAD, "Failed to get commdnad PLSS36");
		close(ssock);
		return -1;
	}
#endif

#if 1
	//PLCS30: Ask Mac and HDCP
	len = create_PLCS30_data(command_data_buffer);
	memset(command_buffer, 0, MAX_DB_COMMAND_LEN);
	combine_data(PLCS30_Send_BOXINFO, command_data_buffer, len,
		     command_buffer);

	if (write(ssock, command_buffer, TRANSMISSION_HEAD_LEN + len) < 0) {
		ui_update_client_status(TS_BAD, "Send command to server failed");
		close(ssock);
		return -1;
	}

	memset(socket_read_buffer, 0, MAX_SOCKET_READ_DATA_LEN);
	//sleep(1);
	if ((readbytes =
	     read(ssock, socket_read_buffer, MAX_SOCKET_READ_DATA_LEN)) <= 0) {
		ui_update_client_status(TS_BAD, "Read command from server failed");
		close(ssock);
		return -1;
	}

	parser_ret =
	    parser_data(&type, socket_read_buffer, parsered_data, &data_len,
			readbytes);

	shot = 0;
	if (parser_ret == RET_OK) {
		shot =
		    command_pre_process(type, parsered_data, data_len,
					PLSS30_Send_OTP_MAC_HDCP_Data);
	} else {
		ui_update_client_status(TS_BAD, "Gould not get ACK");
	}

	if (shot == 1) {
		ui_update_client_status(TS_NORMAL, "Get command PLSS30 succeccfully");
	} else {
		ui_update_client_status(TS_BAD, "Failed to get commdnad PLSS30");
		close(ssock);
		return -1;
	}

	PLSS30_command_process(parsered_data, data_len);
#endif

#if 1
	//PLCS35: send burn result
	len =
	    create_PLCS35_data(g_mac_result, g_hdcp_result,
			       command_data_buffer);
	memset(command_buffer, 0, MAX_DB_COMMAND_LEN);
	combine_data(PLCS35_Send_Burn_Result, command_data_buffer, len,
		     command_buffer);
	if (write(ssock, command_buffer, TRANSMISSION_HEAD_LEN + len) < 0) {
		ui_update_client_status(TS_BAD, "Send command to server failed");
		close(ssock);
		return -1;
	}
	ui_update_client_status(TS_NORMAL, "Send cmd PLCS35");
#endif

	//Stop client procedure when burn mac or hdcp failed.
#ifdef ENABLE_HDCP_PROGRAM
	if (g_factory_conf.prog.hdcp == 1) {
        burn_result = (g_mac_result == 0) || (g_hdcp_result == 0);
    }else{
        burn_result = (g_mac_result == 0 );
    }
#else
	burn_result = (g_mac_result == 0 );
#endif
	if (burn_result) {
		SW_DBG("mac = %d, hdcp key = %d\n", g_mac_result,
		       g_hdcp_result);
		sprintf(status_msg, "Write data failed: 0x%04x",
			(g_hdcp_result << 1) + g_mac_result);
		ui_update_client_status(TS_BAD, status_msg);
		close(ssock);
		return -1;
	}
#if 1
	//PLCS37: send all box info
	len = create_PLCS37_data(command_data_buffer);
	memset(command_buffer, 0, MAX_DB_COMMAND_LEN);
	combine_data(PLCS37_Send_STB_INFO, command_data_buffer, len,
		     command_buffer);

	if (write(ssock, command_buffer, TRANSMISSION_HEAD_LEN + len) < 0) {
		ui_update_client_status(TS_BAD, "Send command to server failed");
		close(ssock);
		return -1;
	}

	memset(socket_read_buffer, 0, MAX_SOCKET_READ_DATA_LEN);
	//sleep(1);
	if ((readbytes =
	     read(ssock, socket_read_buffer, MAX_SOCKET_READ_DATA_LEN)) <= 0) {
		ui_update_client_status(TS_BAD, "Read command from server failed");
		close(ssock);
		return -1;
	}

	parser_ret =
	    parser_data(&type, socket_read_buffer, parsered_data, &data_len,
			readbytes);

	shot = 0;
	if (parser_ret == RET_OK) {
		shot =
		    command_pre_process(type, parsered_data, data_len,
					PLSS38_Store_STB_INFO);
	} else {
		ui_update_client_status(TS_BAD, "Gould not get ACK");
	}

	if (shot == 1) {
		ui_update_client_status(TS_NORMAL, "Get command PLSS38 succeccfully");
	} else {
		ui_update_client_status(TS_BAD, "Failed to get commdnad PLSS38");
		close(ssock);
		return -1;
	}

	ui_update_client_status(TS_GOOD, "Finish successfully");

#endif
	close(ssock);

	return 0;
}

HI_VOID *FS_task_factory_client(HI_VOID * args)
{

	while(1){  
		if (g_got_serial_n == 1) {
			//Attention: Box does not have nature device id, made it from serial number.!!!
			strncpy(g_device_id, g_serial_number, STOR_DEVICE_ID_LEN);

			TalkToDBServer();
			break;
		}else{
			ui_update_client_status(TS_NORMAL, "P: Waiting for barcode...");
		}
		sleep(1);
	}

	return NULL;
}
