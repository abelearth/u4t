/*****************************************************************************
*    Ali Corp. All Rights Reserved. 2011 Copyright (C)
*
*    File:    ServerCommFunc.c
*
*    Description:  this file include the functions about the common function
*	the data format: 1.TRANSMISSION_HEAD,include:command(6bytes)+":"(1byte)+data len(4bytes)+reserve(21bytes)
				   2.DATA	
*    History:
*             Date                 Athor         Version          Reason
*	  ==============	==========     =============   ===============
*	1.  2011.8.11     	peter.tan		Ver 0.1         Create file.
*	2.  2011.8.25		peter.tan	    Ver 0.2         add the command about 3603
*	3.	2011.11.4		peter.tan		Ver 0.3			add the CRC check
*
*****************************************************************************/

//#include"stdafx.h"
#include "command.h"
#include "fast_crc.h"

#define CRC_LEN	4

#define SERVER_COMM_FUNC_DEBUG
#ifdef SERVER_COMM_FUNC_DEBUG
#define SERVER_COMM_FUNC_PRINTF	PRINTF
#else
#define SERVER_COMM_FUNC_PRINTF()	 do{}while(0)
#endif

#if 1
extern const COMMAND_T command[];

/*************commmand for Android****************/
const COMMAND_T command[]=
{
        {Error_Type, "ERR000"},//Acknowledge error

        {PLCS36_Send_C_to_Check,           "PLCS36"}, //send chip id
        {PLSS36_Send_Check_OK,             "PLSS36"}, //get chip id check result
        {PLCS30_Send_BOXINFO,              "PLCS30"}, //Send box id, model id to sever. Request mac and hdcp key.
        {PLSS30_Send_OTP_MAC_HDCP_Data,    "PLSS30"}, //get mac and hdcp key
        {PLCS35_Send_Burn_Result,          "PLCS35"}, //send burn result
        {PLCS37_Send_STB_INFO,             "PLCS37"}, //send all box infor (box id, model id, mac, hdcp)
        {PLSS38_Store_STB_INFO,            "PLSS38"}, //get record result
        {PLCS38_Send_SN_TO_Check,          "PLCS38"}, //send serial number
        {PLSS37_Check_SN_OK,               "PLSS37"}, //get serial number check result
};

#else

const COMMAND_T command[]=
{
	{TEST_Type,						"TEST01"},//for test
	{Error_Type,						"ERR000"},//if the process cause a error, send the error command

	/*type for 3601S sign*/
	{PLCS01_Ask_C_M,					"PLCS01"},//ask customer+model list//no use now
	{PLCS02_Send_C,					"PLCS02"},//send chip ID
	{PLSS01_Send_C_M_List,			"PLSS01"},//send customer+model list////no use now
	{PLSS02_Send_Sig_And_Key,		"PLSS02"},//send chip id sig+ key
	{RDCS01_Send_BootLoader_Hash,	"RDCS01"},//send bootloader hash data
	{RDCS02_Send_Other_Hash,			"RDCS02"},//send maincode/seecode or other hash data
	{RDCS03_Send_USB_Upgrade_File_Hash,			"RDCS03"},//send usb upgrade file hash data	
	{RDSS01_Send_Sig_And_Key_Boot,	"RDSS01"},//send hash sig+ key about bootloader
	{RDSS02_Send_Sig_And_Key_Main,  	"RDSS02"},//send hash sig+ key about maincode

	/*type for 3601S :use for burning hdcp key*/
	{PLCS03_Ask_Ca_Report,  			"PLCS03"},//ask ca report information
	{PLSS03_Send_Ca_Report,  			"PLSS03"},//send the ca report information
	{PLCS04_Send_Burn_HDCP_Data_Result,"PLCS04"},//the result of burning hdcp data 0:success 1:fail

	/*type for 3601S :use for RT*/
	{PLCS05_Send_C_M,  				"PLCS05"},//send chip ID and model ID
	{PLSS04_Send_RT_Data,			"PLSS04"},//this data include sig and user data
	
	/*type for 3603 */
	{PLCS11_Ask_Ca_Report,			"PLCS11"},//ask ca report information
	{PLCS12_Ask_Data_And_Send_Chip_ID,"PLCS12"},//ask the data(data sequence:SN,encrypted HDCP key,HDCP protection key,OTP setting,User Area) ,at the same time, send the chip id
	{PLSS11_Send_Ca_Report,			"PLSS11"},//send the ca report information
	{PLSS12_Send_Need_Encrypt_Data,	"PLSS12"},//send the data(data sequence:SN,encrypted HDCP key,HDCP protection key,OTP setting,User Area) 
	{PLCS13_Send_Result,				"PLCS13"},//the result of encrypt and serial port process,0:success 1:fail

	{PLCS14_Ask_Data_And_Send_Chip_ID_EX, "PLCS14"},

	{PLCS06_Send_C_M_SN_Format,		"PLCS06"},
	{PLSS05_Send_SN_Sig_And_Key,		"PLSS05"},
	{PLCS07_Send_C_And_SN_Format,	"PLCS07"},
	{PLSS07_Send_SN,					"PLSS07"},
	
	{PLCS08_Send_Burn_SN_Result,		"PLCS08"},

	{PLCS09_Send_C_to_Check,			"PLCS09"},//send the chip id to server to check whether there is the another same chip id in the DB
	{PLSS08_Send_Check_C_Result_OK,	"PLSS08"},//send the check result of the chip id 


	/*************commmand for B10****************/
	{PLCS16_Send_C_to_Check,           "PLCS16"},
	{PLSS16_Send_Check_OK,             "PLSS16"},
	{PLCS10_Send_BOXINFO,              "PLCS10"},
	{PLSS10_Send_OTP_MAC_HDCP_Data,    "PLSS10"},
	{PLCS15_Send_Burn_Result,          "PLCS15"},
	{PLCS17_Send_STB_INFO,             "PLCS17"},
	{PLSS18_Store_STB_INFO,            "PLSS18"}, 
	
	{PLCS18_Send_SN_TO_Check,          "PLCS18"},
	{PLSS17_Check_SN_OK,               "PLSS17"},
	/*************commmand for B10****************/
};
#endif
void format_str(char *input_data)
{
	int i;
	for(i=0;i<STRLEN(input_data);i++)
	{
		if(*(input_data+i) == '\n' || *(input_data+i) == '\r')
		{
			*(input_data+i) = '\0';
			break;
		}
	}
		
}


static COMMAND_TYPE get_command_type_by_str(char *comm_str)
{
	int i,num;

	i = 0;
	num = 0;
	
	num = sizeof(command)/sizeof(COMMAND_T);

	for(i=0;i<num;i++)
	{
		if(MEMCMP(command[i].command_str,comm_str,COMMAND_LEN) == 0)
		{
			return command[i].command_type;
		}
	}

	return (COMMAND_TYPE)0xffffffff;
}

static char *get_command_str_by_type(COMMAND_TYPE type)
{
	int i,num;

	i = 0;
	num = 0;
	
	num = sizeof(command)/sizeof(COMMAND_T);

	for(i=0;i<num;i++)
	{
		if(type == command[i].command_type)
		{
			return (char *)command[i].command_str;
		}
	}

	return NULL;
}

int parser_data(COMMAND_TYPE *type, char *input_data, char *output_data, unsigned int *output_data_len, int recv_len)
{
	char comm[COMMAND_LEN+1];
	char *p;
	TRANSMISSION_HEAD_T head_info;
	unsigned char head_size;
	unsigned int data_len;
	unsigned int CRC_read;
	unsigned int CRC_count;
	
	if(NULL == input_data || NULL == output_data)
	{
		ERROR_PRINTF("[%s]NULL pointers\n", __FUNCTION__);
		return RET_ER_DATA_IS_NULL;
	}

	head_size = sizeof(TRANSMISSION_HEAD_T);

	if(recv_len<head_size)
	{
		return RET_ER_PARSER_DATA;
	}

	//format_str(input_data);//maybe has bug!!!

	MEMCPY(&head_info,input_data,head_size);

	MEMSET(comm,0,COMMAND_LEN+1);

	MEMCPY(comm,head_info.command,COMMAND_LEN);

	*type = get_command_type_by_str(comm);

	if((*type) == 0xffffffff)//can't find the command
	{
		return RET_ER_PARSER_DATA;
	}
	
	data_len = (((head_info.data_len[0]<<24) & 0xff000000)|((head_info.data_len[1]<<16) & 0x00ff0000)|((head_info.data_len[2]<<8) & 0x0000ff00)|((head_info.data_len[3]) & 0x000000ff));

	CRC_read= (((head_info.crc[0]<<24) & 0xff000000)|((head_info.crc[1]<<16) & 0x00ff0000)|((head_info.crc[2]<<8) & 0x0000ff00)|((head_info.crc[3]) & 0x000000ff));

	if(data_len>MAX_DATA_LEN)
	{
		data_len = 0;
		return RET_ER_PARSER_DATA;
	}

	*output_data_len = data_len;
	
	p = input_data+head_size;

	//count the CRC value
	CRC_count = 0;
	CRC_count = MG_Table_Driven_CRC(0xFFFFFFFF, (unsigned char *)p,data_len);

	//if the CRC_read is 0, it means that no CRC (old way)
	if(CRC_read!=CRC_count && CRC_read!=0)
	{
		return RET_ER_PARSER_DATA;
	}
	
	MEMCPY(output_data,p,data_len);
	
	return RET_OK;
	
}

int combine_data(COMMAND_TYPE type, char *input_data, unsigned int input_data_len, char *output_data)
{
	char *command_str;
	TRANSMISSION_HEAD_T head_info;
	unsigned char head_size;
	unsigned int CRC;
	
	if(NULL == input_data || NULL == output_data)
	{
		ERROR_PRINTF("[%s]NULL pointers\n", __FUNCTION__);
		return RET_ER_DATA_IS_NULL;
	}

	if(input_data_len>MAX_DATA_LEN)
		input_data_len = 0;

	//count the data CRC value
	CRC = 0;
	CRC  = MG_Table_Driven_CRC(0xFFFFFFFF, (unsigned char *)input_data,input_data_len);
	
	head_size = sizeof(TRANSMISSION_HEAD_T);
	
	MEMSET(&head_info,0,head_size);

	command_str = get_command_str_by_type(type);

	MEMCPY(head_info.command,command_str,COMMAND_LEN);
	head_info.reseved = ':';
	
	head_info.data_len[0] = (input_data_len>>24) & 0xff;
	head_info.data_len[1] = (input_data_len>>16) & 0x00ff;
	head_info.data_len[2] = (input_data_len>>8) & 0x0000ff;
	head_info.data_len[3] = (input_data_len) & 0x000000ff;

	head_info.crc[0] = (CRC>>24) & 0xff;
	head_info.crc[1] = (CRC>>16) & 0x00ff;
	head_info.crc[2] = (CRC>>8) & 0x0000ff;
	head_info.crc[3] = (CRC) & 0x000000ff;	

	MEMCPY(output_data,&head_info,head_size);
	MEMCPY(output_data+head_size,input_data,input_data_len);

	return RET_OK;
}

