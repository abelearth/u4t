/*****************************************************************************
*    Ali Corp. All Rights Reserved. 2011 Copyright (C)
*
*    File:    ServerCommFunc.h
*
*    Description:  
*    History:
*             Date                 Athor         Version          Reason
*	  ==============	==========     =============   ===============
*	1.  2011.8.11     	peter.tan      Ver 0.1         Create file.

*
*****************************************************************************/
#ifndef _SERVERCOMMFUNC_H_
#define _SERVERCOMMFUNC_H_

#include "types_def.h"
#include "sign_api.h"

//#define COMMAND_LEN 6
#define COMMAND_NUM 64
#define DATA_LEN 2*1024
#define MAX_DATA_LEN 3*1024

#if 0
#define TRANSMISSION_HEAD_LEN 32

typedef struct _TRANSMISSION_HEAD_T
{
	char command[6];
	char separate_c;
	char	data_len[4];
	char crc[4];
	char reserve[17];
	
}TRANSMISSION_HEAD_T;


typedef enum
{
	TEST_Type = 0,
	Error_Type = 1,
	
	/*type for 3601S sign*/
	PLCS01_Ask_C_M,//ask customer+model list//no use now
	PLCS02_Send_C,//send chip ID 
	PLSS01_Send_C_M_List,//send customer+model list////no use now
	PLSS02_Send_Sig_And_Key,//send ECC sig+RSA public key
	RDCS01_Send_BootLoader_Hash,//send bootloader hash data
	RDCS02_Send_Other_Hash,//send maincode/seecode or other hash data
	RDCS03_Send_USB_Upgrade_File_Hash,//send usb upgrade file  hash data
	RDSS01_Send_Sig_And_Key_Boot,//send hash sig+ key about bootloader
	RDSS02_Send_Sig_And_Key_Main,//send hash sig+ key about maincode

	/*type for 3603 */
	PLCS11_Ask_Ca_Report,//ask ca report information
	PLCS12_Ask_Data_And_Send_Chip_ID,//ask the data(data sequence:SN,encrypted HDCP key,HDCP protection key,OTP setting,User Area) ,at the same time, send the chip id
	PLSS11_Send_Ca_Report,//send the ca report information
	PLSS12_Send_Need_Encrypt_Data,//send the data(data sequence:SN,encrypted HDCP key,HDCP protection key,OTP setting,User Area) 
	PLCS13_Send_Result,//the result of encrypt and serial port process 0:success 1:fail

	/*Add a new command for 3603*/
	PLCS14_Ask_Data_And_Send_Chip_ID_EX,

	/*type for 3601S :use for burning hdcp key*/
	PLCS03_Ask_Ca_Report,//ask ca report information
	PLSS03_Send_Ca_Report,//send the ca report information
	PLCS04_Send_Burn_HDCP_Data_Result,//the result of burning hdcp data 0:success 1:fail

	/*type for 3601S :use for RT*/
	PLCS05_Send_C_M,//send chip ID and model ID
	PLSS04_Send_RT_Data,//this data include sig and user data

	/*About SN*/
	PLCS06_Send_C_M_SN_Format,
	PLSS05_Send_SN_Sig_And_Key,

	PLCS07_Send_C_And_SN_Format,
	PLSS07_Send_SN,


	PLCS08_Send_Burn_SN_Result,

	PLCS09_Send_C_to_Check,//send the chip id to server to check whether there is the another same chip id in the DB
	PLSS08_Send_Check_C_Result_OK,//send the check result of the chip id 


	/*************commmand for B10****************/
	PLCS16_Send_C_to_Check,
	PLSS16_Send_Check_OK,
	PLCS10_Send_BOXINFO,
	PLSS10_Send_OTP_MAC_HDCP_Data,
	PLCS15_Send_Burn_Result,
	PLCS17_Send_STB_INFO,
	PLSS18_Store_STB_INFO,

	PLCS18_Send_SN_TO_Check,
	PLSS17_Check_SN_OK,
	/*************commmand for B10****************/

}COMMAND_TYPE;

typedef struct _COMMAND_T
{
	COMMAND_TYPE command_type;
	char command_str[COMMAND_LEN+1] ;
}COMMAND_T;

typedef enum
{
	RET_OK,
	RET_ER_UNKNOW,
	RET_ER_DATA_IS_NULL,
	RET_ER_INVALID_COMMAND,	
	RET_ER_MAIN_WIN_HANDEL_IS_NULL,
	RET_ER_CREATE_SEMA, RET_ER_READ_FILE,
	RET_ER_C,//can't  get correct chip id 
	RET_ER_SIG_CHIP_ID,
	RET_ER_SIG_HASH,
	RET_ER_UNKNOW_HASH_MODE,
	RET_ER_SECURE_BOOT_INIT,
	RET_ER_DATA_LEN,
	/*use for 3603*/
	RET_ER_OTP_3603_INIT,
	RET_ER_GENERATE_SN,
	RET_ER_CAN_NOT_FIND_KEY_PACKAGE_FILE,//can use for 3601 burn hdcp key function
	RET_ER_EXPORT_FILE,//can use for 3601 burn hdcp key function
	RET_ER_SAME_CHIP_ID,//can use for 3601 burn hdcp key function

	RET_ER_PARSER_DATA,
	RET_ER_GET_HDCP_KEY,
	RET_ER_C_M,//the customer id or brand error
	RET_ER_SAME_CHIP_ID_EX,//the same chip id  error, different from RET_ER_SAME_CHIP_ID.
	RET_ER_SAME_STB_ID,//the same stb id  error for cas 
	RET_ER_OTP_WRONG,//the otp data is wrong in the db

	/*************commmand for B10****************/
	RET_ER_GET_HDCP_KEY_B10,
	RET_ER_GET_OTP_KEY_B10,
	RET_ER_GET_MAC_ADDRESS_B10,
	RET_ER_CONN_DB,
	RET_ER_OPREATE_DB,
	RET_ER_B10_INIT,
	
	RET_ER_SAME_CHIP_ID_OTP,
	RET_ER_SAME_CHIP_ID_MAC,
	RET_ER_SAME_CHIP_ID_HDCP,
	
	RET_ER_SAME_CHIP_ID_OTP_EX,
	RET_ER_SAME_CHIP_ID_MAC_EX,
	RET_ER_SAME_CHIP_ID_HDCP_EX,

	RET_ERR_SN_NOT_MATCH_MODEL,
	RET_ERR_SN_NOT_IN_DB,
	RET_ERR_MAKE_NO_NOT_IN_DB,

	RET_ER_MODEL_ID_NOT_MATCH_B10,
	RET_ER_MODEL_ID_NOT_IN_DB_B10,
	RET_ER_CHIP_TYPE_NOT_MATCH_B10,

	RET_SN_ALREADY_BEEN_USED,
	
	RET_OTP_KEY_ALREADY_BEEN_USED,
	RET_HDCP_KEY_ALREADY_BEEN_USED,
	RET_ETH_MAC_ALREADY_BEEN_USED,
	
	RET_OTP_KEY_HDCP_KEY_ALREADY_BEEN_USED,
	RET_OTP_KEY_ETH_MAC_ALREADY_BEEN_USED,
	RET_HDCP_KEY_ETH_MAC_ALREADY_BEEN_USED,
	
	RET_OTP_KEY_HDCP_KEY_ETH_MAC_ALREADY_BEEN_USED,
	/*************commmand for B10****************/


}RET_CODE;
#endif

#define ERROR_PRINTF PRINTF
#define __FUNCTION__ __FILE__

int parser_data(COMMAND_TYPE *type, char *input_data, char *output_data, unsigned int *output_data_len, int recv_len);
int combine_data(COMMAND_TYPE type, char *input_data,  unsigned int input_data_len, char *output_data);
void format_str(char *input_data);


#endif
