/****************************************************************************
 * Copyright (C) 2013 Sandmartin
 ****************************************************************************/
/**
 * @file sign_api.h
 * @author Bingo.Zou, Henry.Han
 * @date 9 Oct 2013
 * @brief File containing API definition for production programing.
 */

/****************!!!data package definition!!!!******************************/
//data package is composed by: command + command_data
#ifndef _SIGN_CLIENT_H_
#define _SIGN_CLIENT_H_

#define COMMAND_LEN 6
#define COMMAND_MAX_DATA_LEN 3*1024

#define TRANSMISSION_HEAD_LEN 32
#define PL_SN_LEN 16

typedef struct _TRANSMISSION_HEAD_T
{
	char command[6];
	char reseved;
	char data_len[4];
	char crc[4];
	char reserved2[17];
}TRANSMISSION_HEAD_T;

typedef enum
{
        TEST_Type = 0,
        Error_Type = 1,

        PLCS36_Send_C_to_Check = 30,
        PLSS36_Send_Check_OK,
        PLCS30_Send_BOXINFO,
        PLSS30_Send_OTP_MAC_HDCP_Data,
        PLCS35_Send_Burn_Result,
        PLCS37_Send_STB_INFO,
        PLSS38_Store_STB_INFO,
        PLCS38_Send_SN_TO_Check,
        PLSS37_Check_SN_OK,
                    
}COMMAND_TYPE;

typedef struct _COMMAND_T
{
        COMMAND_TYPE command_type;
        char command_str[COMMAND_LEN+1] ;
}COMMAND_T;


//data structure definition
/*******************************************************/







//COMMAND "ERR000"
//DESCRIPTION: 
//LENGTH: 1 byte 
//DATA: A value of below enum.
typedef enum
{

        RET_OK = 0,
        RET_ER_UNKNOW,
        RET_ER_DATA_IS_NULL,
        RET_ER_INVALID_COMMAND,
        RET_ER_MAIN_WIN_HANDEL_IS_NULL,
        RET_ER_CREATE_SEMA,
        RET_ER_READ_FILE,
        RET_ER_C,//can't  get correct chip id 
        RET_ER_SIG_CHIP_ID,
        RET_ER_SIG_HASH,
        RET_ER_UNKNOW_HASH_MODE,
        RET_ER_SECURE_BOOT_INIT,
        RET_ER_DATA_LEN,
        RET_ER_OTP_3603_INIT,
        RET_ER_GENERATE_SN,
        RET_ER_CAN_NOT_FIND_KEY_PACKAGE_FILE,//can use for 3601 burn hdcp key function
        RET_ER_EXPORT_FILE,//can use for 3601 burn hdcp key function
        RET_ER_SAME_CHIP_ID,//can use for 3601 burn hdcp key function
        RET_ER_PARSER_DATA,
        RET_ER_GET_HDCP_KEY,
        RET_ER_C_M,//the customer id or brand error
        RET_ER_SAME_CHIP_ID_EX,//the same chip id  error, different from RET_ER_SAME_CHIP_ID.
        RET_ER_SAME_STB_ID,//the same stb id  error for cas 
        RET_ER_OTP_WRONG,//the otp data is wrong in the db
        RET_ER_GET_HDCP_KEY_B10,
        RET_ER_GET_OTP_KEY_B10,
        RET_ER_GET_MAC_ADDRESS_B10,
        RET_ER_CONN_DB,
        RET_ER_OPREATE_DB,
        RET_ER_B10_INIT,
        RET_ER_SAME_CHIP_ID_OTP,
        RET_ER_SAME_CHIP_ID_MAC,
        RET_ER_SAME_CHIP_ID_HDCP,
        RET_ER_SAME_CHIP_ID_OTP_EX,
        RET_ER_SAME_CHIP_ID_MAC_EX,
        RET_ER_SAME_CHIP_ID_HDCP_EX,
        RET_ERR_SN_NOT_MATCH_MODEL,
        RET_ERR_SN_NOT_IN_DB,
        RET_ERR_MAKE_NO_NOT_IN_DB,
        RET_ER_MODEL_ID_NOT_MATCH_B10,
        RET_ER_MODEL_ID_NOT_IN_DB_B10,
        RET_ER_CHIP_TYPE_NOT_MATCH_B10,
        RET_SN_ALREADY_BEEN_USED,
        RET_OTP_KEY_ALREADY_BEEN_USED,
        RET_HDCP_KEY_ALREADY_BEEN_USED,
        RET_ETH_MAC_ALREADY_BEEN_USED,
        RET_OTP_KEY_HDCP_KEY_ALREADY_BEEN_USED,
        RET_OTP_KEY_ETH_MAC_ALREADY_BEEN_USED,
        RET_HDCP_KEY_ETH_MAC_ALREADY_BEEN_USED,
        RET_OTP_KEY_HDCP_KEY_ETH_MAC_ALREADY_BEEN_USED,


}RET_CODE;

/**********************************************************************Command data definition*******************************************************/
//COMMAND "PLCS36"
//DESCRIPTION: Send device id to check 
//LENGTH: 16(Device ID) + 4 
//DATA: device_id(X bytes) + otp_flag(0x00) + mac_flag(0x01) + hdcp_flag(0x01) + model_id(2 bytes).

//COMMAND "PLSS36"
//DESCRIPTION: get chip id check result
//LENGTH: 1 
//DATA: 0x00

//COMMAND "PLCS30"
//DESCRIPTION: Send box id, model id to sever. Request mac and hdcp key.
//LENGTH: Not fixed 
//DATA: device_id(X bytes) + otp_flag(0x00) + mac_flag(0x01) + hdcp_flag(0x01) + model_id(2 bytes) + SN(16bytes) + length_of_order_No(1byte) + order_No(x bytes) + server_id(0x00,0x00,0x00,0x00)

//COMMAND "PLSS30"
//DESCRIPTION: get mac and hdcp key
//LENGTH: 16+6+286+17 =  325 bytes
//DATA: otp_key(16 bytes 0x00) + mac(6 bytes) + hdcp key(286 bytes) + hdcp_name(17 bytes)


//COMMAND "PLCS35"
//DESCRIPTION: send burn result
//LENGTH: 1 
//DATA: device_id(X bytes) + otp_flag(0x00) + mac_flag(0x01) + hdcp_flag(0x01)+ otp_result(0x00) + mac_result(0x01 or 0x00) + hdcp_result(0x01 or 0x00) + model_id(2 bytes) + SN(16bytes)

//COMMAND "PLCS37"
//DESCRIPTION: send all box infor (box id, model id, mac, hdcp)
//LENGTH: Not fixed 
//DATA: device_id(X bytes) + model_id(2 bytes) + chip_type(32 byte) + mac(6 bytes) + hdcp_seq_num(4 bytes int) + otp_key(16 bytes 0x00) + board_sn(18 bytes 0x00) + wifi_mac(6 bytes 0x00)+ SN(16 bytes) + order_No(0 terminated string) 

//COMMAND "PLSS38"
//DESCRIPTION: get record result
//LENGTH: 1 
//DATA: 0x00 

//COMMAND "PLCS38"
//DESCRIPTION: send serial number
//LENGTH: Not fixed
//DATA: SN(16 bytes) + length_of_order_No(1 byte) + order_No(x bytes) + otp_flag(0x00) + mac_flag(0x01) + hdcp_flag(0x01) 

//COMMAND "PLSS37"
//DESCRIPTION: get serial number check result
//LENGTH: 1 
//DATA:0x00 

/*****************************************************************************************************/

//Socket APIs usage instruction
/*
int socket(int domain, int type, int protocol); // m_hSocket=socket(PF_INET,SOCK_STREAM,0);
int connect(int sockfd, const struct sockaddr *serv_addr, socklen_t addrlen); //connect(m_hSocket,(LPSOCKADDR)&sockAddr,sizeof(sockAddr)); //port: 8000
int send(int sockfd,void *buf,int len,int flags);
int recv(int sockfd,void *buf,int len,int flags); //num=recv(m_hSocket, (LPSTR)(data_buffer), 64*1024, 0);
*/
                                                          
#endif
