/*****************************************************************************
*    Ali Corp. All Rights Reserved. 2011 Copyright (C)
*
*    File:    PlatformDef.h
*
*    Description:  
*    History:
*             Date                 Athor         Version          Reason
*	  ==============	==========     =============   ===============
*	1.  2011.8.11     	peter.tan      Ver 0.1         Create file.

*
*****************************************************************************/

#ifndef _PLATFORM_DEF_H_
#define _PLATFORM_DEF_H_

#define	WIN32_PLATFORM 	1
#define	ALI_PLATFORM 		2

#define 	USE_PLATFORM WIN32_PLATFORM

#if	(USE_PLATFORM == WIN32_PLATFORM)

#include <stdlib.h>
#include <string.h>
#include <stdio.h> 
#include <math.h> 
#include <assert.h> 

typedef unsigned int		UINT32;
typedef unsigned short		UINT16;
typedef unsigned char		UINT8;

typedef signed int			INT32;
typedef signed short		INT16;
typedef signed short		SINT16;
typedef signed char		INT8;
typedef char				CHAR;

typedef int					BOOL;

#ifdef NULL
#undef NULL  
#endif
#define NULL 			(0)

#ifdef FALSE
#undef FALSE
#endif
#define	FALSE			(0)

#ifdef TRUE
#undef TRUE
#endif
#define	TRUE			(1)

#define	MEMCMP		memcmp
#define	MEMCPY		memcpy
#define	MEMSET		memset
#define MALLOC		malloc
#define FREE		free
#define	STRLEN		strlen
#define	STRCMP		strcmp
#define	STRCAT		strcat
#define	SPRINTF		sprintf
#define STRCPY		strcpy
//#define ASSERT		assert
#define PRINTF		printf

#else

#endif

#endif //_PLATFORM_DEF_H_

