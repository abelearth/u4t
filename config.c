/*****************************************************************************/
/* Config file manipulation functions.****************************************/
/*****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <string.h>

#include <assert.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <net/if.h>
#include <sys/stat.h>

#include "hi_unf_ecs.h"
#include "hifb.h"
#include "hi_unf_disp.h"
#include "hi_unf_ecs.h"

#include "sw_debug.h"
#include "sw_fb_handle.h"
#include "ezxml/ezxml.h"
#include "config.h"

#define CONFIG_FILE "/var/SMT_XBMC/config.xml"

#define TOCKEN_BLOCK_FUN "FUNCTION"
#define TOCKEN_FUN_FRONTTYPE "FrontPanel"
#define TOCKEN_FUN_FRONTEND "FrontEnd"

#define TOCKEN_BLOCK_PROGRAM "PROGRAM"
#define TOCKEN_PROG_IMPORT "import"
#define TOCKEN_PROG_CUSTOMER "Customer"
#define TOCKEN_PROG_MODEL "Model"
#define TOCKEN_PROG_CHIPTYPE "ChipType"
#define TOCKEN_PROG_ORDERNO "Order"
#define TOCKEN_PROG_HDCP "HDCP"
#define TOCKEN_PROG_ANTICOPY "AntiCopy"
#define TOCKEN_PROG_MULTIPLE "Multiple"
#define TOCKEN_PROG_SKIN "Skin"

#define TOCKEN_BLOCK_SERVER "SERVER"
#define TOCKEN_SERVER_DB_IP "DBServerIP"
#define TOCKEN_SERVER_DB_PORT "DBServerPORT"

#define TOCKEN_BLOCK_EC "EMMC-CHECK"
#define TOCKEN_EC_DVBVER "dvb-ver"
#define TOCKEN_EC_SDKVER "sdk-ver"
#define TOCKEN_BLOCK_EC_CHUNK_GROUP "CHUNK-GROUP"
#define TOCKEN_BLOCK_EC_CHUNK "CHUNK"
#define TOCKEN_EC_CHUNK_OFFSET "offset"
#define TOCKEN_EC_CHUNK_LENGTH "length"
#define TOCKEN_EC_CHUNK_CHECKSUM "checksum"
#define TOCKEN_BLOCK_EC_FILE_GROUP "FILE-GROUP"
#define TOCKEN_BLOCK_EC_FILE "FILE"
#define TOCKEN_EC_FILE_NAME "name"
#define TOCKEN_EC_FILE_CHECKSUM "checksum"

#define TOCKEN_BLOCK_CHANNEL_GROUP "CHANNELGROUP"
#define TOCKEN_BLOCK_CHANNEL_ITEM "CHANNEL"
#define TOCKEN_CHANNEL_NAME "NAME"
#define TOCKEN_CHANNEL_FREQ "FREQ"
#define TOCKEN_CHANNEL_SR "SR"
#define TOCKEN_CHANNEL_BW "BW"
#define TOCKEN_CHANNEL_POL "POL"
#define TOCKEN_CHANNEL_LO "LO"
#define TOCKEN_CHANNEL_V_PID "V_PID"
#define TOCKEN_CHANNEL_A_PID "A_PID"
#define TOCKEN_CHANNEL_P_PID "P_PID"
#define TOCKEN_CHANNEL_VENC_FMT "VENC_FMT"
#define TOCKEN_CHANNEL_AENC_FMT "AENC_FMT"

FACTORY_CONFIG g_factory_conf;

static int config_fun(void)
{
	FILE *fp; 
	ezxml_t config;
	ezxml_t fun;
	const char* value;

	if (access(CONFIG_FILE, 0) != 0) {
		SW_ERR("no config file!\n");
		return -1;
	}

	fp = fopen(CONFIG_FILE, "r");
	if (fp == NULL){
		SW_ERR("could not open file!\n");
		return -1;
	}
	config = ezxml_parse_fp(fp);
	if (config == NULL){
		SW_ERR("could not parser config file!\n");
		return -1;
	}

	fun = ezxml_child(config, TOCKEN_BLOCK_FUN);
	if (fun == NULL){
		SW_ERR("could not get token!\n");
		return -1;
	}

	value = ezxml_attr(fun, TOCKEN_FUN_FRONTTYPE);
	if(value != NULL){
		SW_ERR("could not get token!\n");
		if(strcmp("FND", value) == 0){
			g_factory_conf.fun.front_type = 0;
		}else{
			g_factory_conf.fun.front_type = 1;
		}
	}

	value = ezxml_attr(fun, TOCKEN_FUN_FRONTEND);
	if(value != NULL){
		SW_ERR("could not get token!\n");
		if(strcmp("DVB-S2", value) == 0){
			g_factory_conf.fun.fe = DVB_S2;
		}else if(strcmp("DVB-T2", value) == 0){
			g_factory_conf.fun.fe = DVB_T2;
		}else if(strcmp("DVB-T", value) == 0){
			g_factory_conf.fun.fe = DVB_T;
		}else{
			g_factory_conf.fun.fe = DVB_S2;
        }
	}

	ezxml_free(config);
	fclose(fp);

	return 0;
}

static int config_server(void)
{
	FILE *fp; 
	ezxml_t config;
	ezxml_t item;
	const char* value;

	if (access(CONFIG_FILE, 0) != 0) {
		return -1;
	}

	fp = fopen(CONFIG_FILE, "r");
	config = ezxml_parse_fp(fp);
	if (config == NULL){
		return -1;
	}

	item = ezxml_child(config, TOCKEN_BLOCK_SERVER);
	if (item == NULL){
		return -1;
	}

	value = ezxml_attr(item, TOCKEN_SERVER_DB_IP);
	if(value != NULL){
		strcpy(g_factory_conf.server.db_ip, value);
	}

	value = ezxml_attr(item, TOCKEN_SERVER_DB_PORT);
	if(value != NULL){
		g_factory_conf.server.db_port = atoi(value);
	}

	ezxml_free(config);
	fclose(fp);

	return 0;
}
static int config_emmc_check(void)
{
	FILE *fp; 
	ezxml_t config;
	ezxml_t emmc_check;
	ezxml_t item;
	const char* value;
	int ch_id;

	if (access(CONFIG_FILE, 0) != 0) {
		return -1;
	}

	fp = fopen(CONFIG_FILE, "r");
	config = ezxml_parse_fp(fp);
	if (config == NULL){
		return -1;
	}

	emmc_check = ezxml_child(config, TOCKEN_BLOCK_EC);
	if (emmc_check == NULL){
		return -1;
	}

	value = ezxml_attr(emmc_check, TOCKEN_EC_DVBVER);
	if(value != NULL){
        strcpy(g_factory_conf.emmc_check.dvb_ver, value);
	}

	value = ezxml_attr(emmc_check, TOCKEN_EC_SDKVER);
	if(value != NULL){
        strcpy(g_factory_conf.emmc_check.sdk_ver, value);
	}

    //emmc check by chunks
    g_factory_conf.emmc_check.chunks.num = 0;
    item = ezxml_child(emmc_check, TOCKEN_BLOCK_EC_CHUNK_GROUP);
    if(item != NULL){
        item = ezxml_child(item, TOCKEN_BLOCK_EC_CHUNK);
        ch_id = 0;
        while ((item != NULL) && (ch_id < MAX_EMMC_CHUNK_NUMBER))
        {
            value = ezxml_attr(item, TOCKEN_EC_CHUNK_CHECKSUM);
            if(value != NULL){
                strcpy(g_factory_conf.emmc_check.chunks.info[ch_id].checksum, value);
            }
            value = ezxml_attr(item, TOCKEN_EC_CHUNK_OFFSET);
            if(value != NULL){
                g_factory_conf.emmc_check.chunks.info[ch_id].offset = strtol(value, NULL, 0);
            }
            value = ezxml_attr(item, TOCKEN_EC_CHUNK_LENGTH);
            if(value != NULL){
                g_factory_conf.emmc_check.chunks.info[ch_id].length = strtol(value, NULL, 0);
            }
            ch_id++;
            g_factory_conf.emmc_check.chunks.num++;
            item = ezxml_next(item);
        }
    }

    //emmc check by files
    g_factory_conf.emmc_check.files.num = 0;
    item = ezxml_child(emmc_check, TOCKEN_BLOCK_EC_FILE_GROUP);
    if(item != NULL){
        item = ezxml_child(item, TOCKEN_BLOCK_EC_FILE);
        ch_id = 0;
        while ((item != NULL) && (ch_id < MAX_EMMC_FILE_NUMBER))
        {
            value = ezxml_attr(item, TOCKEN_EC_FILE_NAME);
            if(value != NULL){
                strcpy(g_factory_conf.emmc_check.files.info[ch_id].name, value);
            }

            value = ezxml_attr(item, TOCKEN_EC_FILE_CHECKSUM);
            if(value != NULL){
                strcpy(g_factory_conf.emmc_check.files.info[ch_id].checksum, value);
            }
            ch_id++;
            g_factory_conf.emmc_check.files.num++;
            item = ezxml_next(item);
        }
    }

	ezxml_free(config);
	fclose(fp);

	return 0;
}
static int config_channel(void)
{
	FILE *fp; 
	ezxml_t config;
	ezxml_t channel;
	ezxml_t item;
	const char* value;
	int ch_id;

	if (access(CONFIG_FILE, 0) != 0) {
		return -1;
	}

	fp = fopen(CONFIG_FILE, "r");
	config = ezxml_parse_fp(fp);
	if (config == NULL){
		return -1;
	}

	g_factory_conf.channel_db.num = 0;
	channel = ezxml_child(config, TOCKEN_BLOCK_CHANNEL_GROUP);
	if (channel == NULL){
		return -1;
	}
	item = ezxml_child(channel, TOCKEN_BLOCK_CHANNEL_ITEM);
	ch_id = 0;
	while ((item != NULL) && (ch_id < MAX_CHANNEL_NUMBER))
	{
		value = ezxml_attr(item, TOCKEN_CHANNEL_NAME);
		if(value != NULL){
			strcpy(g_factory_conf.channel_db.info[ch_id].name, value);
		}
		value = ezxml_attr(item, TOCKEN_CHANNEL_FREQ);
		if(value != NULL){
			g_factory_conf.channel_db.info[ch_id].freq = strtol(value, NULL, 0);
		}
		value = ezxml_attr(item, TOCKEN_CHANNEL_SR);
		if(value != NULL){
			g_factory_conf.channel_db.info[ch_id].sr = strtol(value, NULL, 0);
		}
		value = ezxml_attr(item, TOCKEN_CHANNEL_BW);
		if(value != NULL){
			g_factory_conf.channel_db.info[ch_id].bw = strtol(value, NULL, 0);
		}
		value = ezxml_attr(item, TOCKEN_CHANNEL_POL);
		if(value != NULL){
			g_factory_conf.channel_db.info[ch_id].pol = strtol(value, NULL, 0);
		}
		value = ezxml_attr(item, TOCKEN_CHANNEL_LO);
		if(value != NULL){
			g_factory_conf.channel_db.info[ch_id].lo = strtol(value, NULL, 0);
		}
		value = ezxml_attr(item, TOCKEN_CHANNEL_A_PID);
		if(value != NULL){
			g_factory_conf.channel_db.info[ch_id].a_pid = strtol(value, NULL, 0);
		}
		value = ezxml_attr(item, TOCKEN_CHANNEL_V_PID);
		if(value != NULL){
			g_factory_conf.channel_db.info[ch_id].v_pid = strtol(value, NULL, 0);
		}
		value = ezxml_attr(item, TOCKEN_CHANNEL_P_PID);
		if(value != NULL){
			g_factory_conf.channel_db.info[ch_id].p_pid = strtol(value, NULL, 0);
		}
		value = ezxml_attr(item, TOCKEN_CHANNEL_VENC_FMT);
		if(value != NULL){
			g_factory_conf.channel_db.info[ch_id].venc_fmt = strtol(value, NULL, 0);
		}
		value = ezxml_attr(item, TOCKEN_CHANNEL_AENC_FMT);
		if(value != NULL){
			g_factory_conf.channel_db.info[ch_id].aenc_fmt = strtol(value, NULL, 0);
		}
		ch_id++;
		g_factory_conf.channel_db.num++;
		item = ezxml_next(item);
	}

	ezxml_free(config);
	fclose(fp);

	return 0;
}

static int config_program(void)
{
	FILE *fp; 
	ezxml_t config;
	ezxml_t item;
	const char* value;

	if (access(CONFIG_FILE, 0) != 0) {
		return -1;
	}

	fp = fopen(CONFIG_FILE, "r");
	config = ezxml_parse_fp(fp);
	if (config == NULL){
		return -1;
	}

	item = ezxml_child(config, TOCKEN_BLOCK_PROGRAM);
	if (item == NULL){
		return -1;
	}

	value = ezxml_attr(item, TOCKEN_PROG_IMPORT);
	if(value != NULL){
		if (strcmp(value, "0") == 0) {
			g_factory_conf.prog.import = 0;
		} else {
			g_factory_conf.prog.import = 1;
		}
	}

	value = ezxml_attr(item, TOCKEN_PROG_CHIPTYPE);
	if(value != NULL){
		strcpy(g_factory_conf.prog.chip_type, value);
	}

	value = ezxml_attr(item, TOCKEN_PROG_CUSTOMER);
	if(value != NULL){
		g_factory_conf.prog.customer = strtol(value, NULL, 0);
	}

	value = ezxml_attr(item, TOCKEN_PROG_MODEL);
	if(value != NULL){
		g_factory_conf.prog.model = strtol(value, NULL, 0);
	}

	value = ezxml_attr(item, TOCKEN_PROG_ORDERNO);
	if(value != NULL){
		strcpy(g_factory_conf.prog.order_no, value);
	}

	value = ezxml_attr(item, TOCKEN_PROG_ANTICOPY);
	if(value != NULL){
		if (strcmp(value, "0") == 0) {
			g_factory_conf.prog.anti_copy = 0;
		} else {
			g_factory_conf.prog.anti_copy = 1;
		}
	}

	value = ezxml_attr(item, TOCKEN_PROG_HDCP);
	if(value != NULL){
		if (strcmp(value, "0") == 0) {
			g_factory_conf.prog.hdcp = 0;
		} else {
			g_factory_conf.prog.hdcp = 1;
		}
	}

	value = ezxml_attr(item, TOCKEN_PROG_MULTIPLE);
	if(value != NULL){
		if (strcmp(value, "0") == 0) {
			g_factory_conf.prog.multiple = 0;
		} else {
			g_factory_conf.prog.multiple = 1;
		}
	}

	value = ezxml_attr(item, TOCKEN_PROG_SKIN);
	if(value != NULL){
		g_factory_conf.prog.skin = strtol(value, NULL, 0);
	}

	ezxml_free(config);
	fclose(fp);

	return 0;
}

int read_factory_config(void)
{
	int ret = 0;

	memset(&g_factory_conf, 0, sizeof(FACTORY_CONFIG));

	ret = config_fun();
	SW_DBG("fun ret = %d.\n", ret);
	ret |= config_program();
	SW_DBG("prog ret = %d.\n", ret);
	ret |= config_server();
	SW_DBG("server ret = %d.\n", ret);
	ret |= config_emmc_check();
	SW_DBG("emmc_check ret = %d.\n", ret);
	ret |= config_channel();
	SW_DBG("channel ret = %d.\n", ret);

	return ret;
}

void print_factory_setting(void)
{
	int i;

	SW_DBG("Got configration.\n");

	//fun
	SW_PRINT("%s\n", TOCKEN_BLOCK_FUN);
	SW_PRINT("%s: %d\n", TOCKEN_FUN_FRONTTYPE, g_factory_conf.fun.front_type);
	SW_PRINT("%s: %d\n", TOCKEN_FUN_FRONTEND, g_factory_conf.fun.fe);
	SW_PRINT("\n");

	//server
	SW_PRINT("%s\n", TOCKEN_BLOCK_SERVER);
	SW_PRINT("%s: %s\n", TOCKEN_SERVER_DB_IP, g_factory_conf.server.db_ip);
	SW_PRINT("%s: %d\n", TOCKEN_SERVER_DB_PORT, g_factory_conf.server.db_port);
	SW_PRINT("\n");

	//EMMC-CHECK
	SW_PRINT("%s\n", TOCKEN_BLOCK_EC);
	SW_PRINT("%s: %s\n", TOCKEN_EC_DVBVER, g_factory_conf.emmc_check.dvb_ver);
	SW_PRINT("%s: %s\n", TOCKEN_EC_SDKVER, g_factory_conf.emmc_check.sdk_ver);
	for(i = 0; i < g_factory_conf.emmc_check.chunks.num; i++) {
		SW_PRINT("%s: %d\n", TOCKEN_EC_CHUNK_OFFSET, g_factory_conf.emmc_check.chunks.info[i].offset);
		SW_PRINT("%s: %d\n", TOCKEN_EC_CHUNK_LENGTH, g_factory_conf.emmc_check.chunks.info[i].length);
		SW_PRINT("%s: %s\n", TOCKEN_EC_CHUNK_CHECKSUM, g_factory_conf.emmc_check.chunks.info[i].checksum);
		SW_PRINT("\n");
	}

	for(i = 0; i < g_factory_conf.emmc_check.files.num; i++) {
		SW_PRINT("%s: %s\n", TOCKEN_EC_FILE_NAME, g_factory_conf.emmc_check.files.info[i].name);
		SW_PRINT("%s: %s\n", TOCKEN_EC_FILE_CHECKSUM, g_factory_conf.emmc_check.files.info[i].checksum);
		SW_PRINT("\n");
	}
	SW_PRINT("\n");
	//channel
	SW_PRINT("%s num = %d\n", TOCKEN_BLOCK_CHANNEL_GROUP, g_factory_conf.channel_db.num);
	for(i = 0; i < g_factory_conf.channel_db.num; i++) {
		SW_PRINT("%s: %s\n", TOCKEN_CHANNEL_NAME, g_factory_conf.channel_db.info[i].name);
		SW_PRINT("%s: %d\n", TOCKEN_CHANNEL_FREQ, g_factory_conf.channel_db.info[i].freq);
		SW_PRINT("%s: %d\n", TOCKEN_CHANNEL_SR, g_factory_conf.channel_db.info[i].sr);
		SW_PRINT("%s: %d\n", TOCKEN_CHANNEL_BW, g_factory_conf.channel_db.info[i].bw);
		SW_PRINT("%s: %d\n", TOCKEN_CHANNEL_POL, g_factory_conf.channel_db.info[i].pol);
		SW_PRINT("%s: %d\n", TOCKEN_CHANNEL_LO, g_factory_conf.channel_db.info[i].lo);
		SW_PRINT("%s: %d\n", TOCKEN_CHANNEL_V_PID, g_factory_conf.channel_db.info[i].v_pid);
		SW_PRINT("%s: %d\n", TOCKEN_CHANNEL_A_PID, g_factory_conf.channel_db.info[i].a_pid);
		SW_PRINT("%s: %d\n", TOCKEN_CHANNEL_P_PID, g_factory_conf.channel_db.info[i].p_pid);
		SW_PRINT("%s: %d\n", TOCKEN_CHANNEL_VENC_FMT, g_factory_conf.channel_db.info[i].venc_fmt);
		SW_PRINT("%s: 0x%x\n", TOCKEN_CHANNEL_AENC_FMT, g_factory_conf.channel_db.info[i].aenc_fmt);
		SW_PRINT("\n");
	}
	SW_PRINT("\n");

	//program
	SW_PRINT("%s\n", TOCKEN_BLOCK_PROGRAM);
	SW_PRINT("%s: 0x%x\n", TOCKEN_PROG_IMPORT, g_factory_conf.prog.import);
	SW_PRINT("%s: 0x%x\n", TOCKEN_PROG_CUSTOMER, g_factory_conf.prog.customer);
	SW_PRINT("%s: 0x%x\n", TOCKEN_PROG_MODEL, g_factory_conf.prog.model);
	SW_PRINT("%s: %s\n", TOCKEN_PROG_CHIPTYPE, g_factory_conf.prog.chip_type);
	SW_PRINT("%s: %s\n", TOCKEN_PROG_ORDERNO, g_factory_conf.prog.order_no);
	SW_PRINT("%s: %d\n", TOCKEN_PROG_ANTICOPY, g_factory_conf.prog.anti_copy);
	SW_PRINT("%s: %d\n", TOCKEN_PROG_HDCP, g_factory_conf.prog.hdcp);
	SW_PRINT("%s: %d\n", TOCKEN_PROG_MULTIPLE, g_factory_conf.prog.multiple);
	SW_PRINT("%s: 0x%x\n", TOCKEN_PROG_SKIN, g_factory_conf.prog.skin);
	SW_PRINT("\n");
}
