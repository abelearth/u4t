#ifndef _CONFIG_H_
#define _CONFIG_H_

#define MAX_CONFIG_STRING 128
#define MAX_CHANNEL_NUMBER 8
#define CHANNEL_NAME_LEN 16
#define MAX_EMMC_CHUNK_NUMBER 8
#define MAX_EMMC_FILE_NUMBER 8
#define CHECKSUM_LENGTH 32
#define MAX_FILE_NAME_LENGTH 256

typedef	enum{
    DVB_S2,
    DVB_T,
    DVB_T2,
}FRONTEND_TYPE;

typedef	struct {
    int offset;
    int length;
	char checksum[CHECKSUM_LENGTH];
}CHUNK_INFO;

typedef	struct {
    char name[MAX_FILE_NAME_LENGTH];
	char checksum[CHECKSUM_LENGTH];
}FILE_INFO;

typedef	struct {
	char name[CHANNEL_NAME_LEN];
	int freq;
	int sr;
	int bw;
	int pol;
	int lo;
	int v_pid;
	int a_pid;
	int p_pid;
	int venc_fmt;
	int aenc_fmt;
}CHANNEL_INFO;

typedef struct _CONFIG_STRUCT {
	struct {
		int front_type;
		FRONTEND_TYPE fe;
	} fun;

	struct {
		char db_ip[MAX_CONFIG_STRING];
		int db_port;
	} server;

    struct {
        int import;
        char chip_type[MAX_CONFIG_STRING];
        unsigned short int customer;
        unsigned short int model;
        char order_no[MAX_CONFIG_STRING];
        int anti_copy;
        int hdcp;
        int multiple;
        unsigned short int skin;
    } prog;
	
    struct {
		char dvb_ver[MAX_CONFIG_STRING];
		char sdk_ver[MAX_CONFIG_STRING];
        struct {
            int num;
            CHUNK_INFO info[MAX_EMMC_CHUNK_NUMBER];
        } chunks;
        struct {
            int num;
            FILE_INFO info[MAX_EMMC_FILE_NUMBER ];
        } files;
    }emmc_check;

	struct {
		int num;
		CHANNEL_INFO info[MAX_CHANNEL_NUMBER];
	} channel_db;
} FACTORY_CONFIG;

extern FACTORY_CONFIG g_factory_conf;

int read_factory_config(void);
void print_factory_setting(void);

#endif
