/*****************************************************************************
*
*    File:   des.cpp
*
*    Description:   3DES Encryption/Decryption function
*    Reference: 
*    Data Encryption Standard http://dhost.info/pasjagor/des/index.html
*    Recommendation for the Triple Data Encryption Algorithm (TDEA) Block Cipher spec. NIST 800-67 ver 1
*    The DES Algorithm Illustrated http://billstclair.com/grabbe/des.htm

*		            
*    History:
*		Date			Author		Version			Reason
*	    ==========		========	=======			=========
*****************************************************************************/
#include <stdio.h>
#include <string.h>
#include "des.h"

/*****************************************************************************
 For  Key Schedule Calculation  
*****************************************************************************/
// permuted choice table PC-1(key) (Pg.20)
static const char PC1_Table[56] = { 57, 49, 41, 33, 25, 17,  9,
								     1, 58, 50, 42, 34, 26, 18, 
 								    10,  2, 59, 51, 43, 35, 27,
 						  		    19, 11,  3, 60, 52, 44, 36,
 								    63, 55, 47, 39, 31, 23, 15,
  								     7, 62, 54, 46, 38, 30, 22,
 								    14,  6, 61, 53, 45, 37, 29,
 								    21, 13,  5, 28, 20, 12,  4     };
// number left rotations of pc1 
static const char Left_Shift_Table[16] = { 1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1 };

// permuted choice key PC-2(table)  (Pg.21)
static const char PC2_Table[48] = {  14, 17, 11, 24,  1,  5,
									  3, 28, 15,  6, 21, 10,
									 23, 19, 12,  4, 26,  8,
									 16,  7, 27, 20, 13,  2,
									 41, 52, 31, 37, 47, 55,
									 30, 40, 51, 45, 33, 48,
									 44, 49, 39, 56, 34, 53,
									 46, 42, 50, 36, 29, 32 };

/*****************************************************************************
 For  DES Encryption Calculation  
*****************************************************************************/
// initial permutation IP (Pg.7)
static const char IP_Table[64] = { 58, 50, 42, 34, 26, 18, 10, 2,
								   60, 52, 44, 36, 28, 20, 12, 4,
								   62, 54, 46, 38, 30, 22, 14, 6,
								   64, 56, 48, 40, 32, 24, 16, 8,
								   57, 49, 41, 33, 25, 17,  9, 1, 
								   59, 51, 43, 35, 27, 19, 11, 3,
								   61, 53, 45, 37, 29, 21, 13, 5,
								   63, 55, 47, 39, 31, 23, 15, 7   };
// Bit Selection Table (Pg.10) for function f 
static const char E_Table[48] = {  32,  1,  2,  3,  4,  5,
									4,  5,  6,  7,  8,  9,
									8,  9, 10, 11, 12, 13,
								   12, 13, 14, 15, 16, 17,
								   16, 17, 18, 19, 20, 21,
								   20, 21, 22, 23, 24, 25,
								   24, 25, 26, 27, 28, 29,
								   28, 29, 30, 31, 32,  1 };

// 32-bit permutation function P used on the output of the S-boxes 
static const char P_Table[32] = { 16,  7, 20, 21,
								  29, 12, 28, 17,
								   1, 15, 23, 26,
								   5, 18, 31, 10,
 							       2,  8, 24, 14,
								  32, 27,  3,  9,
								  19, 13, 30,  6,
								  22, 11,  4,  25   };
// The (in)famous S-boxes 
static const char S_Box[8][4][16] = {
		{	// S1 
 			{14,  4, 13,  1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9,  0,  7},
  			{ 0, 15,  7,  4, 14,  2, 13,  1, 10,  6, 12, 11,  9,  5,  3,  8},
  			{ 4,  1, 14,  8, 13,  6,  2, 11, 15, 12,  9,  7,  3, 10,  5,  0},
  			{15, 12,  8,  2,  4,  9,  1,  7,  5, 11,  3, 14, 10,  0,  6, 13} 
		},
 		{	// S2 
    		{15,  1,  8, 14,  6, 11,  3,  4,  9,  7,  2, 13, 12,  0,  5, 10},
  			{ 3, 13,  4,  7, 15,  2,  8, 14, 12,  0,  1, 10,  6,  9, 11,  5},
  			{ 0, 14,  7, 11, 10,  4, 13,  1,  5,  8, 12,  6,  9,  3,  2, 15},
    		{13,  8, 10,  1,  3, 15,  4,  2, 11,  6,  7, 12,  0,  5, 14,  9}
		},
 		{ // S3 
    		{10,  0,  9, 14,  6,  3, 15,  5,  1, 13, 12,  7, 11,  4,  2,  8},
 			{13,  7,  0,  9,  3,  4,  6, 10,  2,  8,  5, 14, 12, 11, 15,  1},
			{13,  6,  4,  9,  8, 15,  3,  0, 11,  1,  2, 12,  5, 10, 14,  7},
    		{ 1, 10, 13,  0,  6,  9,  8,  7,  4, 15, 14,  3, 11,  5,  2, 12}
		},
 		{ // S4 
     		{ 7, 13, 14,  3,  0,  6,  9, 10,  1,  2,  8,  5, 11, 12,  4, 15},
 			{13,  8, 11,  5,  6, 15,  0,  3,  4,  7,  2, 12,  1, 10, 14,  9},
 			{10,  6,  9,  0, 12, 11,  7, 13, 15,  1,  3, 14,  5,  2,  8,  4},
     		{ 3, 15,  0,  6, 10,  1, 13,  8,  9,  4,  5, 11, 12,  7,  2, 14}
		},
 		{ // S5 
     		{ 2, 12,  4,  1,  7, 10, 11,  6,  8,  5,  3, 15, 13,  0, 14,  9},
 			{14, 11,  2, 12,  4,  7, 13,  1,  5,  0, 15, 10,  3,  9,  8,  6},
  			{ 4,  2,  1, 11, 10, 13,  7,  8, 15,  9, 12,  5,  6,  3,  0, 14},
    		{11,  8, 12,  7,  1, 14,  2, 13,  6, 15,  0,  9, 10,  4,  5,  3}
		},
 		{ // S6 
    		{12,  1, 10, 15,  9,  2,  6,  8,  0, 13,  3,  4, 14,  7,  5, 11},
 			{10, 15,  4,  2,  7, 12,  9,  5,  6,  1, 13, 14,  0, 11,  3,  8},
			{ 9, 14, 15,  5,  2,  8, 12,  3,  7,  0,  4, 10,  1, 13, 11,  6},
    		{ 4,  3,  2, 12,  9,  5, 15, 10, 11, 14,  1,  7,  6,  0,  8, 13}
		},
 		{ // S7 
     		{ 4, 11,  2, 14, 15,  0,  8, 13,  3, 12,  9,  7,  5, 10,  6,  1},
 			{13,  0, 11,  7,  4,  9,  1, 10, 14,  3,  5, 12,  2, 15,  8,  6},
			{ 1,  4, 11, 13, 12,  3,  7, 14, 10, 15,  6,  8,  0,  5,  9,  2},
    		{ 6, 11, 13,  8,  1,  4, 10,  7,  9,  5,  0, 15, 14,  2,  3, 12}
		},
 		{ // S8 
    		{13,  2,  8,  4,  6, 15, 11,  1, 10,  9,  3, 14,  5,  0, 12,  7},
  			{ 1, 15, 13,  8, 10,  3,  7,  4, 12,  5,  6, 11,  0, 14,  9,  2},
  			{ 7, 11,  4,  1,  9, 12, 14,  2,  0,  6, 10, 13, 15,  3,  5,  8},
    		{ 2,  1, 14,  7,  4, 10,  8, 13, 15, 12,  9,  0,  3,  5,  6, 11}
		}
};
// final permutation Inverse of IP (Pg.7) 
static const char IPR_Table[64] = { 40, 8, 48, 16, 56, 24, 64, 32, 
								    39, 7, 47, 15, 55, 23, 63, 31,
								    38, 6, 46, 14, 54, 22, 62, 30, 
								    37, 5, 45, 13, 53, 21, 61, 29,
								    36, 4, 44, 12, 52, 20, 60, 28,
								    35, 3, 43, 11, 51, 19, 59, 27,
								    34, 2, 42, 10, 50, 18, 58, 26,
								    33, 1, 41,  9, 49, 17, 57, 25   };



/*********************************************************************************************
* void Permutation(unsigned char *Out, unsigned char *In,  const char *Table, int output_len)
* Permutation Bit position change by lookup table.
* E.g. Table[index = 0] = 58 
* Output arrary [0] value is input arry[58-1].
*********************************************************************************************/
void Permutation(unsigned char *Out, unsigned char *In,  const char *Table, int output_len)
{
	int i;
	unsigned char temp;

    for(i=0; i<output_len; i++)
	{

		// lookup Input ( bit: table[i] -1 ) value is "0" or "1"
		temp = In[ (Table[i]-1)/8 ] & (0x80 >> ( (Table[i]-1)%8) );

		// Set Output (bit i) value to temp.
		if( temp != 0x00)
			Out[i/8] |= (0x80 >> ( i%8)) ;
		else
			Out[i/8] &= ~(0x80 >> ( i%8));
		
	}
}

/*********************************************************************************************
Key_Schedule (unsigned char* main_key, unsigned char *sub_key)

將 64 bits 的主鑰匙產生 16 把 48 bits 的子鑰匙 Sub_Key。 

產生方法： 
1. 64 bits Key -> Permuted Choice 1 -->56 bits Key

2. 將 56 bits Key 分成兩組 28 bits (C0 與 D0) 

	左旋運算子 (LSi) 
	Ci = LSi (Ci-1) 
	Di = LSi (Di-1) 

3. Sub_Keyi = PC-2 (Ci || Di)；   i = 1, 2, 3, …, 16。
*********************************************************************************************/
void Key_Schedule(unsigned char* main_key, unsigned char *sub_key)
{
	unsigned char C_D[7], temp; // 56bits Key
	int i;

	// Permuted choice 1
	Permutation(C_D, main_key,  PC1_Table, 56);

	for(i=0; i<16; i++) 
	{

		// Left Shift rotate
		// Ci = LSi (Ci-1) 
		temp = (C_D[0] >> (8-Left_Shift_Table[i]) ) << 4;
		C_D[0] = (C_D[0] << Left_Shift_Table[i]) | (C_D[1] >> (8-Left_Shift_Table[i]));
		C_D[1] = (C_D[1] << Left_Shift_Table[i]) | (C_D[2] >> (8-Left_Shift_Table[i]));
		C_D[2] = (C_D[2] << Left_Shift_Table[i]) | (C_D[3] >> (8-Left_Shift_Table[i]));
		C_D[3] = ((C_D[3]&0xF0) << Left_Shift_Table[i]) | temp | (C_D[3]&0x0F);

		// Di = LSi (Di-1) 
		temp = (C_D[3]&0x0F) >> (4-Left_Shift_Table[i]);
		C_D[3] = (C_D[3]&0xF0) | ((C_D[3] << Left_Shift_Table[i])& 0x0F) | (C_D[4] >> (8-Left_Shift_Table[i]));
		C_D[4] = (C_D[4] << Left_Shift_Table[i]) | (C_D[5] >> (8-Left_Shift_Table[i]));
		C_D[5] = (C_D[5] << Left_Shift_Table[i]) | (C_D[6] >> (8-Left_Shift_Table[i]));
		C_D[6] = (C_D[6] << Left_Shift_Table[i]) | temp;

		// Permuted choice 2
		Permutation(sub_key+i*6, C_D,  PC2_Table, 48);
	}
	
}

void Xor(unsigned char *InA, unsigned char *InB, int len)
{
	int i;
    for(i=0; i<len/8; i++)
        InA[i] ^= InB[i];
}

void S_func(unsigned char Sbox_Out[4], unsigned char Sbox_In[6])
{
	unsigned char temp;

	// S0 out
	temp =  (Sbox_In[0] >> 2) & 0x3F;
	Sbox_Out[0] = S_Box[0][(temp & 0x20) >> 4 | (temp & 0x01)][(temp & 0x1E) >> 1] << 4;

	// S1 out
	temp = ((Sbox_In[0]&0x03) << 4) | (Sbox_In[1] >>4) & 0x2F;
	Sbox_Out[0] |= S_Box[1][(temp & 0x20) >> 4 | (temp & 0x01)][(temp & 0x1E) >> 1];

	// S2 out
	temp = ((Sbox_In[1]&0x0F) << 2) | ((Sbox_In[2]&0xC0) >>6) & 0x2F;
	Sbox_Out[1] = S_Box[2][(temp & 0x20) >> 4 | (temp & 0x01)][(temp & 0x1E) >> 1] << 4;
	
	// S3 out
	temp = Sbox_In[2] & 0x3F;
	Sbox_Out[1] |= S_Box[3][(temp & 0x20) >> 4 | (temp & 0x01)][(temp & 0x1E) >> 1];	

	// S4 out
	temp = (Sbox_In[3] >> 2) & 0x3F;
	Sbox_Out[2] = (S_Box[4][(temp & 0x20) >> 4 | (temp & 0x01)][(temp & 0x1E) >> 1] << 4);

	// S5 out
	temp = ((Sbox_In[3]&0x03) << 4) | (Sbox_In[4] >>4) & 0x2F;
	Sbox_Out[2] |= S_Box[5][(temp & 0x20) >> 4 | (temp & 0x01)][(temp & 0x1E) >> 1];

	// S6 out
	temp = ((Sbox_In[4]&0x0F) << 2) | ((Sbox_In[5]&0xC0) >>6) & 0x2F;
	Sbox_Out[3] = (S_Box[6][(temp & 0x20) >> 4 | (temp & 0x01)][(temp & 0x1E) >> 1] << 4);

	// S7 out
	temp = Sbox_In[5] & 0x3F;
	Sbox_Out[3] |= S_Box[7][(temp & 0x20) >> 4 | (temp & 0x01)][(temp & 0x1E) >> 1];	
}

void F_func(unsigned char *R, unsigned char *sub_key)
{
	int i;
	unsigned char MR[6], Sbox_out[4];

	// E Table permutation 32bits -> 48 bits
	Permutation(MR, R,  E_Table, 48);

	// XOR with sub_key
	Xor(MR, sub_key, 48);

	// S box transformation
	S_func(Sbox_out, MR);

	// P Table permutation 32bits -> 32 bits
	Permutation(R, Sbox_out, P_Table, 32);	

}

void DES_Encryption(unsigned char *Plaintext, unsigned char *Ciphertext, unsigned char *Key)
{
	int i, j;
	unsigned char sub_key[16][6], LR[8], temp[4];
	unsigned char *Li = &LR[0], *Ri = &LR[4];

	// Key Schedule Calculation
	Key_Schedule(Key, (unsigned char*) sub_key);

	// Initial Permutation
	Permutation(LR, Plaintext,  IP_Table, 64);

	Li = &LR[0];
	Ri = &LR[4];

    for(i=0; i<16; i++)
	{
		memcpy(temp, Li, 4);			// Backup Ln-1

        memcpy(Li, Ri, 4);				// Ln = Rn-1

        F_func(Ri, &sub_key[i][0]);		// Rn' = f(Rn-1,Kn)

        Xor(Ri, temp, 32);			   // Rn = Ln-1 xor Rn'		 

	}

	// Reverse Conneting  L <-> R
	memcpy(temp, Ri, 4);
    memcpy(Ri, Li, 4);	
	memcpy(Li, temp, 4);

	// Inverse Initial Permutation
    Permutation(Ciphertext, LR,  IPR_Table, 64);

}

void DES_Decryption(unsigned char *Ciphertext, unsigned char *Plaintext, unsigned char *Key)
{
	unsigned char sub_key[16][6], LR[8], temp[6];
	unsigned char *Li = &LR[0], *Ri = &LR[4];
	int i;

	// Key Schedule Calculation
	Key_Schedule(Key, (unsigned char*) sub_key);

	// Initial Permutation
	Permutation(LR, Ciphertext,  IP_Table, 64);

	Li = &LR[4];
	Ri = &LR[0];

	// Reverse Conneting  L <-> R
	memcpy(temp, Ri, 4);
    memcpy(Ri, Li, 4);	
	memcpy(Li, temp, 4);

    for(i=15; i>=0; i--)
	{
		memcpy(temp, Ri, 4);	// Put Rn-1 to temp

		// Rn = F(Rn-1, Kn) xor Ln-1
        F_func(Ri, &sub_key[i][0]);  
        Xor(Ri, Li, 32);			 

		// Ln = Rn-1
        memcpy(Li, temp, 4);
	}

	// Inverse Initial Permutation
    Permutation(Plaintext, LR,  IPR_Table, 64);

}

void TripleDES_Encryption(unsigned char *Plaintext, unsigned char *Ciphertext, unsigned char *Key1, unsigned char *Key2, unsigned char *Key3)
{
	//DEA1-Fkey1
	DES_Encryption(Plaintext, Ciphertext, Key1);

	//DEA2-Ikey2
	DES_Decryption(Ciphertext, Plaintext, Key2);

	//DEA3-Fkey3
	DES_Encryption(Plaintext, Ciphertext, Key3);

}

void TripleDES_Decryption(unsigned char *Ciphertext, unsigned char *Plaintext, unsigned char *Key1, unsigned char *Key2, unsigned char *Key3)
{
	//DEA3-Ikey3
	DES_Decryption(Ciphertext, Plaintext, Key3);

	//DEA2-Fkey2
	DES_Encryption(Plaintext, Ciphertext, Key2);

	//DEA1-Ikey1
	DES_Decryption(Ciphertext, Plaintext, Key1);

}
