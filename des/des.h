/*****************************************************************************
*
*    File:   hdcp_des.h
*
*    Description:   3DES Encryption/Decryption function
*		            
*    History:
*		Date			Author		Version			Reason
*	    ==========		========	=======			=========
*****************************************************************************/
#ifndef __HDCP_DES_H__
#define __HDCP_DES_H__

void DES_Encryption(unsigned char *Plaintext, unsigned char *Ciphertext, unsigned char *Key);
void DES_Decryption(unsigned char *Ciphertext, unsigned char *Plaintext, unsigned char *Key);
void TripleDES_Encryption(unsigned char *Plaintext, unsigned char *Ciphertext, unsigned char *Key1, unsigned char *Key2, unsigned char *Key3);
void TripleDES_Decryption(unsigned char *Ciphertext, unsigned char *Plaintext, unsigned char *Key1, unsigned char *Key2, unsigned char *Key3);
#endif

