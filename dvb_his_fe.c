#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <assert.h>

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>


//#include <linux/dvb/frontend.h>
//#include <linux/dvb/dmx.h>

//#include "tvheadend.h"
//#include "dvb.h"
//#include "dvb_support.h"

#include "hi_unf_ecs.h"

#include "dvb_his_fe.h"
#include "sw_debug.h"

#define TUNER_ID 0
#define MN88472_DEF_ADDR        (0x00)  /* +0x38,+0x30, +0x34 */
#define MXL603T_DEF_ADDR        (0xC0)
#define MN88472_DEF_RESET_GPIO  (12)

/* For demod AVL6211 */
#define AVL6211_DEF_RESET_GPIO (12)    /* GPIO1_4 */
#define AVL6211_DEF_CLK_KHZ (27000)
//#define AVL6211_DEF_CLK_KHZ (16000)
#define AVL6211_DEF_ADDR_1 (0x0C)
#define AVL6211_DEF_ADDR_2 (0x0D)
#define AVL6211_DEF_DISEQCWAVE (HI_UNF_TUNER_DISEQCWAVE_NORMAL)
#define AVL6211_DEF_TSCLKPOLAR (HI_UNF_TUNER_TSCLK_POLAR_RISING)
#define AVL6211_DEF_TSFORMAT (HI_UNF_TUNER_TS_FORMAT_TS)
#define AVL6211_DEF_TSSERIALPIN (HI_UNF_TUNER_TS_SERIAL_PIN_7)
//#define AVL6211_DEF_TSSERIALPIN (HI_UNF_TUNER_TS_SERIAL_PIN_0)

/* For tuner AV2011 */
#define AV2011_DEF_ADDR (0xC6)
//#define AV2011_DEF_ADDR (0xC2)
#define AV2011_DEF_MAXLPF (34)
#define AV2011_DEF_I2CCLK_KHZ (100)
//#define AV2011_DEF_I2CCLK_KHZ (400)
#define AV2011_DEF_RFAGC (HI_UNF_TUNER_RFAGC_INVERT)
#define AV2011_DEF_IQSPEC (HI_UNF_TUNER_IQSPECTRUM_NORMAL)

#define HI_CHECK(ret,FUNC)   \
    if(ret != HI_SUCCESS) \
        {SW_ERR("%s(%d):%s ERROR (%x)\n",__FUNCTION__,__LINE__,#FUNC, ret);} \
    else \
        {SW_DBG("%s(%d):%s OK\n",__FUNCTION__,__LINE__,#FUNC);}

int g_lnbpower = 1; //FIXME
int g_22khz;
static int t2_frontend = 0;

int his_tuner_init(int fe_type)
{
    int ret;
    HI_UNF_TUNER_ATTR_S stTunerAttr;

#define MULIT_CONFIG_BASE 0x10203000
#define SPI_GPIO (MULIT_CONFIG_BASE + 0x0008)
	HI_SYS_WriteRegister(SPI_GPIO, 0x0); //PINMUX for TUNER SENSE
#if 0
    ret = HI_SYS_Init(); //FIXME
    HI_CHECK(ret, HI_SYS_Init);
#endif

    ret = HI_UNF_TUNER_Init();
    HI_CHECK(ret, HI_UNF_TUNER_Init);

    ret = HI_UNF_TUNER_Open(TUNER_ID);
    HI_CHECK(ret, HI_UNF_TUNER_Open);

    //set attr
	ret = HI_UNF_TUNER_GetDeftAttr(TUNER_ID, &stTunerAttr);
    if(fe_type == 0){
        //DVB-S2
        stTunerAttr.enSigType = HI_UNF_TUNER_SIG_TYPE_SAT;
        stTunerAttr.unTunerAttr.stSat.u32ResetGpioNo = AVL6211_DEF_RESET_GPIO;
        stTunerAttr.unTunerAttr.stSat.u32DemodClk = AVL6211_DEF_CLK_KHZ;
        stTunerAttr.u32DemodAddr = AVL6211_DEF_ADDR_1;
        stTunerAttr.unTunerAttr.stSat.enDiSEqCWave = AVL6211_DEF_DISEQCWAVE;
        stTunerAttr.unTunerAttr.stSat.enTSClkPolar = AVL6211_DEF_TSCLKPOLAR;
        stTunerAttr.unTunerAttr.stSat.enTSFormat = AVL6211_DEF_TSFORMAT;
        stTunerAttr.unTunerAttr.stSat.enTSSerialPIN = AVL6211_DEF_TSSERIALPIN;

        stTunerAttr.u32TunerAddr = AV2011_DEF_ADDR;
        stTunerAttr.unTunerAttr.stSat.u16TunerMaxLPF = AV2011_DEF_MAXLPF;
        stTunerAttr.unTunerAttr.stSat.u16TunerI2CClk = AV2011_DEF_I2CCLK_KHZ;
        stTunerAttr.unTunerAttr.stSat.enRFAGC = AV2011_DEF_RFAGC;
        stTunerAttr.unTunerAttr.stSat.enIQSpectrum = AV2011_DEF_IQSPEC;

        stTunerAttr.enTunerDevType = HI_UNF_TUNER_DEV_TYPE_AV2011;
        stTunerAttr.enDemodDevType = HI_UNF_DEMOD_DEV_TYPE_AVL6211;
        stTunerAttr.enI2cChannel = HI_UNF_I2C_CHANNEL_8;
        stTunerAttr.enOutputMode = HI_UNF_TUNER_OUTPUT_MODE_PARALLEL_MODE_A;

        //TODO LNB (A8293)
        stTunerAttr.unTunerAttr.stSat.enLNBCtrlDev = HI_UNF_LNBCTRL_DEV_TYPE_A8293;
        stTunerAttr.unTunerAttr.stSat.u16LNBDevAddress = 0x10;
    }else{
        if(fe_type == 1){
            stTunerAttr.enSigType 		 = HI_UNF_TUNER_SIG_TYPE_DVB_T;
        }else{
            stTunerAttr.enSigType 		 = HI_UNF_TUNER_SIG_TYPE_DVB_T2;
            t2_frontend = 1;
        }
        stTunerAttr.u32DemodAddr	 = MN88472_DEF_ADDR;
        stTunerAttr.u32TunerAddr	 = MXL603T_DEF_ADDR;
        stTunerAttr.enTunerDevType = HI_UNF_TUNER_DEV_TYPE_MXL603;
        stTunerAttr.enDemodDevType = HI_UNF_DEMOD_DEV_TYPE_MN88472;
        stTunerAttr.enI2cChannel 	 = HI_UNF_I2C_CHANNEL_8;
        stTunerAttr.enOutputMode 	 = HI_UNF_TUNER_OUTPUT_MODE_PARALLEL_MODE_A;
        stTunerAttr.unTunerAttr.stTer.u32ResetGpioNo = MN88472_DEF_RESET_GPIO;
    }

    ret = HI_UNF_TUNER_SetAttr(TUNER_ID, &stTunerAttr);
    HI_CHECK(ret,HI_UNF_TUNER_SetAttr);

    return ret;
}

int his_tuner_exit(void)
{
    int ret;

    ret = HI_UNF_TUNER_Close(TUNER_ID);
    HI_CHECK(ret, HI_UNF_TUNER_Close);
    ret = HI_UNF_TUNER_DeInit();
    HI_CHECK(ret, HI_UNF_TUNER_DeInit);

#if 0
    ret = HI_SYS_DeInit();
    HI_CHECK(ret, HI_SYS_DeInit);
#endif

    return ret;
}

unsigned int his_tuner_get_ber(void)
{
    unsigned int u32ber = 0;
    int ret;
    unsigned int au32Ber[3] = {0, };

    ret = HI_UNF_TUNER_GetBER(TUNER_ID, au32Ber);
   HI_CHECK(ret, HI_UNF_TUNER_GetBER);
    if(ret == HI_SUCCESS)
    {
        u32ber = au32Ber[0] << 16 | au32Ber[1] << 8 | au32Ber[2];
    }

    return u32ber;
}

unsigned int his_tuner_get_snr(void)
{
   int ret;
   unsigned int u32SNR = 0;

   ret = HI_UNF_TUNER_GetSNR(TUNER_ID, &u32SNR);
   HI_CHECK(ret, HI_UNF_TUNER_GetSNR);

   return u32SNR;
}

unsigned int his_tuner_get_signal_strength(void)
{
   int ret;
   unsigned int u32SignalStrength = 0;

   ret = HI_UNF_TUNER_GetSignalStrength(TUNER_ID, &u32SignalStrength);
   HI_CHECK(ret, HI_UNF_TUNER_GetSignalStrength);

   return u32SignalStrength;
}

int his_tuner_get_status(void)
{
    int ret;
    int fe_status = 0;
    //fe_status_t fe_status = 0;
    HI_UNF_TUNER_STATUS_S stTunerStatus;

    ret = HI_UNF_TUNER_GetStatus(TUNER_ID, &stTunerStatus);
    HI_CHECK(ret, HI_UNF_TUNER_GetStatus);

    if(ret == HI_SUCCESS)
    {
        if(stTunerStatus.enLockStatus == HI_UNF_TUNER_SIGNAL_LOCKED)
            fe_status = 1;
            //fe_status = FE_HAS_LOCK;
        else
            fe_status = 0; //FIXME

#if 0
        tvhlog(LOG_INFO, "dvb", "lock = %d", stTunerStatus.enLockStatus);
        tvhlog(LOG_INFO, "dvb", "freq = %d", stTunerStatus.stConnectPara.unConnectPara.stSat.u32Freq);
        tvhlog(LOG_INFO, "dvb", "symbol = %d", stTunerStatus.stConnectPara.unConnectPara.stSat.u32SymbolRate);
        tvhlog(LOG_INFO, "dvb", "H/V = %d", stTunerStatus.stConnectPara.unConnectPara.stSat.enPolar);

  uint16_t snr, signal;
  uint32_t ber;

      signal = his_tuner_get_signal_strength();
      snr = his_tuner_get_snr();
      ber = his_tuner_get_ber();
      tvhlog(LOG_INFO, "dvb", "signal = %d, snr = %d, ber = %d",signal,snr,ber);
#endif
    }

    return fe_status;
}


int his_tuner_tune(unsigned int freqKHz, unsigned int symbolRate, int polar, int timeout)
{
    int ret;
    HI_UNF_TUNER_CONNECT_PARA_S stConnectPara;

	//fprintf(stderr, "freqKHz = %d\n",freqKHz);

    stConnectPara.enSigType = HI_UNF_TUNER_SIG_TYPE_SAT;
    stConnectPara.unConnectPara.stSat.u32Freq = freqKHz;
	stConnectPara.unConnectPara.stSat.u32SymbolRate = symbolRate;
	stConnectPara.unConnectPara.stSat.enPolar = polar;

    ret = HI_UNF_TUNER_Connect(TUNER_ID, &stConnectPara, timeout);

    HI_CHECK(ret, HI_UNF_TUNER_Connect);

    return ret;
}

int his_tuner_set_lnb(int lowfreq, int hifreq, int switchfreq)
{
    int ret;
    HI_UNF_TUNER_FE_LNB_CONFIG_S stLNBConfig;
    
	stLNBConfig.u32LowLO = lowfreq/1000;

	if(hifreq)
	{
		stLNBConfig.enLNBType = HI_UNF_TUNER_FE_LNB_DUAL_FREQUENCY;
		stLNBConfig.u32HighLO = hifreq/1000;
	}
	else
	{
		stLNBConfig.enLNBType = HI_UNF_TUNER_FE_LNB_SINGLE_FREQUENCY;
		stLNBConfig.u32HighLO = lowfreq/1000;
	}

    //fprintf(stderr,"stLNBConfig.u32LowLO = %d, stLNBConfig.u32HighLO = %d\n", stLNBConfig.u32LowLO, stLNBConfig.u32HighLO);

    if(stLNBConfig.u32HighLO < 8000) //FIXME
        stLNBConfig.enLNBBand = HI_UNF_TUNER_FE_LNB_BAND_C;
    else
        stLNBConfig.enLNBBand = HI_UNF_TUNER_FE_LNB_BAND_KU;

    ret = HI_UNF_TUNER_SetLNBConfig(TUNER_ID, &stLNBConfig);
    HI_CHECK(ret, HI_UNF_TUNER_SetLNBConfig);

    return ret;
}

int his_tuner_read_uncorrected_blocks(void) //TODO
{

    return 0;
}

int check_his_frontend (int fe_fd, int dvr, int human_readable) 
{
   (void)dvr;
  int status;
  uint16_t snr, signal;
  uint32_t ber;
  int timeout = 0;

  do {
      status = his_tuner_get_status();
      signal = his_tuner_get_signal_strength();
      snr = his_tuner_get_snr();
      ber = his_tuner_get_ber();

    if (human_readable) {
      printf ("status %02x | signal %3u%% | snr %3u%% | ber %d | ",
          status, (signal * 100) / 0xffff, (snr * 100) / 0xffff, ber);
    } else {
      printf ("status %02x | signal %04x | snr %04x | ber %08x | ",
          status, signal, snr, ber);
    }
    if (status)
      printf("FE_HAS_LOCK");
    printf("\n");

    if ((status) || (++timeout >= 10))
      break;

    usleep(1000000);
  } while (1);

  return 0;
   
}

int his_tuner_set_lnb_power(int enable, int polar)
{
	int enPolar = 0;

	g_lnbpower = enable;

	enPolar = polar;

	if(enable)
		return HI_UNF_TUNER_SetLNBPower(TUNER_ID, HI_UNF_TUNER_FE_LNB_POWER_ENHANCED, enPolar);
	else
		return HI_UNF_TUNER_SetLNBPower(TUNER_ID, HI_UNF_TUNER_FE_LNB_POWER_OFF, enPolar);
}

int his_tuner_set_22khz(int enable)
{
	g_22khz = enable;

	if(enable)
		return HI_UNF_TUNER_Switch22K(TUNER_ID, HI_UNF_TUNER_SWITCH_22K_22);
	else
		return HI_UNF_TUNER_Switch22K(TUNER_ID, HI_UNF_TUNER_SWITCH_22K_0);
}

int his_tuner_ter_tune(unsigned int uRfKhz, int bwKHz, int timewait)
{
    HI_UNF_TUNER_CONNECT_PARA_S stConnectPara;
    int ret = 0;

    if (t2_frontend == 1){
        stConnectPara.enSigType = HI_UNF_TUNER_SIG_TYPE_DVB_T2;
    }else{
        stConnectPara.enSigType = HI_UNF_TUNER_SIG_TYPE_DVB_T;
    }
    stConnectPara.unConnectPara.stTer.u32Freq      = uRfKhz;
    stConnectPara.unConnectPara.stTer.u32BandWidth = bwKHz;
    stConnectPara.unConnectPara.stTer.enModType    = HI_UNF_MOD_TYPE_AUTO;
    stConnectPara.unConnectPara.stTer.bReverse     = 0;
    //stConnectPara.unConnectPara.stTer.bSignalAuto  = 0; /* futures */
    ret = HI_UNF_TUNER_Connect(TUNER_ID, &stConnectPara, timewait);
    HI_CHECK(ret, HI_UNF_TUNER_Connect);

    return ret;
}

int his_tuner_ter_antennapower(int poweron)
{
    int ret = 0;

    ret = HI_UNF_TUNER_SetAntennaPower(TUNER_ID, poweron);
    HI_CHECK(ret, HI_UNF_TUNER_SetAntennaPower);

    return ret;
}

