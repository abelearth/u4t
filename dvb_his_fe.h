#ifndef __DVB_HIS_FE_H__
#define __DVB_HIS_FE_H__

int his_tuner_init(int fe_type);
int his_tuner_exit(void);

unsigned int his_tuner_get_ber(void);
unsigned int his_tuner_get_snr(void);
unsigned int his_tuner_get_signal_strength(void);
int his_tuner_get_status(void);
int his_tuner_tune(unsigned int freqKHz, unsigned int symbolRate, int polar, int tomeout);
int his_tuner_read_uncorrected_blocks(void);
int check_his_frontend (int fe_fd, int dvr, int human_readable);
int his_tuner_set_lnb(int lowfreq, int hifreq, int switchfreq);

int his_tuner_set_lnb_power(int enable, int polar);
int his_tuner_set_22khz(int enable);
int his_tuner_ter_tune(unsigned int uRfKhz, int bwKHz, int timewait);
int his_tuner_ter_antennapower(int poweron);
#endif //__DVB_HIS_FE_H__
