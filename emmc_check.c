#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <string.h>

#include <assert.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>

#include "hi_unf_ecs.h"
#include "sw_debug.h"
#include "config.h"
#include "getline.h"

#define VERSION_FILE "/etc/version.txt"

static char g_dvb_ver[MAX_CONFIG_STRING];
static char g_sdk_ver[MAX_CONFIG_STRING];

static int version_check(char** dvb, char** sdk)
{
	FILE *fp; 
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

	fp = fopen(VERSION_FILE, "r");
	if (fp == NULL){
		SW_ERR("could not open file!\n");
        return -1;
    }

    while ((read = getline(&line, &len, fp)) != -1) {
        SW_DBG("Retrieved line of length %zu :\n", read);
        SW_DBG("%s", line);
        if(strncmp(line, "dvb = ", 6) == 0){
            strncpy(g_dvb_ver, line+6, MAX_CONFIG_STRING);
            *dvb = g_dvb_ver;
            if(*sdk != NULL){
                break;
            }
        }
        if(strncmp(line, "sdk = ", 6) == 0){
            strncpy(g_sdk_ver, line+6, MAX_CONFIG_STRING);
            *sdk = g_sdk_ver;
            if(*dvb != NULL){
                break;
            }
        }

    }

    if(line != NULL){
        free(line);
    }
    fclose(fp);

    return 0;
}

static int chunks_check(int * v_chunks)
{
	FILE *fp; 
    char buf[512];
    size_t len = 0;
    ssize_t read;
    char command[256];
    int i;

    *v_chunks = 0;

    CHUNK_INFO * info = g_factory_conf.emmc_check.chunks.info;

    for(i=0; i<g_factory_conf.emmc_check.chunks.num; i++){
        sprintf(command, "dd if=/dev/block/mmcblk0 bs=512 count=%d skip=%d | md5sum | awk '{print $1}'", info[i].length/512, info[i].offset/512);
        SW_DBG("%s\n", command);

        fp = popen(command, "r");
        if (fp == NULL){
            SW_ERR("Failed to run command!\n");
            return -1;
        }

        if(fgets(buf, sizeof(buf)-1, fp) != NULL){
            SW_DBG("buffer: %s\n", buf);
            if(strncmp(buf, info[i].checksum, 32) != 0){
                *v_chunks |= 1<<i;
            }
        }

        pclose(fp);
    }

    return 0;
}

static int files_check(int * v_files)
{
	FILE *fp; 
    char buf[512];
    size_t len = 0;
    ssize_t read;
    char command[256];
    int i;

    *v_files = 0;

    FILE_INFO * info = g_factory_conf.emmc_check.files.info;

    for(i=0; i<g_factory_conf.emmc_check.files.num; i++){
        sprintf(command, "md5sum %s | awk '{print $1}'", info[i].name);
        SW_DBG("%s\n", command);

        fp = popen(command, "r");
        if (fp == NULL){
            SW_ERR("Failed to run command!\n");
            return -1;
        }

        if(fgets(buf, sizeof(buf)-1, fp) != NULL){
            SW_DBG("buffer: %s\n", buf);
            if(strncmp(buf, info[i].checksum, 32) != 0){
                *v_files |= 1<<i;
            }
        }

        pclose(fp);
    }

    return 0;
}

int firmware_check(char** dvb, int *v_dvb, char** sdk, int*v_sdk, int* v_chunks, int* v_files)
{
    int ret;

    *dvb=NULL;
    *sdk=NULL;
    *v_chunks = -1;
    *v_files = -1;

    ret = version_check(dvb, sdk);

    if(*dvb != NULL){
        if(strstr(*dvb, g_factory_conf.emmc_check.dvb_ver) != NULL){
            *v_dvb = 0;
        }else{
            *v_dvb = 1;
        }
    }else{
        *v_dvb=-1;
    }

    if(*sdk != NULL){
        if(strstr(*sdk, g_factory_conf.emmc_check.sdk_ver) != NULL){
            *v_sdk = 0;
        }else{
            *v_sdk = 1;
        }
    }else{
        *v_sdk = -1;
    }


    ret = chunks_check(v_chunks);
    if(ret != 0){
        *v_chunks = -1;
    }

    ret = files_check(v_files);
    if(ret != 0){
        *v_files = -1;
    }
    SW_DBG("EMMC check result: %s, %d, %s, %d, %d, %d\n", *dvb, *v_dvb, *sdk, *v_sdk, *v_chunks, *v_files);

    return ret;
}

int u4t_enable_hdcp(void)
{

    int ret;

    ret = system("busybox mount -o remount /system /system");
    SW_DBG("remount system, ret = %d\n", ret);
    if(ret == 0){
        ret = system("cp -pf /var/SMT_XBMC/display.godbox.so /system/lib/hw/");
        SW_DBG("cp display.godbox.so, ret = %d\n", ret);
        system("sync");
    }

    return ret;
}
