
#ifndef _EMMC_CHECK_H_
#define _EMMC_CHECK_H_

int firmware_check(char** dvb, int *v_dvb, char** sdk, int*v_sdk, int* v_chunks, int* v_files);
int u4t_enable_hdcp(void);

#endif
