#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <assert.h>
#include <linux/fb.h>
#include <fcntl.h>

#include "hi_unf_ecs.h"
#include "dvb_his_fe.h"

#include "sw_debug.h"
#include "sw_fb_handle.h"
#include "config.h"
#include "ui.h"
#include "channel.h"

/*Factory Front Driver*/
#define FRONT_DEV_NAME      "front_misc"
#define FRONT_DEV_MINOR     80
#define FRONT_INFO_BUF_SIZE 8
#define IOC_FRONT_MAGIC     't'

typedef struct _ST_IOC_FRONT_INFO {
	unsigned long size;
	unsigned char buf[FRONT_INFO_BUF_SIZE];
} ST_IOC_FRONT_INFO;

unsigned char oldbuf;
enum {
	E_IOC_FRONT_CMD_POWER_ON = 0,
	E_IOC_FRONT_CMD_POWER_OFF,
	E_IOC_FRONT_CMD_READY,
	E_IOC_FRONT_CMD_MODE_SET,
	E_IOC_FRONT_CMD_DISPLAY,
	E_IOC_FRONT_CMD_KEYSCAN,
	E_IOC_FRONT_CMD_MAX
};

#define IOC_FRONT_CMD_POWER_ON  _IO(IOC_FRONT_MAGIC, E_IOC_FRONT_CMD_POWER_ON)
#define IOC_FRONT_CMD_POWER_OFF _IO(IOC_FRONT_MAGIC, E_IOC_FRONT_CMD_POWER_OFF)
#define IOC_FRONT_CMD_READY     _IO(IOC_FRONT_MAGIC, E_IOC_FRONT_CMD_READY)
#define IOC_FRONT_CMD_MODE_SET  _IOW(IOC_FRONT_MAGIC, E_IOC_FRONT_CMD_MODE_SET, ST_IOC_FRONT_INFO)
#define IOC_FRONT_CMD_DISPLAY   _IOW(IOC_FRONT_MAGIC, E_IOC_FRONT_CMD_DISPLAY, ST_IOC_FRONT_INFO)
#define IOC_FRONT_CMD_KEYSCAN   _IOR(IOC_FRONT_MAGIC, E_IOC_FRONT_CMD_DISPLAY, ST_IOC_FRONT_INFO)

static int g_ledtoggle = 0;	// 0=Red Led On, 1=Green Led On
static int b_fronttype = 1;	// 0=FND, 1=VFD
static int g_vfdcount = 0;	// 

static int g_fpanel_fd = 0;

void set_power_led(int status)
{
	ST_IOC_FRONT_INFO stFrontInfo;

	if (status == 0)	// Red Led On
	{
		stFrontInfo.size = 0;
		ioctl(g_fpanel_fd, IOC_FRONT_CMD_POWER_OFF, &stFrontInfo);
	} else {		// Green Led On
		stFrontInfo.size = 0;
		ioctl(g_fpanel_fd, IOC_FRONT_CMD_POWER_ON, &stFrontInfo);
	}

}

static void front_panel_display(const char *str)
{
	int ret;
	ST_IOC_FRONT_INFO stFrontInfo;
	int len;
	int max_len;

	if (str == NULL) {
		return;
	}
	len = strlen(str);

	if (g_factory_conf.fun.front_type == 0) {
		//fnd
		max_len = 5;
	} else {
		//vfd
		max_len = 8;
	}

	if (max_len > FRONT_INFO_BUF_SIZE) {
		SW_ERR
		    ("Buffer size is not enough to hold front panel display!\n");
		return;
	}

	stFrontInfo.size = max_len;
	memcpy(stFrontInfo.buf, str, (len < max_len) ? len : max_len);
	if (len < max_len) {
		memset(stFrontInfo.buf + len, 0, max_len - len);
	}

	ret = ioctl(g_fpanel_fd, IOC_FRONT_CMD_DISPLAY, &stFrontInfo);
	if (ret < 0) {
		SW_DBG("len = %d, max_len = %d, str=%s, buffer = %s\n", len,
		       max_len, str, stFrontInfo.buf);
		SW_ERR("ioctl IOC_FRONT_CMD_DISPLAY Error! ret = %d\n", ret);
	}

	return;
}

HI_VOID *FS_TaskFrontKey(HI_VOID * args)
{
	int menu_switch_key_flag = 0;
	int count = 0;
	ST_IOC_FRONT_INFO stFrontInfo;
	int ret;
#ifdef SW_DEBUG
	unsigned int i;
#endif

	g_fpanel_fd = open("/dev/" FRONT_DEV_NAME, O_RDONLY);
	if (g_fpanel_fd < 0) {
		SW_ERR("Failed to open the device file! [%s]\n",
		       FRONT_DEV_NAME);
		return NULL;
	} else {
		SW_DBG("Open the device file successfully. [%s]\n",
		       FRONT_DEV_NAME);
	}

	while (1) {
		usleep(200000);

		//switch light and display. 2 seconds cycle.
		count++;
		if (count % 10 == 0) {
			set_power_led(0);
			front_panel_display("Red");
		} else if (count % 10 == 2) {
			set_power_led(1);
			front_panel_display("Green");
		}

		stFrontInfo.size = 0;
		ret = ioctl(g_fpanel_fd, IOC_FRONT_CMD_KEYSCAN, &stFrontInfo);

		if (stFrontInfo.size == 0) {
			continue;
		} else {
#ifdef SW_DEBUG
			SW_PRINT("get keyscan: ret = %d, size = %ld\n", ret,
				 stFrontInfo.size);
			for (i = 0; i < stFrontInfo.size; i++) {
				SW_PRINT("0x%02x, ", stFrontInfo.buf[i]);
			}
			SW_PRINT("\n");
#endif
		}
		switch (stFrontInfo.buf[0]) {
		case 0x10:	//power
			if (ui_get_menu_foucus() == 0) {
				ui_switch_fpanel_key(0);
			}
			break;
		case 0x20:	//vol+
			if (ui_get_menu_foucus() == 0) {
				ui_switch_fpanel_key(1);
			} else {
				if (g_factory_conf.fun.fe == DVB_T
				    || g_factory_conf.fun.fe == DVB_T2) {
					g_antenna_power = !g_antenna_power;
					his_tuner_ter_antennapower
					    (g_antenna_power);
					ui_update_antenna_power_text
					    (g_antenna_power);
				} else if (g_factory_conf.fun.fe == DVB_S2) {
					g_lnb = (g_lnb + 1) % 3;
					ui_update_lnb_text(g_lnb);
					if (g_lnb == 0) {
						//0V
						his_tuner_set_lnb_power(0, 0);
					} else if (g_lnb == 1) {
						//13V
						his_tuner_set_lnb_power(1, 1);
					} else {
						//18V
						his_tuner_set_lnb_power(1, 0);
					}
				}
			}
			break;
		case 0x08:	//vol-
			if (ui_get_menu_foucus() == 0) {
				ui_switch_fpanel_key(2);
			} else if (g_factory_conf.fun.fe == DVB_S2) {
				g_22khzTone = (g_22khzTone + 1) % 2;
				ui_update_22k_text(g_22khzTone);
				his_tuner_set_22khz(g_22khzTone);
			}
			break;
		case 0x40:	//ch+
			if (ui_get_menu_foucus() == 0) {
				ui_switch_fpanel_key(3);
			} else {
				channel_change(1);
			}
			break;
		case 0x02:	//ch-
			if (ui_get_menu_foucus() == 0) {
				ui_switch_fpanel_key(4);
			} else {
				channel_change(-1);
			}
			break;
		case 0x04:	//menu
			if (ui_get_menu_foucus() == 0) {
				if (menu_switch_key_flag == 0) {
					menu_switch_key_flag = 1;
					ui_switch_fpanel_key(5);
				} else {
					ui_switch_menu_foucus();
					menu_switch_key_flag = 0;
				}
			} else {
				ui_switch_menu_foucus();
			}
			break;
		case 0x80:	//ok
			if (ui_get_menu_foucus() == 0) {
				ui_switch_fpanel_key(6);
			}
			break;
		}

	}

	close(g_fpanel_fd);
	g_fpanel_fd = 0;

	return NULL;
}
