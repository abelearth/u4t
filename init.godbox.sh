#!/system/bin/sh

setprop media.stagefright.enable-player false
setprop media.stagefright.enable-meta false
setprop media.stagefright.enable-scan false
setprop media.stagefright.enable-http false
setprop dalvik.vm.checkjni false
setprop persist.sys.flashplayer.1080 true

# setprop for disp_channel
setprop ro.sf.display_channel "1"

#set chip type and memory size for different version
setprop chip.type "hi3716c"
setprop total.memory "1024"

#enable 3DTV
#setprop ro.fb.3DEnable    1

#flash player install
#setprop ro.product.cpu.abi2 armeabi-v7a

REBOOT_ARG=0
/system/etc/fsck.sh /dev/block/mmcblk0p7
if [ "$?" != "0" ];then
touch /var/P7
REBOOT_ARG=1
fi
/system/etc/fsck.sh /dev/block/mmcblk0p8
if [ "$?" != "0" ];then
touch /var/P8
REBOOT_ARG=1
fi
/system/etc/fsck.sh /dev/block/mmcblk0p6
if [ "$?" != "0" ];then
touch /var/P6
REBOOT_ARG=1
fi

insmod /system/lib/modules/hi_media.ko

#DDR 1GB
/system/busybox/sbin/insmod /system/lib/modules/hi_mmz.ko mmz=vdec,0,0xB0B00000,85M:jpeg,0,0xB6000000,20M:ddr,0,0xB7400000,44M

/system/busybox/sbin/insmod /system/lib/modules/hi_common.ko LogBufSize=0x80000
# for system standby
insmod /system/lib/modules/hi_c51.ko
insmod /system/lib/modules/hi_dmac.ko
insmod /system/lib/modules/tde.ko
insmod /system/lib/modules/hi_gpio.ko
insmod /system/lib/modules/hi_ndpt.ko
insmod /system/lib/modules/hi_mpi.ko
/system/busybox/sbin/insmod /system/lib/modules/hifb.ko video="hifb:vram0_size:0, vram2_size:8000"

############# For Test Progrm  ###############
#recover from test program
if [ -f /system/bin/bootanimation.org ];then
mount -o rw,remount /system
mv /system/app/xbmcapp-armeabi-v7a-debug.apk.org /system/app/xbmcapp-armeabi-v7a-debug.apk
mv /system/bin/bootanimation.org /system/bin/bootanimation
sync
mount -o ro,remount /system
fi

#set enviroment from test program
if [ -d /proc/scsi/usb-storage ];then      
#mount and check if test program exist
mkdir -p /mnt/sdau4t                         
busybox mount /dev/block/sda1 /mnt/sdau4t    
if [ -f /mnt/sdau4t/SMT_XBMC/factory_test.sh ];then
touch /var/.debug
#rename boot animation and xbmc app.
mount -o rw,remount /system
mv /system/bin/bootanimation /system/bin/bootanimation.org
mv /system/app/xbmcapp-armeabi-v7a-debug.apk /system/app/xbmcapp-armeabi-v7a-debug.apk.org
sync
mount -o ro,remount /system

#cp test program to memory
cp -rf /mnt/sdau4t/SMT_XBMC/ /var/
chmod 755 /var/SMT_XBMC/factory_test.sh
fi

#umount
busybox umount /mnt/sdau4t
rm -r /mnt/sdau4t
fi


# jpeg not stable, use software mode
insmod /system/lib/modules/jpeg.ko
insmod /system/lib/modules/png.ko

insmod /system/lib/modules/hi_svdec.ko
insmod /system/lib/modules/hi_otp.ko
insmod /system/lib/modules/ufsd.ko
/system/busybox/sbin/insmod /system/lib/modules/hi_gpioi2c.ko gpioidclock=11 clockbit=3 gpioiddata=12 databit=5 i2cmode=2
insmod /system/lib/modules/hi_i2c.ko
#insmod /system/lib/modules/alsa_sio.ko
insmod /system/lib/modules/hi_tuner.ko
insmod /system/lib/modules/hi_sci.ko
#insmod /system/lib/modules/v4l2_video.ko video_nr=16 do_convert=0
chmod 666 /dev/video16

chmod 777 /dev/video0

chmod 666 /dev/png
chmod 666 /dev/jpeg
chmod 777 /data/app
chmod 777 /dev/hi_tde
chmod 777 /dev/hi_otp

busybox mknod /dev/galcore c 199 0

#DDR 1GB
/system/busybox/sbin/insmod /system/lib/modules/galcore.ko registerMemBase=0x60180000 irqLine=69 contiguousBase=0xBA000000 contiguousSize=0x6000000 baseAddress=0x80000000

chmod 777 /dev/hi_*
chmod 777 /dev/mmz_userdev
chmod 777 /dev/mem
busybox chmod 777 -R /dev/*
busybox chmod 772 -R /dev/log/*

echo 10240 >> /proc/sys/vm/min_free_kbytes
ln -s /dev/mtd/mtd4 /dev/mtd4

#chmod 777 /system/bin/ms
#/system/bin/ms &
insmod /system/lib/modules/hi_vinput.ko
chmod 777 /dev/vinput
#MUAPControlCenter&
#vinput_thread&

# configuration for html5 video
# true: default fullscreen; false: default inline video
setprop html5.video.fullscreen true

#for front keyboad dirver, ,defalut for 6964 in mv300 deomon
#insmod /system/lib/modules/hi_keyled.ko
#sample_keyled 3 &

############# sample test ###############
#source /system/etc/sample_insmod.sh

insmod /system/lib/modules/jpge.ko
chmod 777 /dev/jpge
busybox chmod +r /dev/graphics/fb2

insmod /system/lib/modules/sw_front.ko
chmod 777 /dev/front_misc
#/system/tvheadend/tvheadend.sh

############# For Test Progrm  ###############
if [ -f /var/SMT_XBMC/factory_test.sh ];then
killall -9 tvheadend
killall -9 oscam-1.20
sh /var/SMT_XBMC/factory_test.sh /var/SMT_XBMC
else
#insmod /system/lib/modules/hi_keypad.ko
insmod /system/lib/modules/hi_ir.ko key_hold_timeout=150 symbol_width=6000
ir_user -R 10 &
fi                            

if [ $REBOOT_ARG == "1" ];then
reboot
fi
