#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <pthread.h>
#include <assert.h>
#include <linux/fb.h>
#include <fcntl.h>

#include "hi_unf_ecs.h"
#include "sw_debug.h"
#include "sw_fb_handle.h"
#include "config.h"
#include "ui.h"
#include "storage.h"

static unsigned int get_ip(const char *devname)
{
	struct ifreq ifr;
	struct sockaddr_in sin;

	int skfd;

	memset(&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(21);
	sin.sin_addr.s_addr = inet_addr("192.168.0.1");

	skfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (skfd < 0) {
		SW_ERR("no inet socket available: %s\n", strerror(errno));
		return 0;
	}

	ifr.ifr_addr.sa_family = AF_INET;

	strcpy(ifr.ifr_name, devname);
	if (ioctl(skfd, SIOCGIFADDR, &ifr) < 0) {
		SW_ERR("no %s, %s", devname, strerror(errno));
		close(skfd);
		return 0;
	}

	close(skfd);

	memcpy(&sin, &(ifr.ifr_addr), sizeof(struct sockaddr_in));

	return (unsigned int)sin.sin_addr.s_addr;
}

void wait_dhcp(void)
{
	unsigned int ip_addr;
	char ip_string[32];
	unsigned int count = 0;

	while (1) {

		ip_addr = get_ip("eth0");
		if (ip_addr) {
			sprintf(ip_string, "%d.%d.%d.%d",
				(unsigned int)((ip_addr >> 0) & 0xFF),
				(unsigned int)((ip_addr >> 8) & 0xFF),
				(unsigned int)((ip_addr >> 16) & 0xFF),
				(unsigned int)((ip_addr >> 24) & 0xFF));

			SW_DBG("Get IP Address: %s\n", ip_string);
			ui_update_ip_text(TS_GOOD, ip_string);
			break;
		} else {
			if ((count % 4) == 0) {
				ui_update_ip_text(TS_NORMAL, "DHCP fetching");
			} else if ((count % 4) == 1) {
				ui_update_ip_text(TS_NORMAL, "DHCP fetching.");
			} else if ((count % 4) == 2) {
				ui_update_ip_text(TS_NORMAL, "DHCP fetching..");
			} else {
				ui_update_ip_text(TS_NORMAL, "DHCP fetching...");
			}
		}

		count++;
		usleep(300000);
	}

}

