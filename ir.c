#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/fb.h>

#include "hi_unf_ecs.h"
#include "hifb.h"
#include "sw_debug.h"
#include "sw_fb_handle.h"
#include "config.h"
#include "ui.h"
#include "storage.h"

#define PROTOCOL_NAME_LEN 64

int g_thread_should_stop = 0;

static int mode = 0;
static unsigned int read_timeout = 200;

void *ir_sample_thread(void *arg)
{
	int ret;
	HI_U64 key, lower, upper;
	char name[PROTOCOL_NAME_LEN + 1];
	HI_UNF_KEY_STATUS_E status;
	int dev;
	char buff;
	int fd;
	int retVal = 0;
	int key_record = 0;

	ui_update_ir_text(TS_NORMAL, "Press 123");

	while (!g_thread_should_stop) {
		if (mode != 0) {
			ret = HI_UNF_IR_GetSymbol(&lower, &upper, read_timeout);
			if (!ret) {
				SW_DBG("Received symbols [%d, %d].\n", (HI_U32)lower, (HI_U32)upper);
			}
		} else {
			ret = HI_UNF_IR_GetValueWithProtocol(&status, &key, name, PROTOCOL_NAME_LEN, read_timeout);
			if (!ret) {
				SW_DBG("Received key: 0x%.016llx, status=%d,\tprotocol: %s.\n", key, status, name);
				if(status != HI_UNF_KEY_STATUS_DOWN){
					continue;
				}
				switch(key){
					case 0xfe01ff00:
						if(key_record == 0){
							key_record = 1;
						}else{
							key_record = 0;
						}
						break;
					case 0xfd02ff00:
						if(key_record == 1){
							key_record = 2;
						}else{
							key_record = 0;
						}
						break;
					case 0xfc03ff00:
						if(key_record == 2){
							key_record = 3;
						}else{
							key_record = 0;
						}
						break;
					default: 
						key_record = 0;
						break;
				}

				switch(key_record){
					case 0:
						ui_update_ir_text(TS_NORMAL, "Press 123");
						break;
					case 1:
						ui_update_ir_text(TS_NORMAL, "1");
						break;
					case 2:
						ui_update_ir_text(TS_NORMAL, "1 2");
						break;
					case 3:
						ui_update_ir_text(TS_GOOD, "OK");
						break;
					default:
						ui_update_ir_text(TS_NORMAL, "Press 123");
						break;
				}
			}
		}
	}

	return NULL;
}

int ir_open(void)
{
	HI_S32 ret;

	//IR
	ret = HI_UNF_IR_Open();
	if (HI_SUCCESS != ret) {
		SW_ERR("Fail to open ir dev!");
		return -1;
	}

	/* key mode. */
	if (0 == mode) {

		ret = HI_UNF_IR_EnableKeyUp(1);
		if (ret) {
			SW_ERR("Fail to set keyup event!");
			return -1;
		}

		ret = HI_UNF_IR_EnableRepKey(1);
		if (ret) {
			SW_ERR("Fail to set repeat key event!");
			return -1;
		}

		ret = HI_UNF_IR_SetRepKeyTimeoutAttr(300);
		if (ret) {
			SW_ERR("Fail to set repeat interval!");
			return -1;
		}
	}

	ret = HI_UNF_IR_SetFetchMode(mode);
	if (ret) {
		SW_ERR("Fail to set key fetch mode!");
		if (ret == HI_ERR_IR_UNSUPPORT) {
			SW_WARN("You may not use s2 mode!\n");
		} else {
			return -1;
		}
	}

	ret = HI_UNF_IR_Enable(1);
	if (ret) {
		SW_ERR("Fail to set enable state of ir module!");
		return -1;
	}

	return 0;
}

void ir_close(void)
{
	HI_UNF_IR_Close();
}
