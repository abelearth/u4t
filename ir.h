
#ifndef _IR_H_
#define _IR_H_

extern int g_thread_should_stop;

void *ir_sample_thread(void *arg);
int ir_open(void);
void ir_close(void);

#endif
