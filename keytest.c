#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "des.h"

#define SW_ERR printf
#define SW_DBG printf

#define HDCP_KEY_MODE_RAW                   1
#define HDCP_KEY_MODE_HISILICON_ENCRYPT     2
#define HDCP_KEY_MODE_IDS_ENCRYPT           3

#define HDCP_KEY_MODE HDCP_KEY_MODE_IDS_ENCRYPT

//#define INTERNAL_KEYS

#if 1
unsigned char Key1[8] = {0x70, 0x13, 0xdb, 0xe9, 0x79, 0x43, 0xea, 0xd0};
unsigned char Key2[8] = {0xc0, 0xa0, 0x60, 0xce, 0x7b, 0x36, 0x82, 0x05};
unsigned char Key3[8] = {0x65, 0x34, 0x83, 0xee, 0xb9, 0x84, 0x00, 0xcb};
#else
static const unsigned char Key1[8] = { 0x27, 0x3f, 0xe1, 0xb5, 0xd7, 0x5e, 0xe3, 0xd4 };
static const unsigned char Key2[8] = { 0x8d, 0x5c, 0x34, 0xc4, 0xc6, 0x9b, 0x6a, 0x12 };
static const unsigned char Key3[8] = { 0x3e, 0xee, 0xb8, 0xe3, 0x0e, 0x8d, 0x1a, 0xc5 };
#endif

/* Each HDCP orignal bin file(308Bytes):
   Byte 0~4:      KSV(Key Selection Vector).
   Byte 5~7:      0x00.
   Byte 8~287:    Device Private Key, 40*7bytes.
   Byte 288~307:  Hash, 20bytes, NoUsed.
   */
typedef struct _RAW_KEY_ {
    unsigned char ksv[5];
    unsigned char res1[3];
    unsigned char dpk[40*7];
    unsigned char hash[20];
}RAW_KEY;

static int get_3des_keys(char* file, unsigned char* key1, unsigned char* key2, unsigned char* key3)
{
    FILE *f;
    unsigned int len;
    int i;

    f = fopen(file, "rb");
    if(f==NULL){
        SW_ERR("Open file failed: %s\n", file);
        return -1;
    }

    fseek(f, 0, SEEK_END);
    len = ftell(f);
    if(len != 24)
    {
        SW_ERR("keys file length is not correct\n");
        fclose(f);
        return -1;
    }

    fseek(f, 0, SEEK_SET);
    i = fread(key1, 1, 8, f);
    if(i != 8) {
        SW_ERR("Read file error!\n");
        fclose(f);
        return -1;
    }

    i = fread(key2, 1, 8, f);
    if(i != 8) {
        SW_ERR("Read file error!\n");
        fclose(f);
        return -1;
    }

    i = fread(key3, 1, 8, f);
    if(i != 8) {
        SW_ERR("Read file error!\n");
        fclose(f);
        return -1;
    }

    fclose(f);

    //Get real key
    for(i=0; i< 8; i++){
        key1[i] ^= 0xE1;
    }

    for(i=0; i< 8; i++){
        key2[i] ^= 0xE2;
    }

    for(i=0; i< 8; i++){
        key3[i] ^= 0xE3;
    }

    return 0;
}

static int make_3des_keys(char* file, const unsigned char* k1, const unsigned char* k2, const unsigned char* k3)
{
    int i;
    FILE *f;
    unsigned int len;
    unsigned char key1[8];
    unsigned char key2[8];
    unsigned char key3[8];

    memcpy(key1, k1, 8);
    memcpy(key2, k2, 8);
    memcpy(key3, k3, 8);

    //Get real key
    for(i=0; i< 8; i++){
        key1[i] ^= 0xE1;
    }

    for(i=0; i< 8; i++){
        key2[i] ^= 0xE2;
    }

    for(i=0; i< 8; i++){
        key3[i] ^= 0xE3;
    }

    f = fopen(file, "wb");
    if(f==NULL){
        SW_ERR("Open file failed: %s\n", file);
        return -1;
    }

    fseek(f, 0, SEEK_SET);
    i = fwrite(key1, 1, 8, f);
    if(i != 8) {
        SW_ERR("write file error!\n");
        fclose(f);
        return -1;
    }

    i = fwrite(key2, 1, 8, f);
    if(i != 8) {
        SW_ERR("write file error!\n");
        fclose(f);
        return -1;
    }

    i = fwrite(key3, 1, 8, f);
    if(i != 8) {
        SW_ERR("write file error!\n");
        fclose(f);
        return -1;
    }

    fclose(f);


}
static int repack_hdcp_key(char* keys, unsigned char *hdcp, unsigned int len)
{
    RAW_KEY raw_key;
    unsigned int i;
#if (HDCP_KEY_MODE == HDCP_KEY_MODE_IDS_ENCRYPT)
    unsigned char key1[8];
    unsigned char key2[8];
    unsigned char key3[8];
    unsigned char HDCP_pk_buf[280];
#endif


    SW_DBG("Repack hdcp key\n");
#if (HDCP_KEY_MODE == HDCP_KEY_MODE_IDS_ENCRYPT)
    /*
       Each HDCP Encryption key bin file(286ytes):
       Byte  0(1)               :0
       Byte  1~5(5)             :KSV
       Byte 6~285(280)          :TripleDES Encrypted data
       */

    SW_DBG("IDS encrypt key\n");
    if (len != 286) {
		return -1;
	}
#ifdef INTERNAL_KEYS
    memcpy(key1, Key1, 8);
    memcpy(key2, Key2, 8);
    memcpy(key3, Key3, 8);
    make_3des_keys("keys.new", key1, key2, key3);
#else
    if (0 != get_3des_keys(keys, key1, key2, key3)){
		return -1;
    }
#endif
    SW_DBG("Got des keys\n");
    printf("key1: ");
    for (i = 0; i<8; i++){
        printf("0x%0x, ", key1[i]);
    }
    printf("\nkey2: ");
    for (i = 0; i<8; i++){
        printf("0x%0x, ", key2[i]);
    }
    printf("\nkey3: ");
    for (i = 0; i<8; i++){
        printf("0x%2x, ", key3[i]);
    }
    printf("\n");
	for (i = 0; i < 280; i += 8) {
		TripleDES_Decryption(hdcp + 6 + i, HDCP_pk_buf + i, key1, key2, key3);
    }

    SW_DBG("To prepare hdcp pack\n");
    memset(&raw_key, 0, sizeof(RAW_KEY));

    /* Add KSV */
    for (i = 0; i < 5; i ++) {
        raw_key.ksv[i] = hdcp[i+1];
    }
    /* Add Device Private Key */
    for (i = 0; i < 280; i ++) {
        raw_key.dpk[i] = HDCP_pk_buf[i];
    }

#elif (HDCP_KEY_MODE == HDCP_KEY_MODE_RAW)
    /*
       Each HDCP orignal bin file(308Bytes):
       Byte 0~4:      KSV(Key Selection Vector).
       Byte 5~7:      0x00.
       Byte 8~287:    Device Private Key, 40*7bytes.
       Byte 288~307:  Hash, 20bytes, NoUsed.
       */
    SW_DBG("Origianl raw key\n");
    if (len != sizeof(RAW_KEY)) {
		return -1;
	}

    memcpy(raw_key, hdcp, len);


#else
    return -1;
#endif

    /* Add KSV */
    printf("ksv:\n");
    for (i = 0; i < 5; i ++) {
        printf("0x%2x, ", raw_key.ksv[i]);
    }

    /* Add Device Private Key */
    printf("\n");
    printf("device private key:\n");
    for (i = 0; i < 280; i ++) {
        printf("0x%02x, ", raw_key.dpk[i]);
    }
    printf("\n");
    SW_DBG("hdcp packed\n");
    return 0;
}

int main(int argc, char** argv)
{
    FILE *f;
    unsigned int len;
    unsigned char binbuf[286];
    int i;

    
    if(argc != 3){
        printf("Usage: u4tk <hdcp-file> <key-file>");
        return -1;
    }

    f = fopen(argv[1], "rb");
    if(f==NULL){
        SW_ERR("Open file failed: %s\n", argv[1]);
        return -1;
    }

    fseek(f, 0, SEEK_END);
    len = ftell(f);
    if(len != 286) {
        SW_ERR("keys file length is not correct\n");
        fclose(f);
        return -1;
    }

    fseek(f, 0, SEEK_SET);
    i = fread(binbuf, 1, 286, f);
    if(i != 286) {
        SW_ERR("Read file error!\n");
        fclose(f);
        return -1;
    }
    fclose(f);

    repack_hdcp_key(argv[2], binbuf, 286);

    return 0;
}

