#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <assert.h>
#include <fcntl.h>

#include "hi_unf_ecs.h"

#include "sw_debug.h"
#include "sw_fb_handle.h"
#include "ui.h"
#include "config.h"
#include "client/client.h"
#include "storage.h"
#include "smartcard.h"
#include "channel.h"
#include "usb.h"
#include "ip.h"
#include "front_panel.h"
#include "serial.h"
#include "emmc_check.h"

#ifdef ENABLE_IR_TEST
#include "ir.h"
#endif

#define KO_PATH_LOCAL "/var/SMT_XBMC/Driver/"
#define KO_PATH_SYS "/system/lib/modules/"

#define KO_HI_CIPER KO_PATH_SYS "hi_cipher.ko"
#define KO_HI_OTP KO_PATH_SYS "hi_otp.ko"
#define KO_HI_HDCP_READ KO_PATH_SYS "hi_sethdcp.ko" 
#define KO_HI_IR KO_PATH_SYS "hi_ir.ko"
#define KO_HI_EEPROM KO_PATH_SYS "hi_e2prom.ko"

#define KO_VFD_FRONT KO_PATH_LOCAL "sw_factory_front.ko"
#define KO_FND_FRONT KO_PATH_LOCAL "sw_factory_fnd_front.ko"

HI_BOOL g_RunFlag1, g_RunFlag2;
pthread_mutex_t g_SciMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_t task_sci_c;
pthread_t task_sci_r;
pthread_t usb_task;
pthread_t front_task;
pthread_t factory_client_task;
pthread_t serial_task;

int allow_programming(const EEPROM_CONFIG* c)
{
	int eeprom = 0;
	int anti_copy = 0;
	int hdcp = 0;
	int ret;

	if ((c->mac[0] == 0x00) && (c->mac[1] == 0x1F) && (c->mac[2] == 0x0C)) {
		eeprom = 1;
	}

#ifdef ENABLE_HDCP_PROGRAM
	if(1 == g_factory_conf.prog.hdcp){
		ret = otp_check_hdcp_key();
		SW_DBG("OTP check hdcp, ret = %d\n", ret);
		if((ret != 0) ){
			hdcp = 1;
		}
	}
#endif

	if(1 == g_factory_conf.prog.anti_copy){
		//TODO: Judge if anti_copy has been written.
		anti_copy = 1;
	}

	if ( eeprom || hdcp || anti_copy) {
		if(hdcp || anti_copy){
			return 0;
		}else{
			if(1 == g_factory_conf.prog.multiple) {
				return 1;
			}else{
				return 0;
			}
		}

	}else{
		return 1;
	}
}

int main(int argc, char *argv[])
{
	HI_S32 s32Ret = HI_FAILURE;
	int ret;
	unsigned short int customer = 0;
	unsigned short int model = 0;
    EEPROM_CONFIG conf;
    char* dvb;
    char* sdk;
    int v_dvb;
    int v_sdk;
    int v_chunks;
    int v_files;
	
#ifdef ENABLE_IR_TEST
	pthread_t thread_ir;
#endif

	SW_DBG("\nu4t program starts.\n");

	system("touch /var/.factory");

	system("rmmod hi_sethdcp");
	system("rmmod hi_cipher");
	system("rmmod hi_otp");
	system("rmmod hi_e2prom");
	system("rmmod hi_ir");
	system("rmmod sw_front");
	sleep(1);
	system("insmod " KO_HI_OTP);
	system("insmod " KO_HI_CIPER);
	system("insmod " KO_HI_HDCP_READ);
	system("insmod " KO_HI_IR);
	system("insmod " KO_HI_EEPROM);


	//read config and storage
	ret = read_factory_config();
	if (ret != 0) {
		SW_ERR("Could not load configration successfully!!\n");
		return -1;
	}
	print_factory_setting();

	if(g_factory_conf.fun.front_type == 0){
        //FND
		SW_DBG("insmod %s\n", KO_FND_FRONT);
		system("insmod " KO_FND_FRONT);
	}else{
        //VFD
		SW_DBG("insmod %s\n", KO_VFD_FRONT);
		system("insmod " KO_VFD_FRONT);
	}

    //import eeprom setting and program it by u4ee.
    if(g_factory_conf.prog.import == 1){
        SW_DBG("Import eeprom setting from file %s!\n", IMPORT_U4EE_FILE_NAME);
        system("/var/SMT_XBMC/u4ee load "IMPORT_U4EE_FILE_NAME);
    }

	//open GUI
	ui_init();

	ret = load_factory_data(&conf);
	if (ret == 0) {
		ui_update_factory_data(&conf);
	} else {
		SW_DBG("Load storage data unsuccessfully!!!\n");
		while (1) {
			ui_update_client_status(TS_BAD, "Load storage data unsuccessfully!!!");
			sleep(1);
		}
	}

#ifdef ENABLE_HDCP_PROGRAM
    ret = otp_check_hdcp_key();
    SW_DBG("OTP check hdcp, ret = %d\n", ret);
    ui_update_otp(ret);
#endif

    ret = firmware_check(&dvb, &v_dvb, &sdk, &v_sdk, &v_chunks, &v_files);
	if (ret == 0) {
		ui_update_firmware_info(dvb, v_dvb, sdk, v_sdk, v_chunks, v_files);
	} else {
		SW_DBG("Firmware check failed!!!\n");
		while (1) {
			ui_update_client_status(TS_BAD, "Firmware check failed!!!");
			sleep(1);
		}
	}

	open_channel();
    channel_change(0);

	//smart card test
	s32Ret = open_smi();
	if (HI_SUCCESS != s32Ret) {
		SW_DBG("open smi ErrorCode=0x%x\n", s32Ret);
		return -1;
	}

	pthread_mutex_init(&g_SciMutex, NULL);

	/** create thread to checkca */
	g_RunFlag1 = 1;
	s32Ret = pthread_create(&task_sci_c, NULL, CheckSci, NULL);
	if (HI_SUCCESS != s32Ret) {
		SW_DBG("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		pthread_mutex_destroy(&g_SciMutex);
	}

	/** Create thread to send and rcv data */
	g_RunFlag2 = 1;
	s32Ret = pthread_create(&task_sci_r, NULL, RunSci, NULL);
	if (HI_SUCCESS != s32Ret) {
		SW_DBG("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		g_RunFlag1 = 0;
		pthread_join(task_sci_c, NULL);
	}

	//usb test
	ret = pthread_create(&usb_task, NULL, (void *)FS_TaskUSB, NULL);
	if (0 != ret) {
		SW_DBG("hiMonitor_TaskUSB create error!\n");
	}

#ifdef DISABLE_SERIAL_TEST
#else
	//serial test
	ret = pthread_create(&serial_task, NULL, (void *)task_serial_test, NULL);
	if (0 != ret) {
		SW_DBG("hiMonitor_TaskUSB create error!\n");
	}
#endif

	//front pannel test
	ret = pthread_create(&front_task, NULL, (void *)FS_TaskFrontKey, NULL);
	if (0 != ret) {
		SW_DBG("hiMonitor_TaskFrontKey create error!\n");
	}

#ifdef ENABLE_IR_TEST
	//ir RCU test
	ret = ir_open();
	if (0 != ret) {
		SW_ERR("Open ir failed!\n");
	}
	ret = pthread_create(&thread_ir, NULL, ir_sample_thread, NULL);
	if (ret < 0) {
		perror("Failt to create thread!");
		exit(-1);
	}
#endif

	wait_dhcp();

	if(1 == allow_programming(&conf)){
		SW_DBG("Allow programming!\n");
		ret =
			pthread_create(&factory_client_task, NULL,
					(void *)FS_task_factory_client, NULL);
		if (0 != ret) {
			SW_DBG("FS_task_factory_client create error!\n");
		}
	}else{
		SW_DBG("Forbit programming!\n");
	}


	//Never quit
	while (1) {
		usleep(500000);
	}

	close_channel();

#ifdef ENABLE_IR_TEST
	g_thread_should_stop = 1;
	pthread_join(thread_ir, NULL);
	ir_close();
#endif

	g_RunFlag1 = 0;
	g_RunFlag2 = 0;
	pthread_join(task_sci_c, NULL);
	pthread_join(task_sci_r, NULL);
	pthread_mutex_destroy(&g_SciMutex);
	close_smi();

	pthread_join(usb_task, NULL);
	pthread_join(front_task, NULL);
	pthread_join(factory_client_task, NULL);

	fb_deinit();

	return 0;
}
