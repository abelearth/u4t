Customer and model list for U4 products
========================================

| customer                        | customer-value | model-name     | model-value | serial-number    |Panel | Skin       |
| :------------------------------ | --------------:| :--------------| -----------:| ----------------:|:---- |:-----------|
| BADA CO. (SEOWOO)               |          0x0001| BADA-U4        |       0x0001| U4-XBMCYYMMXXXXX | VFD  | PM3.HD     |
| BADA CO. (SEOWOO)               |          0x0001| HD-BOX AND-II  |       0x000a| HD-BOX-YYMMXXXXX | FND  | PM3.HD     |
| BADA CO. (SEOWOO)               |          0x0001| PHANTOM XBMC   |       0x000c| PHANTOMYYMMXXXXX | VFD  | PM3.HD     |
| AMIKO (SEOWOO, HUNGARY)         |          0x0002| EVO-Xfinity    |       0x0002| 61103--YYMMXXXXX | FND  | PM3.HD     |
| TEVICOM                         |          0x0003| TEVI-U4        |       0x0003| TEVI-U4YYMMXXXXX | VFD  | PM3.HD     |
| SPIDY                           |          0x0004| ECHOSONIC ALPES|       0x0004| SMARTTVYYMMXXXXX | FND  | Confluence |
| STREAM SYSTEM (SEOWOO, ALGERIA) |          0x0005| BM-HYBRID      |       0x0005|                  | VFD  | PM3.HD     |
|               (SEOWOO, Germany) |          0x0006| Ultima         |       0x0006|                  | FND  | PM3.HD     |
|                 (SEOWOO, Czech) |          0x0007| HD-Box         |       0x0007|                  | FND  | PM3.HD     |
| EUROSAT (Kuwait)                |          0x0008| Frodo          |       0x0008| FRODO--YYMMXXXXX | VFD  | PM3.HD     |
| AL-TAREEQ AL-AMEN (OpenSky)     |          0x0009| Android555     |       0x0009| OPENSKYYYMMXXXXX | VFD  | PM3.HD     |
| DIGICLASS  (Belgium)            |          0x000a| DC-850 CA IPHD |       0x000b| DC850CAYYMMXXXXX | VFD  | PM3.HD     |

