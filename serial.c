#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <pthread.h>
#include <assert.h>
#include <linux/fb.h>
#include <fcntl.h>
#include <linux/types.h>
#include <termios.h>
#include <errno.h>
#include <signal.h>

#include "hi_unf_ecs.h"


#include "sw_debug.h"
#include "sw_fb_handle.h"
#include "client/client.h"
#include "config.h"
#include "ui.h"
#include "storage.h"
#ifdef ENABLE_IR_TEST
#include "ir.h"
#endif
#include "smartcard.h"
#include "client/fast_crc.h"

extern int g_got_serial_n;
extern char g_serial_number[];

#define TEST_DATA_SEND "U4TSerialTest"
#define TEST_DATA_RECEIVE "U4TSerialAck"

#define BARCODE_TOCKEN "BarCode:"
#define BARCODE_TOCKEN_LEN 8 
#define BARCODE_LEN 16 
#define BARCODE_DATA_LEN (BARCODE_TOCKEN_LEN + BARCODE_LEN + 4)

int set_speed(int fd)
{
	int ret = 0;
	struct  termios opt;

	tcgetattr(fd, &opt);
	tcflush(fd, TCIOFLUSH);
	cfsetispeed(&opt,B115200);
	cfsetospeed(&opt,B115200);

	ret = tcsetattr(fd,TCSANOW,&opt);
	if(ret <0) {
		perror("");
	}
	tcflush(fd,TCIOFLUSH);

	return ret;
}

int set_serial_rowmode(int fd)
{
	int ret = 0;
	struct termios options; 

	tcgetattr(fd, &options);

	tcflush(fd, TCIOFLUSH);
	options.c_cflag &= ~CSIZE;
	options.c_cflag |= CS8;
	options.c_cflag &= ~PARENB;
	options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CSTOPB;
	options.c_oflag  &= ~OPOST;
	

	tcflush(fd,TCIFLUSH); 
	options.c_cc[VTIME] = 128;
	options.c_cc[VMIN] = 1;


	cfmakeraw(&options);

	//options.c_lflag |= ECHO;

	ret = tcsetattr(fd,TCSANOW,&options);
	if (ret < 0) { 
		perror(""); 
	} 

	return ret;
}

int get_bar_code(char* buf, char* bar_code)
{
	unsigned int CRC_read;
	unsigned int CRC_count;
	char* p_crc;

	if(strncmp(buf, BARCODE_TOCKEN, BARCODE_TOCKEN_LEN) == 0){
		MG_Setup_CRC_Table();
		p_crc = buf + BARCODE_TOCKEN_LEN + BARCODE_LEN;
		CRC_read= (p_crc[0]<<24) | (p_crc[1]<<16) |(p_crc[2]<<8) |(p_crc[3]);
		CRC_count = MG_Table_Driven_CRC(0xFFFFFFFF, (unsigned char *)buf, BARCODE_TOCKEN_LEN + BARCODE_LEN);
		if(CRC_read == CRC_count){
			memcpy(bar_code, buf + BARCODE_TOCKEN_LEN, BARCODE_LEN);
			bar_code[BARCODE_LEN] = 0;
			return 0;
		}else{
			return -2;
		}
	}else{
		return -1;
	}
}

HI_VOID *task_serial_test(HI_VOID * args)
{
#define BUF_SIZE 256

	int fd = -1;
	int i; 
	int ret;
	char buf[BUF_SIZE];
	char message[256];
	int serial_ok = 0;
	int barcode_ok = 0;

	SW_DBG("serial test task start\n");
	
	fd = open("/dev/ttyAMA0", O_RDWR|O_NONBLOCK);
	if(fd < 0 ){
		SW_ERR("open /dev/ttyAMA0 error!\n");
		return NULL;
	}
		
	set_speed(fd);

	set_serial_rowmode(fd);

	while((serial_ok == 0) || (barcode_ok == 0) ){
		if(serial_ok == 0){
			SW_DBG("send data to serial: %s\n", TEST_DATA_SEND);
			write(fd, TEST_DATA_SEND, strlen(TEST_DATA_SEND));
		}

		memset(buf,0,BUF_SIZE);
		ret = read(fd, buf, BUF_SIZE);
		if(ret >= 0) {
#ifdef SW_DEBUG

			{
				char* p;
				int ll;

				p = message;
				ll = sprintf(p, "read %d bytes:", ret);
				p += ll; 

				for(i = 0; i < 10; i++){
					ll = sprintf(p, "%02x.", buf[i]);
					p += ll; 
				}
				ui_update_client_status(TS_NORMAL, message);
			}
			SW_PRINT("Get [%d] bytes from serial port.\n", ret);
			for(i = 0; i < ret; i++){
				SW_PRINT("0x%02x %c, ", buf[i], buf[i]);
			}
			SW_PRINT("\n");
#endif

			if(ret == strlen(TEST_DATA_RECEIVE)){
				if(memcmp(buf, TEST_DATA_RECEIVE, strlen(TEST_DATA_RECEIVE)) == 0){
					serial_ok = 1;
					ui_update_rs232_text(TS_GOOD, "OK");
				}
			} else if(ret == BARCODE_DATA_LEN){
				int r;
				char bar_code[BARCODE_LEN + 1];

				if(barcode_ok == 0){
					r = get_bar_code(buf, bar_code);
					if(r == 0){
						SW_DBG("Got barcode!\n");
						sprintf(message, "SN: %s", bar_code);
						ui_update_client_status(TS_NORMAL, message);
						memcpy(g_serial_number, bar_code, BARCODE_LEN);
						barcode_ok  = 1;
						g_got_serial_n = 1; 
					}else{
						ui_update_client_status(TS_BAD, buf);
						SW_DBG("Wrong barcode!\n");
					}
				}
			}
		}else{
			SW_DBG("read serial error!\n");
		}

		tcflush(fd, TCIOFLUSH);

		sleep(2);
	}
	close(fd);
	return 0;
}

