#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <linux/fb.h>
#include <fcntl.h>

#include "hi_unf_ecs.h"
#include "hifb.h"
#include "hi_unf_disp.h"
#include "hi_adp_data.h"
#include "hi_unf_ecs.h"
#include "sw_fb_handle.h"
#include "sw_debug.h"
#include "client/client.h"
#include "config.h"
#include "ui.h"
#include "storage.h"
#include "smartcard.h"

extern HI_BOOL g_RunFlag1, g_RunFlag2;
extern pthread_mutex_t g_SciMutex;

//#define ADVANCED_SM_CHECK

//ATR
static const HI_U8 atr_fte[] = 		{0x3B, 0x66, 0x00, 0x00, 0x62, 0xC8, 0x01, 0x01, 0x10, 0x05}; 
static const HI_U8 atr_tvt[] = 		{0x3B, 0x66, 0x00, 0x00, 0x62, 0xC8, 0x01, 0x01, 0x80, 0x05}; 
static const HI_U8 atr_safeview[] = 	{0x3B, 0x66, 0x00, 0x00, 0x62, 0xC9, 0x01, 0x01, 0x00, 0x00}; 
static const HI_U8 atr_verimatrix[] = 	{0x3B, 0xB0, 0x36, 0x00, 0x81, 0x31, 0xFE, 0x5D, 0x95};
static const HI_U8 atr_ichd[] = 	{0x3B, 0x34, 0x94, 0x00, 0x30, 0x42, 0x30, 0x30};

#ifdef ADVANCED_SM_CHECK
/******************t0_irdeto_zeta*************************************/
static const HI_U8 t0_irdeto_zeta_send[][17] = {
	{0xd2, 0x42, 0x00, 0x00, 0x01},
	{0x1d},
	{0xd2, 0xfe, 0x00, 0x00, 0x19},
	{0xd2, 0x40, 0x00, 0x00, 0x11},
	{0x03, 0x06, 0x14, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x1d},
	{0xd2, 0xfe, 0x00, 0x00, 0x09},
	{0xd2, 0x3c, 0x00, 0x00, 0x01},
	{0x22},
	{0xd2, 0xfe, 0x00, 0x00, 0x15},
	{0xd2, 0x3e, 0x00, 0x00, 0x01},
	{0x23},
	{0xd2, 0xfe, 0x00, 0x00, 0x0b},
	{0xd2, 0x00, 0x00, 0x00, 0x01},
	{0x3c},
	{0xd2, 0xfe, 0x00, 0x00, 0x1d},
};

static const HI_U8 t0_irdeto_zeta_rcv[][32] = {
	{0x42},
	{0x90, 0x19},
	{0xfe, 0x01, 0x02, 0x00, 0x00,
	 0x21, 0x00, 0x00, 0x10, 0x03,
	 0x06, 0x14, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x1c, 0x90, 0x00},
	{0x40},
	{0x90, 0x09},
	{0xfe, 0x01, 0x02, 0x00, 0x00,
	 0x20, 0x00, 0x00, 0x00, 0x1c,
	 0x90, 0x00},
	{0x3c},
	{0x90, 0x15},
	{0xfe, 0x01, 0x02, 0x00, 0x00,
	 0x1e, 0x00, 0x00, 0x0c, 0x53,
	 0x36, 0x01, 0x0e, 0x00, 0x08,
	 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x4c, 0x90, 0x00},
	{0x3e},
	{0x90, 0x0b},
	{0xfe, 0x01, 0x02, 0x00, 0x00,
	 0x1f, 0x00, 0x00, 0x02, 0x00,
	 0x02, 0x23, 0x90, 0x00},
	{0x00},
	{0x90, 0x1d},
	{0xfe, 0x01, 0x02, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x14, 0x34,
	 0x30, 0x32, 0x30, 0x33, 0x32,
	 0x35, 0x31, 0x35, 0x32, 0x54,
	 0x36, 0x33, 0x38, 0x35, 0x30,
	 0x30, 0x30, 0x41, 0x00, 0x01,
	 0x90, 0x00},
};
static const HI_U32 t0_irdeto_zeta_send_len[15] =
    { 5, 1, 5, 5, 17, 5, 5, 1, 5, 5, 1, 5, 5, 1, 5 };
static const HI_U32 t0_irdeto_zeta_rcv_len[15] =
    { 1, 2, 28, 1, 2, 12, 1, 2, 24, 1, 2, 14, 1, 2, 32 };


static const HI_U8 atr_suantong_t1[18] = { 0x3b, 0xe9, 0x00, 0x00, 0x81,
	0x31, 0xc3, 0x45, 0x99, 0x63,
	0x74, 0x69, 0x19, 0x99, 0x12,
	0x56, 0x10, 0xec
};
static const HI_U32 atr_suantong_t1_cnt = 18;
static const HI_U8 t1_suantong_send[][23] = {	//[][6]
	{0x00, 0x00, 0x06, 0x00, 0x32, 0x10, 0x01, 0x01, 0x01, 0x25},

	//{0x00, 0x00, 0x05, 0x00, 0x84, 0x00, 0x00, 0x10, 0x91},
	{0x00, 0x00, 0x05, 0x00, 0x25, 0xa0, 0x21, 0x20, 0x81},
	{0x00, 0x00, 0x05, 0x81, 0xdd, 0x00, 0x10, 0x04, 0x4d},
	{0x00, 0x00, 0x05, 0x81, 0xd4, 0x00, 0x01, 0x05, 0x54},
	{0x00, 0x00, 0x05, 0x81, 0xd4, 0x00, 0x01, 0x0b, 0x5a},
	{0x00, 0x00, 0x05, 0x81, 0xd0, 0x00, 0x01, 0x08, 0x5d},
	{0x00, 0x00, 0x05, 0x81, 0xc0, 0x00, 0x01, 0x0a, 0x4f},
	{0x00, 0x00, 0x05, 0x00, 0x31, 0x05, 0x00, 0x08, 0x39},
	{0x00, 0x00, 0x05, 0x81, 0xd1, 0x00, 0x01, 0x10, 0x44},
	{0x00, 0x00, 0x05, 0x81, 0xd2, 0x00, 0x01, 0x10, 0x47},
	{0x00, 0x00, 0x05, 0x81, 0xa3, 0x00, 0x00, 0x05, 0x22},
	{0x00, 0x00, 0x05, 0x81, 0xa3, 0x00, 0x01, 0x05, 0x23},
	{0x00, 0x00, 0x05, 0x00, 0x25, 0x00, 0x02, 0x01, 0x23},
	{0x00, 0x00, 0x0e, 0x02, 0x68, 0x00, 0x00, 0x09, 0x00, 0x00,
	 0x00, 0x00, 0x01, 0x82, 0x02, 0x50, 0x03, 0xbf},
	{0x00, 0x00, 0x13, 0x02, 0x68, 0x00, 0x00, 0x0e, 0x00, 0x00,
	 0x00, 0x00, 0x01, 0x82, 0x07, 0x90, 0x03, 0x00, 0x00, 0x10,
	 0xa7, 0x10, 0xc7},
	{0x00, 0x00, 0x13, 0x02, 0x68, 0x00, 0x00, 0x0e, 0x00, 0x00,
	 0x00, 0x00, 0x01, 0x82, 0x07, 0x90, 0x03, 0x00, 0x00, 0x20,
	 0xa7, 0x10, 0xf7},
	{0x00, 0x00, 0x13, 0x02, 0x68, 0x00, 0x00, 0x0e, 0x00, 0x00,
	 0x00, 0x00, 0x01, 0x82, 0x07, 0x90, 0x03, 0x00, 0x00, 0x30,
	 0xa7, 0x10, 0xe7},
	{0x00, 0x00, 0x05, 0x81, 0xd4, 0x00, 0x01, 0x0b, 0x5a},
	{0x00, 0x00, 0x05, 0x00, 0x25, 0x80, 0x14, 0x01, 0xb5},
};

static const HI_U8 t1_suantong_rcv[][36] = {	//[][13]
	{0x00, 0x00, 0x02, 0x90, 0x00, 0x92},

	//{0x00, 0x00, 0x12, 0xd0, 0x9d, 0x75, 0xb4, 0xcf, 0x06, 0xd4, 0xa9, 0x5c, 0x02, 0x5f, 0x56, 0xb1, 0xa9,
	//0xf1, 0xf3, 0x90, 0x00, 0xf7},
	{0x00, 0x00, 0x02, 0x6a, 0x86, 0xee},
	{0x00, 0x00, 0x06, 0x00, 0x01, 0x00, 0x01, 0x90, 0x00, 0x96},
	{0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x01, 0x90, 0x00,
	 0x96},
	{0x00, 0x00, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x01, 0x86, 0xf1, 0x90, 0x00, 0xeb},
	{0x00, 0x00, 0x0a, 0x33, 0xc5, 0xcf, 0xc0, 0x02, 0xed, 0x02,
	 0x7b, 0x90, 0x00, 0xf5},
	{0x00, 0x00, 0x04, 0x30, 0x00, 0x90, 0x00, 0xa4},
	{0x00, 0x00, 0x0a, 0x32, 0x2e, 0x35, 0x31, 0x00, 0x00, 0x00,
	 0x00, 0x90, 0x00, 0x82},
	{0x00, 0x00, 0x12, 0x00, 0x1c, 0x6b, 0xf6, 0x14, 0x9f, 0x39,
	 0xe9, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x90,
	 0x00, 0x58},
	{0x00, 0x00, 0x12, 0x43, 0x54, 0x49, 0x5f, 0xd3, 0xc3, 0xbb,
	 0xa7, 0xbf, 0xa8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x90,
	 0x00, 0x98},
	{0x00, 0x00, 0x07, 0x1c, 0x29, 0x0b, 0x20, 0x00, 0x90, 0x00,
	 0x89},
	{0x00, 0x00, 0x07, 0x20, 0xa9, 0x0b, 0x20, 0x00, 0x90, 0x00,
	 0x35},
	{0x00, 0x00, 0x08, 0x00, 0x25, 0x00, 0x02, 0x01, 0x12, 0x90,
	 0x00, 0xac},
	{0x00, 0x00, 0x20, 0x50, 0x09, 0x1b, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x10, 0x00, 0x00, 0x20, 0x00, 0x00, 0x30, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x90, 0x00, 0xf2},
	{0x00, 0x00, 0x14, 0xa7, 0x10, 0x53, 0x54, 0x42, 0x20, 0x54,
	 0x65, 0x61, 0x6d, 0x28, 0x43, 0x41, 0x29, 0x00, 0x00, 0x00,
	 0x00, 0x90, 0x00, 0x68},
	{0x00, 0x00, 0x14, 0xa7, 0x10, 0x32, 0xb1, 0xb1, 0xbe, 0xa9,
	 0xb6, 0xfe, 0xcc, 0xa8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x90, 0x00, 0x3a},
	{0x00, 0x00, 0x14, 0xa7, 0x10, 0x33, 0xb1, 0xb1, 0xbe, 0xa9,
	 0xc8, 0xfd, 0xcc, 0xa8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x90, 0x00, 0x46},
	{0x00, 0x00, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x01, 0x86, 0xf1, 0x90, 0x00, 0xeb},
	{0x00, 0x00, 0x02, 0x6a, 0x86, 0xee},
};
static const HI_U32 t1_suantong_send_len[19] = { 10, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 18, 23, 23, 23, 9, 9 };	//{6,5,5,5};
static const HI_U32 t1_suantong_rcv_len[19] =
    { 6, 6, 10, 11, 17, 14, 8, 14, 22, 22, 11, 11, 12, 36, 24, 24, 24, 17, 6 };

/******************t14_irdeto_access*************************************/
static const HI_U8 atr_irdeto_access_t14[20] =
    { 0x3b, 0x9f, 0x21, 0xe, 0x49, 0x52, 0x44, 0x45,
	0x54, 0x4f, 0x20, 0x41, 0x43, 0x53, 0x20, 0x56,
	0x32, 0x2e, 0x32, 0x98
};
static const HI_U32 atr_irdeto_access_t14_cnt = 20;
static const HI_U8 t14_irdeto_access_send[][7] = {
	{0x1, 0x2, 0x0, 0x0, 0x0, 0x0, 0x3c},
	{0x1, 0x2, 0x1, 0x0, 0x0, 0x0, 0x3d},
	{0x1, 0x2, 0x2, 0x0, 0x0, 0x0, 0x3e},
	{0x1, 0x2, 0x8, 0x0, 0x0, 0x0, 0x34},
	{0x1, 0x2, 0x3, 0x0, 0x0, 0x0, 0x3f},
};

static const HI_U8 t14_irdeto_access_rcv[][73] = {
	{0x1, 0x2, 0x0, 0x0, 0x0, 0x0, 0x0, 0x14,
	 0x34, 0x30, 0x30, 0x32, 0x33, 0x33, 0x30, 0x34,
	 0x39, 0x34, 0x54, 0x33, 0x32, 0x34, 0x30, 0x31,
	 0x41, 0x0, 0x0, 0x0, 0x6},
	{0x1, 0x2, 0x0, 0x0, 0x1, 0x0, 0x0, 0x10,
	 0xff, 0xff, 0xff, 0x0, 0x0, 0x0, 0x0, 0x0,
	 0x0, 0x0, 0x4, 0x7, 0x37, 0x94, 0xe6, 0x18,
	 0x8c},
	{0x1, 0x2, 0x0, 0x0, 0x2, 0x0, 0x0, 0x10,
	 0x4, 0x6, 0x12, 0x6, 0x23, 0x6, 0x24, 0x6,
	 0x25, 0x6, 0x26, 0x0, 0x0, 0x43, 0x48, 0x4e,
	 0x7f},
	{0x1, 0x2, 0x0, 0x0, 0x8, 0x0, 0x0, 0x40,
	 0x0, 0x1, 0x8, 0x20, 0x0, 0x15, 0x60, 0x2,
	 0x60, 0x0, 0x0, 0x3, 0x81, 0x0, 0x0, 0x0,
	 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	 0xc8},
	{0x1, 0x2, 0x0, 0x0, 0x3, 0x0, 0x0, 0x18,
	 0x0, 0x3, 0xe9, 0x97, 0x0, 0x0, 0x0, 0x0,
	 0x0, 0x0, 0x2, 0x11, 0x1, 0x1, 0x0, 0x0,
	 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	 0x49},
};
static const HI_U32 t14_irdeto_access_send_len[5] = { 7, 7, 7, 7, 7 };
static const HI_U32 t14_irdeto_access_rcv_len[5] = { 29, 25, 25, 73, 33 };

/********************t0_crypto_send*******************************/
static const HI_U8 t0_crypto_send[][5] = {
	{0xa4, 0xc0, 0x00, 0x00, 0x11},
	{0xa4, 0xa4, 0x00, 0x00, 0x02},
	{0x3f, 0x00},
	{0xa4, 0xa4, 0x00, 0x00, 0x02},
	{0x2f, 0x01},
	{0xa4, 0xa2, 0x00, 0x00, 0x01},
	{0xd1},
};
static const HI_S32 t0_crypto_send_len[7] = { 5, 5, 2, 5, 2, 5, 1 };

static const HI_U8 t0_crypto_rcv[][20] = {
	{0xc0, 0xdf, 0x0f, 0x05, 0x04, 0x00, 0x09, 0x3f, 0x00, 0x01,
	 0x00, 0xf0, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x90, 0x00},
	{0xa4},
	{0x9f, 0x11},
	{0xa4},
	{0x9f, 0x11},
	{0xa2},
	{0x9f, 0x04},
};
static const HI_S32 t0_crypto_rcv_len[7] = { 20, 1, 2, 1, 2, 1, 2 };

#endif

/********************************************************************/

/********************************************************************/
#ifdef DONT_UNDERSTAND_THIS_PIECE_OF_CODE
static const char Left_Shift_Table[16] =
    { 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 };

static const char PC1_Table[56] = { 57, 49, 41, 33, 25, 17, 9,
	1, 58, 50, 42, 34, 26, 18,
	10, 2, 59, 51, 43, 35, 27,
	19, 11, 3, 60, 52, 44, 36,
	63, 55, 47, 39, 31, 23, 15,
	7, 62, 54, 46, 38, 30, 22,
	14, 6, 61, 53, 45, 37, 29,
	21, 13, 5, 28, 20, 12, 4
};

static const char PC2_Table[48] = { 14, 17, 11, 24, 1, 5,
	3, 28, 15, 6, 21, 10,
	23, 19, 12, 4, 26, 8,
	16, 7, 27, 20, 13, 2,
	41, 52, 31, 37, 47, 55,
	30, 40, 51, 45, 33, 48,
	44, 49, 39, 56, 34, 53,
	46, 42, 50, 36, 29, 32
};

static const char IP_Table[64] = { 58, 50, 42, 34, 26, 18, 10, 2,
	60, 52, 44, 36, 28, 20, 12, 4,
	62, 54, 46, 38, 30, 22, 14, 6,
	64, 56, 48, 40, 32, 24, 16, 8,
	57, 49, 41, 33, 25, 17, 9, 1,
	59, 51, 43, 35, 27, 19, 11, 3,
	61, 53, 45, 37, 29, 21, 13, 5,
	63, 55, 47, 39, 31, 23, 15, 7
};

static const char E_Table[48] = { 32, 1, 2, 3, 4, 5,
	4, 5, 6, 7, 8, 9,
	8, 9, 10, 11, 12, 13,
	12, 13, 14, 15, 16, 17,
	16, 17, 18, 19, 20, 21,
	20, 21, 22, 23, 24, 25,
	24, 25, 26, 27, 28, 29,
	28, 29, 30, 31, 32, 1
};

static const char P_Table[32] = { 16, 7, 20, 21,
	29, 12, 28, 17,
	1, 15, 23, 26,
	5, 18, 31, 10,
	2, 8, 24, 14,
	32, 27, 3, 9,
	19, 13, 30, 6,
	22, 11, 4, 25
};

static const char S_Box[8][4][16] = {

	{			// S1 
	 {14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7},
	 {0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8},
	 {4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0},
	 {15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13}
	 },

	{			// S2 
	 {15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10},
	 {3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5},
	 {0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15},
	 {13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9}
	 },

	{			// S3 
	 {10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8},
	 {13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1},
	 {13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7},
	 {1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12}
	 },

	{			// S4 
	 {7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15},
	 {13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9},
	 {10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4},
	 {3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14}
	 },

	{			// S5 
	 {2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9},
	 {14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6},
	 {4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14},
	 {11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3}
	 },

	{			// S6 
	 {12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11},
	 {10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8},
	 {9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6},
	 {4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13}
	 },

	{			// S7 
	 {4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1},
	 {13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6},
	 {1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2},
	 {6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12}
	 },

	{			// S8 
	 {13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7},
	 {1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2},
	 {7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8},
	 {2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11}
	 }
};

static const char IPR_Table[64] = { 40, 8, 48, 16, 56, 24, 64, 32,
	39, 7, 47, 15, 55, 23, 63, 31,
	38, 6, 46, 14, 54, 22, 62, 30,
	37, 5, 45, 13, 53, 21, 61, 29,
	36, 4, 44, 12, 52, 20, 60, 28,
	35, 3, 43, 11, 51, 19, 59, 27,
	34, 2, 42, 10, 50, 18, 58, 26,
	33, 1, 41, 9, 49, 17, 57, 25
};
#endif

#define ACTION_CARDIN 0		/* card inset flag */
#define ACTION_CARDOUT 1	/* card pull out flag */
#define ACTION_NONE 2		/* card immobile */

static HI_U8 g_u8SCIPort;
static HI_U8 g_u8WarmReset;

static HI_BOOL g_bCardStatus = HI_FALSE;	/*HI_TRUE, card in; HI_FALSE, no card */
static HI_U8 g_CardAction = ACTION_NONE;

static HI_U32 u32ClkMode;
static HI_U32 u32Freq;
static HI_U32 u32Level, u32Detect;
static HI_UNF_SCI_CLK_MODE_E enClkMode;
static HI_UNF_SCI_PROTOCOL_E enProtocolType;
static HI_UNF_SCI_LEVEL_E enSciLevel;

#ifdef ADVANCED_SM_CHECK
static HI_U32 g_u32MaxSndLen;
static HI_U32 g_u32SndTimes;
static HI_U32 *g_pSndLen;
static HI_U8 *g_pSndBuf;

static HI_U32 g_u32MaxRcvLen;
static HI_U32 *g_pRcvLen;
static HI_U8 *g_pRcvBuf;
static HI_BOOL g_bDropWord;

static char rcv_buf[512] = { 0, };
#endif


#ifdef ADVANCED_SM_CHECK
static HI_S32 SelectCard(HI_U32 CardNo)
{
	if (0 == CardNo) {
		g_pSndBuf = (HI_U8 *) t0_irdeto_zeta_send;
		g_pSndLen = t0_irdeto_zeta_send_len;
		g_u32MaxSndLen = sizeof(t0_irdeto_zeta_send[0]);
		g_u32SndTimes =
		    sizeof(t0_irdeto_zeta_send) /
		    sizeof(t0_irdeto_zeta_send[0]);

		g_pRcvBuf = (HI_U8 *) t0_irdeto_zeta_rcv;
		g_pRcvLen = t0_irdeto_zeta_rcv_len;
		g_u32MaxRcvLen = sizeof(t0_irdeto_zeta_rcv[0]);
		g_bDropWord = HI_TRUE;
	} else if (1 == CardNo) {
		g_pSndBuf = (HI_U8 *) t1_suantong_send;
		g_pSndLen = t1_suantong_send_len;
		g_u32MaxSndLen = sizeof(t1_suantong_send[0]);
		g_u32SndTimes =
		    sizeof(t1_suantong_send) / sizeof(t1_suantong_send[0]);

		g_pRcvBuf = (HI_U8 *) t1_suantong_rcv;
		g_pRcvLen = t1_suantong_rcv_len;
		g_u32MaxRcvLen = sizeof(t1_suantong_rcv[0]);
		g_bDropWord = HI_FALSE;
	} else if (14 == CardNo) {
		g_pSndBuf = (HI_U8 *) t14_irdeto_access_send;
		g_pSndLen = t14_irdeto_access_send_len;
		g_u32MaxSndLen = sizeof(t14_irdeto_access_send[0]);
		g_u32SndTimes =
		    sizeof(t14_irdeto_access_send) /
		    sizeof(t14_irdeto_access_send[0]);

		g_pRcvBuf = (HI_U8 *) t14_irdeto_access_rcv;
		g_pRcvLen = t14_irdeto_access_rcv_len;
		g_u32MaxRcvLen = sizeof(t14_irdeto_access_rcv[0]);
		g_bDropWord = HI_FALSE;
	} else {
		SW_DBG("not supported in this sample now\n");
		return HI_FAILURE;
	}

	return HI_SUCCESS;
}
#endif

static HI_S32 CardOutProcess()
{
	return HI_SUCCESS;
}

static HI_S32 CardInProcess()
{
	HI_S32 s32Ret;
	HI_UNF_SCI_STATUS_E u32SCIStatus;
	HI_S32 s32ResetTime;
	HI_U8 u8ATRCount;
	HI_U8 ATRBuf[255];
	HI_U32 i;
	char *buf2;
#ifdef ADVANCED_SM_CHECK
	HI_U32 u32TotalReadLen, u32ReadLen;
	HI_U32 u32SendLen;
	HI_U32 j;
	HI_U8 u8Result = 0;
#endif

#ifdef SW_DEBUG
	char status_msg[STATUS_MSG_MAX_LEN + 1];
#endif

	s32Ret = HI_UNF_SCI_ResetCard(g_u8SCIPort, g_u8WarmReset);
	if (HI_SUCCESS != s32Ret) {
		SW_DBG("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		return s32Ret;
	} else {
		SW_DBG("Reset Card\n");
	}

	s32ResetTime = 0;
	while (1) {
		/* will exit reset when reseting out of 2S */
		if (s32ResetTime >= 5) {
			SW_DBG("Reset Card Failure\n");
			return HI_FAILURE;
		}

		/*get SCI card status */
		s32Ret = HI_UNF_SCI_GetCardStatus(g_u8SCIPort, &u32SCIStatus);
		if (HI_SUCCESS != s32Ret) {
			SW_DBG("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__,
			       s32Ret);
			return s32Ret;
		} else {
			if (u32SCIStatus >= HI_UNF_SCI_STATUS_READY) {
				SW_DBG("Reset Card Success\n");
				break;
			} else {
				SW_DBG("Reset Card Waiting...\n");
				usleep(400000);
				s32ResetTime++;
			}
		}
	}

	/*get and print ATR message */
	s32Ret = HI_UNF_SCI_GetATR(g_u8SCIPort, ATRBuf, 255, &u8ATRCount);
	if (HI_SUCCESS != s32Ret) {
		SW_DBG("Get ATR failed. ErrorCode=0x%x\n", s32Ret);
		return s32Ret;
	}

#ifdef SW_DEBUG
	buf2 = status_msg;
	for (i = 0; i < u8ATRCount; i++) {
		buf2 += sprintf(buf2, "0x%02X, ", ATRBuf[i]);
	}

	SW_PRINT("GetATR Count:%d\n", u8ATRCount);
	SW_PRINT("ATR: %s\n", status_msg);

	buf2 = status_msg;
	for (i = 0; i < u8ATRCount; i++) {
		buf2 += sprintf(buf2, "%02X ", ATRBuf[i]);
	}

	SW_PRINT("Another format output.\n");
	SW_PRINT("ATR: %s\n", status_msg);
#endif
	if(u8ATRCount != sizeof(atr_safeview)){
		return HI_FAILURE;
	}
	if(memcmp(ATRBuf, atr_safeview, sizeof(atr_safeview)) != 0){
		return HI_FAILURE;
	}

#ifdef ADVANCED_SM_CHECK
	/* start send and receive data */
	SW_DBG("now begin send and rcv\n");
	SW_DBG("we should send %d times\n", g_u32SndTimes);
	for (j = 0; j < g_u32SndTimes; j++) {
		SW_PRINT("this is round %d\n", j);
		SW_PRINT("send sequence:");
		for (i = 0; i < g_pSndLen[j]; i++) {
			SW_PRINT("%#x ", g_pSndBuf[j * g_u32MaxSndLen + i]);
		}
		SW_PRINT("\n");

		s32Ret =
		    HI_UNF_SCI_Send(g_u8SCIPort,
				    (HI_U8 *) g_pSndBuf + j * g_u32MaxSndLen,
				    g_pSndLen[j], &u32SendLen, 5000);
		if ((HI_SUCCESS != s32Ret) || (g_pSndLen[j] != u32SendLen)) {
			SW_ERR("HI_UNF_SCI_Send return %d, SendLen:%d\n",
			       s32Ret, u32SendLen);
			return s32Ret;
		}

		u32TotalReadLen = 0;
		while (u32TotalReadLen < g_pRcvLen[j]) {
			s32Ret =
			    HI_UNF_SCI_Receive(g_u8SCIPort,
					       (HI_U8 *) &
					       rcv_buf[u32TotalReadLen], 1,
					       &u32ReadLen, 10000);
			if (s32Ret != HI_SUCCESS) {
				SW_ERR
				    ("HI_UNF_SCI_Receive return %d, ReadLen:%d\n",
				     s32Ret, u32ReadLen);
				return s32Ret;
			}

			if ((0x60 == rcv_buf[u32TotalReadLen])
			    && (HI_TRUE == g_bDropWord)) {
				SW_DBG("drop 0x60\n");
				continue;
			}

			u32TotalReadLen++;
		}

		SW_PRINT("expect rcv sequence:");
		for (i = 0; i < g_pRcvLen[j]; i++) {
			SW_PRINT("%#x ", g_pRcvBuf[j * g_u32MaxRcvLen + i]);
		}

		SW_PRINT("\nactual rcv sequence:");
		for (i = 0; i < g_pRcvLen[j]; i++) {
			if ((0x60 == rcv_buf[i]) && (HI_TRUE == g_bDropWord)) {
				SW_DBG("drop 0x60\n");
				continue;
			}

			SW_PRINT("%#x ", rcv_buf[i]);
			if ((0 == u8Result)
			    && (rcv_buf[i] !=
				g_pRcvBuf[j * g_u32MaxRcvLen + i])) {
				u8Result = 1;
			}
		}

		SW_PRINT("\n");

		if (1 == u8Result) {
			SW_DBG("ERROR\n");
		} else {
			SW_DBG("OK\n");
		}

		u8Result = 0;
	}
#endif

	return HI_SUCCESS;
}

static HI_S32 CheckCa(HI_U8 * pAction)
{
	HI_S32 s32Ret;
	HI_UNF_SCI_STATUS_E enStatus;
	HI_BOOL bStatus;

	s32Ret = HI_UNF_SCI_GetCardStatus(g_u8SCIPort, &enStatus);
	if (HI_SUCCESS != s32Ret) {
		return HI_FAILURE;
	}

	if (enStatus <= HI_UNF_SCI_STATUS_NOCARD) {
		bStatus = HI_FALSE;	/*no card  */
	} else {
		bStatus = HI_TRUE;	/*have card */
	}

	/* if bStatus is ture indicated the card have been pull out or push in */
	if (g_bCardStatus != bStatus) {
		g_bCardStatus = bStatus;
		if (HI_TRUE == bStatus) {
			*pAction = ACTION_CARDIN;	/*card in  */
		} else {
			*pAction = ACTION_CARDOUT;	/*card out */
		}
	} else {
		*pAction = ACTION_NONE;	/*no operation */
	}

	return HI_SUCCESS;
}

HI_VOID *CheckSci(HI_VOID * args)
{
	HI_S32 Ret;

	while (g_RunFlag1) {
		pthread_mutex_lock(&g_SciMutex);
		Ret = CheckCa(&g_CardAction);
		if (HI_SUCCESS != Ret) {
			SW_DBG("CheckCa failed\n");
		}

		pthread_mutex_unlock(&g_SciMutex);
		usleep(50 * 1000);
	}

	return (HI_VOID *) HI_SUCCESS;
}

HI_VOID *RunSci(HI_VOID * args)
{
	HI_U8 CardAction;
	HI_S32 s32Ret;

	while (g_RunFlag2) {
		pthread_mutex_lock(&g_SciMutex);
		CardAction = g_CardAction;
		pthread_mutex_unlock(&g_SciMutex);

		if (ACTION_CARDIN == CardAction) {
			SW_DBG("smart card is pluged in.\n");
			ui_update_smartcard_text(TS_NORMAL, "In");
			s32Ret = CardInProcess();
			if (s32Ret != HI_SUCCESS) {
				ui_update_smartcard_text(TS_BAD, "Fail");
			} else {
				ui_update_smartcard_text(TS_GOOD, "OK");
			}
		} else if (ACTION_CARDOUT == CardAction) {
			SW_DBG("smart card is pluged out.\n");
			ui_update_smartcard_text(TS_NORMAL, "Out");
			CardOutProcess();
		} else {
			//do nothing
		}

		usleep(50 * 1000);
	}

	return (HI_VOID *) HI_SUCCESS;
}

void close_smi(void)
{
	HI_UNF_SCI_DeactiveCard(g_u8SCIPort);
	HI_UNF_SCI_Close(g_u8SCIPort);
	HI_UNF_SCI_DeInit();
}

HI_S32 open_smi(void)
{
	HI_S32 s32Ret = HI_FAILURE;
	HI_U32 u8ProtocolType = 0;

	g_u8SCIPort = 0;
	g_u8WarmReset = 0;
	u32ClkMode = 0;
	u32Level = 0;
	u32Detect = 0;

#ifdef ADVANCED_SM_CHECK
	s32Ret = SelectCard(u8ProtocolType);
	if (HI_SUCCESS != s32Ret){
		SW_DBG("u8ProtocolType = %d\n", u8ProtocolType);
	}
#endif

	if (14 == u8ProtocolType) {
		enProtocolType = HI_UNF_SCI_PROTOCOL_T14;
		u32Freq = 6000;
	} else {
		enProtocolType = u8ProtocolType;
		//3570 frequency is too high for SafeView card.
		//u32Freq = 3570;
		u32Freq = 3000;
	}

	HI_UNF_SCI_Init();
	/*open SCI device */
	s32Ret = HI_UNF_SCI_Open(g_u8SCIPort, enProtocolType, u32Freq);
	if (HI_SUCCESS != s32Ret) {
		SW_DBG("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		HI_UNF_SCI_DeInit();
		return s32Ret;
	}

	if (0 == u32ClkMode) {
		enClkMode = HI_UNF_SCI_CLK_MODE_OD;
	} else {
		enClkMode = HI_UNF_SCI_CLK_MODE_CMOS;
	}

	/*set SCI clk mode:  0-OD 1-CMOS . it reference the hardware design */
	s32Ret = HI_UNF_SCI_ConfigClkMode(g_u8SCIPort, enClkMode);
	if (HI_SUCCESS != s32Ret) {
		SW_DBG("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		close_smi();
		return s32Ret;
	}

	if (!u32Level) {
		enSciLevel = HI_UNF_SCI_LEVEL_LOW;
	} else {
		enSciLevel = HI_UNF_SCI_LEVEL_HIGH;
	}

	/*set sci card power enable level.it reference the hardware design */
	s32Ret = HI_UNF_SCI_ConfigVccEn(g_u8SCIPort, enSciLevel);
	if (HI_SUCCESS != s32Ret) {
		SW_DBG("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		close_smi();
		return s32Ret;
	}

	if (!u32Detect) {
		enSciLevel = HI_UNF_SCI_LEVEL_LOW;
	} else {
		enSciLevel = HI_UNF_SCI_LEVEL_HIGH;
	}

	/*set sci card detect level.it reference the hardware design */
	s32Ret = HI_UNF_SCI_ConfigDetect(g_u8SCIPort, enSciLevel);
	if (HI_SUCCESS != s32Ret) {
		SW_DBG("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		close_smi();
		return s32Ret;
	}

	return HI_SUCCESS;
}
