#ifndef _SMARTCARD_H_
#define _SMARTCARD_H_

HI_VOID *CheckSci(HI_VOID * args);
HI_VOID *RunSci(HI_VOID * args);
void close_smi(void);
HI_S32 open_smi(void);

#endif
