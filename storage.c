#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <pthread.h>

#include <assert.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <net/if.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <arpa/inet.h>

#include "hi_unf_ecs.h"

#include "hi_adp.h"
#include "hi_adp_audio.h"
#include "hi_adp_hdmi.h"
#include "hi_adp_boardcfg.h"
#include "hi_adp_mpi.h"
#include "hi_adp_tuner.h"
#include "hi_adp_pvr.h"

#include "hifb.h"
#include "hi_unf_disp.h"
#include "hi_adp_data.h"
#include "hi_unf_ecs.h"

#include "dvb_his_fe.h"

#include "HA.AUDIO.MP3.decode.h"

#include "sw_debug.h"
#include "sw_fb_handle.h"
#include "client/client.h"
#include "config.h"
#include "ui.h"
#include "storage.h"
#include "des/des.h"

static int g_key_arleady_burned = 0;

int is_blank_data(unsigned char *data, unsigned int len)
{
	unsigned int i;

	for(i = 0; i < len; i++){
		if(data[i] != 0xFF){
			break;
		}
	}

	if(i >= len){
		return 1;
	}

	return 0;
}

int otp_read_device_id(unsigned char *id, unsigned int len)
{
	HI_S32 ret;

	if (len != STOR_DEVICE_ID_LEN) {
		return -1;
	}

	memset(id, 0xEE, len);

	ret = HI_SYS_Init();
	if(HI_SUCCESS != ret){
		return -2;
	}

	ret = HI_UNF_OTP_Open();
	if(HI_SUCCESS != ret){
		ret = HI_SYS_DeInit();
		return -3;
	}

	ret = HI_UNF_OTP_GetCustomerKey(id, len);
	if(HI_SUCCESS != ret){
		HI_UNF_OTP_Close();
		HI_SYS_DeInit();
		SW_ERR("Get customer key error: %d\n", ret);
		return -4;
	}

	ret = HI_UNF_OTP_Close();
	if(HI_SUCCESS != ret){
		ret = HI_SYS_DeInit();
		return -5;
	}

	ret = HI_SYS_DeInit();
	if(HI_SUCCESS != ret){
		return -6;
	}

	return 0;
}

int otp_write_device_id(unsigned char *data, unsigned int len)
{
	int i;
	unsigned char buffer[STOR_DEVICE_ID_LEN];
	HI_S32 ret;

	if (len != STOR_DEVICE_ID_LEN) {
		return -1;
	}

	ret = HI_SYS_Init();
	if(HI_SUCCESS != ret){
		return -2;
	}

	ret = HI_UNF_OTP_Open();
	if(HI_SUCCESS != ret){
		ret = HI_SYS_DeInit();
		return -3;
	}
	ret = HI_UNF_OTP_SetCustomerKey(data, len);
	if(HI_SUCCESS != ret){
		ret = HI_UNF_OTP_Close();
		ret = HI_SYS_DeInit();
		return -4;
	}
	ret = HI_UNF_OTP_Close();
	if(HI_SUCCESS != ret){
		ret = HI_SYS_DeInit();
		return -5;
	}

	ret = HI_SYS_DeInit();
	if(HI_SUCCESS != ret){
		return -6;
	}

	sleep(1);
	memset(buffer, 0xAB, STOR_DEVICE_ID_LEN);
	otp_read_device_id(buffer, STOR_DEVICE_ID_LEN);

	for (i = 0; i < STOR_DEVICE_ID_LEN; i++) {
		if (buffer[i] != data[i]) {
			break;
		}
	}
	if (i != STOR_DEVICE_ID_LEN) {

#ifdef SW_DEBUG
		SW_PRINT("OTP data is\n");
		for(i=0;i<STOR_DEVICE_ID_LEN;i++){
			SW_PRINT("%02x, ", buffer[i]);
		}
		SW_PRINT("\n");
#endif
		return -7;
	}

	return 0;
}

int otp_check_device_id(void)
{
	int i;
	unsigned char buffer[STOR_DEVICE_ID_LEN];
	int ret;

	memset(buffer, 0xAB, STOR_DEVICE_ID_LEN);
	ret = otp_read_device_id(buffer, STOR_DEVICE_ID_LEN);
	if (ret != 0) {
		return -1;
	}

#ifdef SW_DEBUG
		SW_PRINT("OTP data is\n");
		for(i=0;i<STOR_DEVICE_ID_LEN;i++){
			SW_PRINT("%02x, ", buffer[i]);
		}
		SW_PRINT("\n");
#endif

	for (i = 0; i < STOR_DEVICE_ID_LEN; i++) {
		if (buffer[i] != 0x00) {
			break;
		}
	}
	if (i == STOR_DEVICE_ID_LEN) {
		return 0;
	}

	return 1;
}

#ifdef ENABLE_HDCP_PROGRAM
int otp_check_hdcp_key(void)
{
	HI_S32 Ret;
	HI_BOOL bKeyBurnFlag;

    if(1 == g_key_arleady_burned){
        return 1;
    }

	Ret = HI_UNF_HDCP_GetKeyBurnFlag(&bKeyBurnFlag);
	if (HI_SUCCESS != Ret) {
		return -1;
	}

	if (HI_TRUE == bKeyBurnFlag) {
		return 1;
	}

	return 0;
}

#if (HDCP_KEY_MODE == HDCP_KEY_MODE_IDS_ENCRYPT)
static int get_3des_keys(unsigned char* key1, unsigned char* key2, unsigned char* key3)
{
    FILE *f;
    unsigned int len;
    unsigned char *binbuf = NULL;
    int i;

    f = fopen(DES_KEY_FILE, "rb");
    if(f==NULL){
        SW_ERR("Open file failed: %s\n", DES_KEY_FILE);
        return -1;
    }

    fseek(f, 0, SEEK_END);
    len = ftell(f);
    if(len != 24)
    {
        SW_ERR("keys file length is not correct\n");
        fclose(f);
        return -1;
    }

    fseek(f, 0, SEEK_SET);
    i = fread(key1, 1, 8, f);
    if(i != 8) {
        SW_ERR("Read file error!\n");
        fclose(f);
        return -1;
    }

    i = fread(key2, 1, 8, f);
    if(i != 8) {
        SW_ERR("Read file error!\n");
        fclose(f);
        return -1;
    }

    i = fread(key3, 1, 8, f);
    if(i != 8) {
        SW_ERR("Read file error!\n");
        fclose(f);
        return -1;
    }

    fclose(f);

    //Get real key
    for(i=0; i< 8; i++){
        key1[i] ^= 0xE1;
    }

    for(i=0; i< 8; i++){
        key2[i] ^= 0xE2;
    }

    for(i=0; i< 8; i++){
        key3[i] ^= 0xE3;
    }

    return 0;
}
#endif

static int repack_hdcp_key(unsigned char *key, unsigned int len, HI_UNF_OTP_HDCPKEY_S* otp_key)
{
    unsigned int i;
#if (HDCP_KEY_MODE == HDCP_KEY_MODE_IDS_ENCRYPT)
    unsigned char key1[8];
    unsigned char key2[8];
    unsigned char key3[8];
    unsigned char HDCP_pk_buf[280];
#endif


    SW_DBG("Repack hdcp key\n");
#if (HDCP_KEY_MODE == HDCP_KEY_MODE_HISILICON_ENCRYPT)
    /*
       HDCP Encryption Key have below structure, after it is made by Hisilicon HDCP_Key Tool.
       Each HDCP Encryption key bin file(384Bytes):
       Byte  0~ 7(8)         :HISIxxx
       Byte  7~15(8)         :V0000001
       Byte 16~48(32)        :CustomerID
       Byte 48~368(320)      :Encryption KEY(aligned by 16)
       Byte 368~383(16)      :RESERVED
       */

    SW_DBG("Hisilicon encrypt key\n");
    if (len != 384) {
		return -1;
	}
	memset(otp_key, 0, sizeof(HI_UNF_OTP_HDCPKEY_S));
	otp_key->EncryptionFlag = HI_TRUE;
    otp_key->Reserved = 0xabcd1234;      //for debug
	//otp_key.Reserved = 0x00;	//real burn
	memcpy(otp_key->key.EncryptData.u8EncryptKey, key, 384);
#elif (HDCP_KEY_MODE == HDCP_KEY_MODE_IDS_ENCRYPT)
    /*
       HDCP Encryption Key have below structure, after it is made by Hisilicon HDCP_Key Tool.
       Each HDCP Encryption key bin file(384Bytes):
       Byte  0(1)               :0
       Byte  1~5(5)             :KSV
       Byte 6~285(280)          :TripleDES Encrypted data
       */

    SW_DBG("IDS encrypt key\n");
    if (len != 286) {
		return -1;
	}

	// HDCP Device Private Key (280Byte)
    if (0 != get_3des_keys(key1, key2, key3)){
		return -1;
    }
    SW_DBG("Got des keys\n");
	for (i = 0; i < 280; i += 8) {
		TripleDES_Decryption(key + 6 + i, HDCP_pk_buf + i, key1, key2, key3);
    }

    SW_DBG("To prepare hdcp pack\n");
    memset(otp_key, 0, sizeof(HI_UNF_OTP_HDCPKEY_S));
    otp_key->EncryptionFlag = HI_FALSE; //key is not encryption!
    //otp_key->Reserved = 0xabcd1234;     //dubug, donot burn
    otp_key->Reserved = 0x00;         //real burn

    /* Add KSV */
    for (i = 0; i < 5; i ++) {
        otp_key->key.DecryptData.u8KSV[i] = key[i+1];
    }
    /* Add Device Private Key */
    for (i = 0; i < 280; i ++) {
        otp_key->key.DecryptData.u8PrivateKey[i] = HDCP_pk_buf[i];
    }

#elif (HDCP_KEY_MODE == HDCP_KEY_MODE_RAW)
    /*
       Each HDCP orignal bin file(308Bytes):
       Byte 0~4:      KSV(Key Selection Vector).
       Byte 5~7:      0x00.
       Byte 8~287:    Device Private Key, 40*7bytes.
       Byte 288~307:  Hash, 20bytes, NoUsed.
       */
    SW_DBG("Origianl raw key\n");
    if (len != 308) {
		return -1;
	}

    memset(otp_key, 0, sizeof(HI_UNF_OTP_HDCPKEY_S));
    otp_key->EncryptionFlag = HI_FALSE; //key is not encryption!
    otp_key->Reserved = 0xabcd1234;     //dubug, donot burn
    //otp_key->Reserved = 0x00;         //real burn

    /* Add KSV */
    for (i = 0; i < 5; i ++) {
        otp->key.DecryptData.u8KSV[i] = key[i];
    }
    /* Add Device Private Key */
    for (i = 0; i < 280; i ++) {
        otp_key->key.DecryptData.u8PrivateKey[i] = key[8 + i];
    }

#else
    return -1;
#endif

    SW_DBG("hdcp packed\n");
    return 0;
}

int otp_write_hdcp_key(unsigned char *key, unsigned int len)
{
    int r = -1;
    HI_S32 Ret;
    HI_UNF_OTP_HDCPKEY_S stHdcpKey;

    if( 0 != repack_hdcp_key(key, len, &stHdcpKey)){
        return -1;
    }

    HI_SYS_Init();
    HI_UNF_CIPHER_Open();

    SW_DBG("Burn HDCP Key into OTP.\n");
    Ret = HI_UNF_HDCP_SetHDCPKey(stHdcpKey);
    if (HI_SUCCESS != Ret) {
        SW_DBG("HI_UNF_HDCP_SetHDCPKey failed\n");
    } else {
        SW_DBG("HI_UNF_HDCP_SetHDCPKey Success\n");
        SW_DBG("Lock HDCP Key in OTP.\n");
        Ret = HI_UNF_HDCP_LockHDCPKeyEx();
        if (HI_SUCCESS != Ret)
        {
            printf("Lock HDCP key failed\n");
        }
        else
        {
            r = 0;
            g_key_arleady_burned = 1;
            printf("Lock HDCP key successfully\n");
        }
    }


    HI_UNF_CIPHER_Close();
    HI_SYS_DeInit();

	return r;
}
#endif

static int eeprom_read_factory_data(unsigned char *buf, unsigned int len)
{
	HI_S32 s32Ret = HI_FAILURE;
	unsigned int offset = 0;

	if (len > MAX_EEPROM_DATA_LEN) {
		return -2;
	}

	s32Ret = HI_UNF_E2PROM_Open();
	if (HI_SUCCESS != s32Ret) {
		SW_DBG("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		return -1;
	}

	/*set e2prom mode */
	s32Ret = HI_UNF_E2PROM_SetChipType(HI_UNF_E2PROM_CHIP_M24LC128);
	if (HI_SUCCESS != s32Ret) {
		SW_DBG("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		HI_UNF_E2PROM_Close();
		return -1;
	}

	/*set E2PROM i2c device address */
	s32Ret = HI_UNF_E2PROM_SetAddr(0xA0);
	if (HI_SUCCESS != s32Ret) {
		SW_DBG("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		HI_UNF_E2PROM_Close();
		return -1;
	}

	/*set E2PROM use i2c  which channel */
	s32Ret = HI_UNF_E2PROM_SetI2cNum(3);
	if (HI_SUCCESS != s32Ret) {
		SW_DBG("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		HI_UNF_E2PROM_Close();
		return -1;
	}

	/*read data from e2prom */
	s32Ret = HI_UNF_E2PROM_Read(EEPROM_START_ADDRESS, buf, len);
	if (HI_SUCCESS != s32Ret) {
		SW_DBG("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		HI_UNF_E2PROM_Close();
		return -1;
	}

	HI_UNF_E2PROM_Close();

	return 0;
}

static int eeprom_write_factory_data(unsigned char *data, unsigned int len)
{
	HI_S32 s32Ret = HI_FAILURE;
	HI_U32 u32Loop;
	char *buffer;

	if (len > MAX_EEPROM_DATA_LEN) {
		SW_ERR("Wrong eeprom data length!\n");
		return -1;
	}

	s32Ret = HI_UNF_E2PROM_Open();
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		return -1;
	}

	/*set e2prom mode */
	s32Ret = HI_UNF_E2PROM_SetChipType(HI_UNF_E2PROM_CHIP_M24LC128);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		HI_UNF_E2PROM_Close();
		return -1;
	}

	/*set E2PROM i2c device address */
	s32Ret = HI_UNF_E2PROM_SetAddr(0xA0);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		HI_UNF_E2PROM_Close();
		return -1;
	}

	/*set E2PROM use i2c  which channel */
	s32Ret = HI_UNF_E2PROM_SetI2cNum(3);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		HI_UNF_E2PROM_Close();
		return -1;
	}

	/*write data to e2prom */
	s32Ret = HI_UNF_E2PROM_Write(EEPROM_START_ADDRESS, data, len);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		HI_UNF_E2PROM_Close();
		return -1;
	}

	usleep(10000);

	/*read data from e2prom */
	buffer = malloc(len);
	if (buffer == NULL) {
		SW_ERR("malloc buffer unsuccessfully.");
		HI_UNF_E2PROM_Close();
		return -1;
	}
	s32Ret = HI_UNF_E2PROM_Read(EEPROM_START_ADDRESS, (unsigned char*)buffer, len);
	if (HI_SUCCESS != s32Ret) {
		SW_ERR("%s: %d ErrorCode=0x%x\n", __FILE__, __LINE__, s32Ret);
		HI_UNF_E2PROM_Close();
		free(buffer);
		return -1;
	}

	/*if read data compare with write data  verify equally  ,   the e2prom normal operating */
	for (u32Loop = 0; u32Loop < len; u32Loop++) {
		if (buffer[u32Loop] != data[u32Loop]) {
			SW_ERR("Error:r[%d] = %#x, w[%d] = %#x\n", u32Loop,
			       buffer[u32Loop], u32Loop, data[u32Loop]);
			HI_UNF_E2PROM_Close();
			free(buffer);
			return -1;
		}
	}

	HI_UNF_E2PROM_Close();
	free(buffer);

	return 0;
}

static int create_eeprom_factory_data(const EEPROM_CONFIG* c,  unsigned char *data)
{
	int i;
	int offset = 0;

	// Copy Now Mac Buffer
	for (i = 0; i < 6; i++) {
		data[offset++] = c->mac[i];
	}

	// Write Coustom Information
	data[offset++] = (c->customer & 0xFF);
	data[offset++] = (c->customer >> 8) & 0xFF;
	data[offset++] = (c->model & 0xFF);
	data[offset++] = (c->model >> 8) & 0xFF;

	// Device ID 16byte
	for (i = 0; i < 16; i++) {
		data[offset++] = c->device_id[i];
	}

#ifdef ENABLE_HDCP_PROGRAM
	//hdcp key number
	data[offset++] = c->hdcp_key_no & 0xFF;
	data[offset++] = (c->hdcp_key_no >> 8) & 0xFF;
	data[offset++] = (c->hdcp_key_no >> 16) & 0xFF;
	data[offset++] = (c->hdcp_key_no >> 24) & 0xFF;
#else
    offset += 4;
#endif

	//Write front panel and skin config 
	data[offset++] = (c->front_panel & 0xFF);
	data[offset++] = (c->front_panel >> 8) & 0xFF;
	data[offset++] = (c->skin & 0xFF);
	data[offset++] = (c->skin >> 8) & 0xFF;

	return offset;
}

int store_factory_data(const EEPROM_CONFIG* c) 
{
	unsigned char buffer[MAX_EEPROM_DATA_LEN];
	int len;
	int ret;

	if (c == NULL) {
		return -1;
	}

	len = create_eeprom_factory_data(c, buffer);
	if (len > MAX_EEPROM_DATA_LEN) {
		SW_ERR("Wrong eeprom data length!\n");
		return -1;
	}
	ret = eeprom_write_factory_data(buffer, len);

	return ret;
}

int load_factory_data(EEPROM_CONFIG* c)
{
    int ret;
    HI_S32 Ret;
    HI_BOOL bKeyBurnFlag;
    int offset = 0;
    unsigned char buffer[STOR_ALL_LEN];

    ret = eeprom_read_factory_data(buffer, STOR_ALL_LEN);
    if (ret != 0) {
        return -1;
    }

    memset(c, 0, sizeof(EEPROM_CONFIG));

    //get mac
    memcpy(c->mac, buffer + offset, STOR_MAC_LEN);

    offset += STOR_MAC_LEN;

    //customer & model 
    c->customer = buffer[offset] + (buffer[offset + 1] << 8);
    offset += STOR_CUSTOMER_LEN;
    c->model = buffer[offset] + (buffer[offset + 1] << 8);
    offset += STOR_MODEL_LEN;

    //Get device id

    //get device id from eeprom
    memcpy(c->device_id, buffer + offset, STOR_DEVICE_ID_LEN);

    //skip device id 
    offset += STOR_DEVICE_ID_LEN;

    if(1 == g_factory_conf.prog.anti_copy){
        ret = otp_check_device_id();
        SW_DBG("OTP check customer key, ret = %d\n", ret);
    }

#ifdef ENABLE_HDCP_PROGRAM
    //hdcp key number is stored in eeprom
    c->hdcp_key_no =
        buffer[offset] + (buffer[offset + 1] << 8) +
        (buffer[offset + 2] << 16) +
        (buffer[offset + 3] << 24);
#else
    c->hdcp_key_no = INVALID_HDCP_KEY_NO;
#endif

    //skip hdcp key no
    offset += STOR_HDCP_KEY_NO_LEN;

    //Front panel
    c->front_panel = buffer[offset] + (buffer[offset + 1] << 8);
    offset += STOR_FRONT_PANEL_LEN;

    //skin
    c->skin = buffer[offset] + (buffer[offset + 1] << 8);
    offset += STOR_SKIN_LEN;
    return 0;
}
