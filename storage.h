
#ifndef _STORAGE_H_
#define _STORAGE_H_

/******************************eeprom map***************************************
 *
 * address(byte)    length 	format              usage
 * 0~9              10      undefine            reserved area
 * 10~15            6       six hex charactors  mac
 * 16~17            2       little endian int   customer value	
 * 18~19            2       little endian int   model value	
 * 20~35            16      16 ASCII chars      device id (serial number)
 * 36-39            4       little endian int   hdcp key number 
 * 40~41            2       LE int              VFD or LED for panel (0x01=ch455,  0x02=pt6311)
 * 42~43            2       LE int              Skin for First Boot  (0x01=PM3.HD, 0x02=Conflenuce,  0x03=Bello)
********************************eeprom map**************************************/
#define EEPROM_START_ADDRESS 10	//10 bytes at the head is reserved
#define MAX_EEPROM_DATA_LEN 0x100	//(16*1024 - EEPROM_START_ADDRESS)

#define STOR_ALL_LEN 34
#define STOR_MAC_LEN 6
#define STOR_CUSTOMER_LEN 2
#define STOR_MODEL_LEN 2
#define STOR_DEVICE_ID_LEN 16
#define STOR_HDCP_KEY_NO_LEN 4
#define STOR_FRONT_PANEL_LEN 2
#define STOR_SKIN_LEN 2

#define INVALID_HDCP_KEY_NO 0xFFFFFFFE

typedef struct {
    unsigned short int customer;
    unsigned short int model;
    unsigned char mac[STOR_MAC_LEN];
    unsigned char device_id[STOR_DEVICE_ID_LEN];
    unsigned int hdcp_key_no;
    unsigned short int front_panel;
    unsigned short int skin;
}EEPROM_CONFIG;

int is_blank_data(unsigned char *data, unsigned int len);
int otp_check_device_id(void);
int otp_read_device_id(unsigned char *id, unsigned int len);
int otp_write_device_id(unsigned char *data, unsigned int len);

int otp_check_hdcp_key(void);
int otp_write_hdcp_key(unsigned char *key, unsigned int len);

int store_factory_data(const EEPROM_CONFIG* c);
int load_factory_data(EEPROM_CONFIG* c);

#endif
