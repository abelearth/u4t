/**
  @file		sw_debug.h
  @author	Jeon min-jeong (luck@sewoo.tv)
  @section	Modification-history
  - 2013-09-13 luck135    : Base ver. 
  
**/

#ifndef __SW_DEBUG_H_
#define __SW_DEBUG_H_

/*****************************************************************************/
//#define SW_DEBUG
#define SHELL_OUTPUT 
#define SW_VERSION "U4T v1.4.4"
#define ENABLE_IR_TEST
#define ENABLE_HDCP_PROGRAM
//#define DISABLE_SERIAL_TEST //For SW debug 

#ifdef ENABLE_HDCP_PROGRAM
#define HDCP_KEY_MODE_RAW                   1
#define HDCP_KEY_MODE_HISILICON_ENCRYPT     2
#define HDCP_KEY_MODE_IDS_ENCRYPT           3

#define HDCP_KEY_MODE HDCP_KEY_MODE_IDS_ENCRYPT 
#define DES_KEY_FILE "/var/SMT_XBMC/keys"
#define IMPORT_U4EE_FILE_NAME "/var/SMT_XBMC/u4ee.dump"
#endif
/*****************************************************************************/

#define FALSE	0
#define TRUE	1

#define E_SUCCESS		0
#define E_ERROR			-1
#define E_NOMEMORY		-2
#define E_INVALID		-3

#define DW 8			//White
#define DR 31			//Red
#define DG 32			//Green
#define DO 33			//Orange
#define DB 34			//Blue
#define DP 35			//Purple
#define DS 36			//Sky
#define DY 93			//Yellow

#define cprint_msg(io, color, arg...) \
	fprintf(io, "\x1b[0;%dm" "[%s:%3d] %s, " , \
			color, __FILE__,  __LINE__, __FUNCTION__); \
fprintf(io, arg ); \
fputs( "\x1b[0m\n", io); fflush(stderr);


#ifdef SW_DEBUG
#ifdef SHELL_OUTPUT 
#define SW_PRINT(fmt,args...)  fprintf(stderr,fmt, ## args )
#define SW_DBG(fmt,args...)  fprintf(stderr,"sw(pid=%d)::L_%d " fmt, getpid(),__LINE__,## args )
#define SW_LDBG(fmt,args...) fprintf(stderr,"sw(pid=%d)::[%s %d]" fmt, getpid(),__FUNCTION__,__LINE__, ## args )
#define SW_ERR(fmt,args...)  fprintf(stderr,"[ERR](pid=%d)::[%s %d] " fmt, getpid(),__FUNCTION__,__LINE__,## args )
#define SW_WARN(fmt,args...)  fprintf(stderr,"[WARN](pid=%d)::[%s %d] " fmt, getpid(),__FUNCTION__,__LINE__,## args )
#define SW_CDBG(arg...)  cprint_msg(stderr, DO, arg)
#define SW_CDEBUG( color, arg...)  cprint_msg(stderr, color, arg)
#else
#include <android/log.h>
#include <unistd.h>

#define LOG_TAG "U4TLOG"

#define SW_PRINT(fmt,args...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, fmt, ## args)
#define SW_DBG(fmt,args...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, fmt, ## args)
#define SW_LDBG(fmt,args...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, fmt, ## args)
#define SW_ERR(fmt,args...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, fmt, ## args) 
#define SW_WARN(fmt,args...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, fmt, ## args)
#define SW_CDBG( color, arg...) {}
#define SW_CDEBUG( color, arg...) {}
#endif

#else

#define SW_PRINT(fmt,args...) {}
#define SW_DBG(fmt,args...) {}
#define SW_LDBG(fmt,args...) {}
#define SW_ERR(fmt,args...) {}
#define SW_WARN(fmt,args...) {}
#define SW_CDBG( color, arg...) {}
#define SW_CDEBUG( color, arg...) {}
#endif

#endif
