
/**
  @file		sw_fb_handle.c
  @author	Jeon min-jeong (luck@sewoo.tv)
  @section	Modification-history
  - 2013-09-12 luck    : Base ver. 
  
**/

#include <stdio.h>
#include <pthread.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/in.h>
#include <linux/fb.h>

#include "hifb.h"
#include "hi_unf_disp.h"
#include "hi_adp_data.h"
#include "hi_adp_mpi.h"

#include "sw_debug.h"
#include "sw_fb_handle.h"
#include "sw_fb_surface.h"
#include "sw_fb_mm.h"

#define WORK_MEM_SIZE	(12*1024*1024) // 12Mb

//----------------------------------------------------------
int BYTES_PER_PIXEL = 4;
int SCREEN_WIDTH = 1280;
int SCREEN_HEIGHT = 720;
int LINE_LENGHT = 1280*4;
unsigned int COLOR_KEY = 0;
//----------------------------------------------------------

const char* FB_FILE = "/dev/graphics/fb2";
HI_S32	g_Fbfd = 0;
void* g_mapped_mem = NULL;
void* g_mapped_io = NULL;
unsigned long g_mapped_memlen = 0;
unsigned long g_mapped_iolen = 0;
struct fb_fix_screeninfo g_finfo;
struct fb_var_screeninfo g_vinfo;
unsigned char * g_work_mem = NULL;

static struct fb_var_screeninfo g_hifb_st_def_vinfo_32bpp =
{
    1280,    //visible resolution xres
    720, // yres
    1280, //virtual resolution xres_virtual
    720, //yres_virtual
    0, //xoffset
    0, //yoffset
    32, //bits per pixel
    0, //grey levels, 1 means black/white
    {16, 8, 0}, //fb_bitfiled red
    { 8, 8, 0}, // green
    { 0, 8, 0}, //blue
    {24, 8, 0}, // transparency
    0,  //non standard pixel format
    FB_ACTIVATE_FORCE,
    0, //height of picture in mm
    0, //width of picture in mm
    0, //acceleration flags
    -1, //pixclock
    -1, //left margin
    -1, //right margin
    -1, //upper margin
    -1, //lower margin
    -1, //hsync length
    -1, //vsync length
        //sync: FB_SYNC
        //vmod: FB_VMOD
        //reserved[6]: reserved for future use
};

static struct fb_var_screeninfo g_hifb_st_def_vinfo_16bpp =
{
    1280,    //visible resolution xres
    720, // yres
    1280, //virtual resolution xres_virtual
    720, //yres_virtual
    0, //xoffset
    0, //yoffset
    16, //bits per pixel
    0, //grey levels, 1 means black/white
    {10, 5, 0}, //fb_bitfiled red
    { 5, 5, 0}, // green
    { 0, 5, 0}, //blue
    {15, 1, 0}, // transparency
    0,  //non standard pixel format
    FB_ACTIVATE_FORCE,
    0, //height of picture in mm
    0, //width of picture in mm
    0, //acceleration flags
    -1, //pixclock
    -1, //left margin
    -1, //right margin
    -1, //upper margin
    -1, //lower margin
    -1, //hsync length
    -1, //vsync length
        //sync: FB_SYNC
        //vmod: FB_VMOD
        //reserved[6]: reserved for future use
};

static void print_vinfo(struct fb_var_screeninfo *vinfo)
{
    SW_LDBG( "Printing vinfo:\n");
    SW_LDBG("txres: %d\n", vinfo->xres);
    SW_LDBG( "tyres: %d\n", vinfo->yres);
    SW_LDBG( "txres_virtual: %d\n", vinfo->xres_virtual);
    SW_LDBG( "tyres_virtual: %d\n", vinfo->yres_virtual);
    SW_LDBG( "txoffset: %d\n", vinfo->xoffset);
    SW_LDBG( "tyoffset: %d\n", vinfo->yoffset);
    SW_LDBG( "tbits_per_pixel: %d\n", vinfo->bits_per_pixel);
    SW_LDBG( "tgrayscale: %d\n", vinfo->grayscale);
    SW_LDBG( "tnonstd: %d\n", vinfo->nonstd);
    SW_LDBG( "tactivate: %d\n", vinfo->activate);
    SW_LDBG( "theight: %d\n", vinfo->height);
    SW_LDBG( "twidth: %d\n", vinfo->width);
    SW_LDBG( "taccel_flags: %d\n", vinfo->accel_flags);
    SW_LDBG( "tpixclock: %d\n", vinfo->pixclock);
    SW_LDBG( "tleft_margin: %d\n", vinfo->left_margin);
    SW_LDBG( "tright_margin: %d\n", vinfo->right_margin);
    SW_LDBG( "tupper_margin: %d\n", vinfo->upper_margin);
    SW_LDBG( "tlower_margin: %d\n", vinfo->lower_margin);
    SW_LDBG( "thsync_len: %d\n", vinfo->hsync_len);
    SW_LDBG( "tvsync_len: %d\n", vinfo->vsync_len);
    SW_LDBG( "tsync: %d\n", vinfo->sync);
    SW_LDBG( "tvmode: %d\n", vinfo->vmode);
    SW_LDBG( "tred: %d/%d\n", vinfo->red.length, vinfo->red.offset);
    SW_LDBG( "tgreen: %d/%d\n", vinfo->green.length, vinfo->green.offset);
    SW_LDBG( "tblue: %d/%d\n", vinfo->blue.length, vinfo->blue.offset);
    SW_LDBG( "talpha: %d/%d\n", vinfo->transp.length, vinfo->transp.offset);
}

static void print_finfo(struct fb_fix_screeninfo *finfo)
{
    SW_LDBG( "Printing finfo:\n");
    SW_LDBG( "tsmem_start = %p\n", (char *)finfo->smem_start);
    SW_LDBG( "tsmem_len = %d\n", finfo->smem_len);
    SW_LDBG( "ttype = %d\n", finfo->type);
    SW_LDBG( "ttype_aux = %d\n", finfo->type_aux);
    SW_LDBG( "tvisual = %d\n", finfo->visual);
    SW_LDBG( "txpanstep = %d\n", finfo->xpanstep);
    SW_LDBG( "typanstep = %d\n", finfo->ypanstep);
    SW_LDBG( "tywrapstep = %d\n", finfo->ywrapstep);
    SW_LDBG( "tline_length = %d\n", finfo->line_length);
    SW_LDBG( "tmmio_start = %p\n", (char *)finfo->mmio_start);
    SW_LDBG( "tmmio_len = %d\n", finfo->mmio_len);
    SW_LDBG( "taccel = %d\n", finfo->accel);
}

int fb_init(int *surface_id, int nScreenWidth, int nScreenHeight, int bpp, unsigned char alpha /* 0x00 -투명 0xff-불투명*/)
{
	HIFB_ALPHA_S stAlpha;
    int ret = E_SUCCESS;
    
	g_Fbfd = open(FB_FILE, O_RDWR, 0);
    if (g_Fbfd < 0)
    {
        SW_ERR( "Unable to open %s\n", FB_FILE);
        return E_ERROR;
    }
	
	if( nScreenWidth > 0 && nScreenHeight > 0 && bpp > 0 )
	{
		if( bpp == 16 )
		{
			g_hifb_st_def_vinfo_16bpp.xres = nScreenWidth;
			g_hifb_st_def_vinfo_16bpp.yres = nScreenHeight;
		
			if (ioctl(g_Fbfd, FBIOPUT_VSCREENINFO, &g_hifb_st_def_vinfo_16bpp) < 0)
		    {
		        SW_ERR ( "Unable to set variable screeninfo!\n");
		        ret = E_ERROR;
		        goto CLOSEFD;
		    }
		}
		else
		{
			g_hifb_st_def_vinfo_16bpp.xres = nScreenWidth;
			g_hifb_st_def_vinfo_16bpp.yres = nScreenHeight;
		
			if (ioctl(g_Fbfd, FBIOPUT_VSCREENINFO, &g_hifb_st_def_vinfo_16bpp) < 0)
		    {
		        SW_ERR ( "Unable to set variable screeninfo!\n");
		        ret = E_ERROR;
		        goto CLOSEFD;
		    }
		    
		    
			g_hifb_st_def_vinfo_32bpp.xres = nScreenWidth;
			g_hifb_st_def_vinfo_32bpp.yres = nScreenHeight;
			
			if (ioctl(g_Fbfd, FBIOPUT_VSCREENINFO, &g_hifb_st_def_vinfo_32bpp) < 0)
		    {
		        SW_ERR ( "Unable to set variable screeninfo!\n");
		        ret = E_ERROR;
		        goto CLOSEFD;
		    }
		}
	}
	
    /* Get the type of video hardware */
    if (ioctl(g_Fbfd, FBIOGET_FSCREENINFO, &g_finfo) < 0)
    {
        SW_ERR ( "Couldn't get console hardware info\n");
        ret = E_ERROR;
        goto CLOSEFD;
    }

    print_finfo(&g_finfo);
    
   
    g_mapped_memlen = g_finfo.smem_len;
    g_mapped_mem = mmap(NULL, g_mapped_memlen,
                      PROT_READ | PROT_WRITE, MAP_SHARED, g_Fbfd, 0);
    if (g_mapped_mem == (char *)-1)
    {
        SW_ERR ( "Unable to memory map the video hardware\n");
        g_mapped_mem = NULL;
        ret = E_ERROR;
        goto CLOSEFD;
    }

    /* Determine the current screen depth */
    if (ioctl(g_Fbfd, FBIOGET_VSCREENINFO, &g_vinfo) < 0)
    {
        SW_ERR ( "Couldn't get console pixel format\n");
        ret = E_ERROR;
        goto UNMAP;
    }

    print_vinfo(&g_vinfo);
	
    BYTES_PER_PIXEL = g_vinfo.bits_per_pixel/8;
	SCREEN_WIDTH = g_vinfo.xres;
	SCREEN_HEIGHT = g_vinfo.yres;
	LINE_LENGHT = g_finfo.line_length;

    /* set alpha */
    stAlpha.bAlphaEnable  = HI_TRUE;
    stAlpha.bAlphaChannel = HI_FALSE;
    stAlpha.u8Alpha0 = alpha;
    stAlpha.u8Alpha1 = alpha;
    if (ioctl(g_Fbfd, FBIOPUT_ALPHA_HIFB, &stAlpha) < 0)
    {
        SW_ERR ( "Couldn't set alpha\n");
        ret = E_ERROR;
        goto UNMAP;
    }

	memset(g_mapped_mem, 0x00, g_finfo.smem_len);
	
	
	
	mm_initManagermentMemory(0, 0);	
	g_work_mem = (unsigned char*)malloc(WORK_MEM_SIZE); 
	
	mm_initManagermentMemory( (int)g_work_mem, WORK_MEM_SIZE );
	
	//primery
	// primary surface 할당한다.
	if( fb_createSurface(SCREEN_WIDTH, SCREEN_HEIGHT, surface_id) != E_SUCCESS )
	{
		SW_ERR("Primary Surface Create Failed!! \n");
		*surface_id = 0;
		return E_ERROR;
	}	
	SW_LDBG("Primary Surface id = %d \n", *surface_id );
	
	return ret;   
	
UNMAP:
	if( g_mapped_mem )
	{
	    munmap(g_mapped_mem, g_mapped_memlen);
	    g_mapped_mem = NULL;
	}
	
CLOSEFD:
	if( g_Fbfd > 0 )
	{
	    close(g_Fbfd);
	    g_Fbfd = 0;
	}
    
    return ret;    
}

int fb_deinit()
{
	if( g_mapped_mem )
	{
		munmap(g_mapped_mem, g_mapped_memlen);
		g_mapped_mem = NULL;
	}
	
	if( g_Fbfd > 0 )
	{
	    close(g_Fbfd);
	    g_Fbfd = 0;
	}
	
	return E_SUCCESS;
}

int fb_createSurface(int w, int h, int *surface_id)
{
	int sf_id= 0;
	SWSurface_t * pSWsf = NULL;
	
	if( w == 0 || h == 0 )
	{
		SW_ERR("E_ERROR  w[%d] h[%d]\n", w,h);
		return E_ERROR;
	}
	
	pSWsf = getSurfaceInfUsable(&sf_id);
	
	pSWsf->nUse = 1;
	pSWsf->nColor = 0x0;    
	pSWsf->x = 0;
	pSWsf->y = 0;
	pSWsf->w = w;
	pSWsf->h = h;
	pSWsf->size = BYTES_PER_PIXEL*w*h;
	if( mm_MemoryAllocation(pSWsf->size , &pSWsf->nMMKey) != E_SUCCESS)
	{
		pSWsf->nUse = 0;
		SW_ERR("not memory allocation! w[%d], h[%d]\n", w, h);
		
		printMemoryTable();
		
		return E_NOMEMORY;
	}
	

	//SW_LDBG("sf_id = %d \n",sf_id);
	
	*surface_id = sf_id;

	fb_clear(sf_id, 0x00);
	
	return E_SUCCESS;
}

int fb_destroySurface(int surface_id)
{
	SWSurface_t * pSWsf = NULL;
	
	pSWsf = getSurfaceInfo(surface_id);
	if( pSWsf == NULL)
	{
		SW_ERR("E_ERROR surface_id[%d]\n", surface_id);
		return E_ERROR;
	}
	
	mm_MemoryRelease(pSWsf->nMMKey);
	pSWsf->nMMKey = 0;
	pSWsf->nUse = 0;
	pSWsf->nColor = 0x0;
	pSWsf->x = 0;
	pSWsf->y = 0;
	pSWsf->w = 0;
	pSWsf->h = 0;
	pSWsf->size = 0;
	
	return E_SUCCESS;
}

int fb_flush( Rect *rc)
{
	// primary surface 의 rect 영역을 fb 영역에 반영한다.
	SWSurface_t * pSWPrimarySf = NULL;
	int i, j;
	
	pSWPrimarySf = getSurfaceInfo(MIN_VALID_SURFACE_ID);
	if( pSWPrimarySf == NULL)
	{
		SW_ERR("E_ERROR surface_id[%d]\n", MIN_VALID_SURFACE_ID);
		return E_ERROR;
	}

	if( BYTES_PER_PIXEL == 2 )
	{
		unsigned short* pfb = (unsigned short*)g_mapped_mem;
		unsigned short* pPrimary = (unsigned short*)getAllocTableAddr(pSWPrimarySf->nMMKey);
		
		pfb += (SCREEN_WIDTH*rc->y);
		pPrimary += (SCREEN_WIDTH*rc->y);
	    for (i = rc->y; i < rc->y + rc->h; i++)
	    {
	        for (j = rc->x; j < rc->x + rc->w; j++)
	        {
	            *(pfb+j) = *(pPrimary+j);
	        }
	        pfb += SCREEN_WIDTH;
	        pPrimary += SCREEN_WIDTH;
	    }
	}
	else
	{
		unsigned int* pfb = (unsigned int*)g_mapped_mem;
		unsigned int* pPrimary = (unsigned int*)getAllocTableAddr(pSWPrimarySf->nMMKey);
	    
	    pfb += (SCREEN_WIDTH*rc->y);
		pPrimary += (SCREEN_WIDTH*rc->y);
	    for (i = rc->y; i < rc->y + rc->h; i++)
	    {
	        for (j = rc->x; j < rc->x + rc->w; j++)
	        {
	            *(pfb+j) = *(pPrimary+j);
	        }
	        pfb += SCREEN_WIDTH;
	        pPrimary += SCREEN_WIDTH;
	    }
	}
	
	return E_SUCCESS;
}

int fb_clear(int surface_id, unsigned int color)
{
	
	SWSurface_t * pSWSf = NULL;
	int i, j;
	
	pSWSf = getSurfaceInfo(surface_id);
	if( pSWSf == NULL)
	{
		SW_ERR("E_ERROR surface_id[%d]\n", surface_id);
		return E_ERROR;
	}

	if( BYTES_PER_PIXEL == 2 )
	{
		unsigned short* pMM = (unsigned short*)getAllocTableAddr(pSWSf->nMMKey);
	    for (i = 0; i < pSWSf->h; i++)
	    {
	        for (j = 0; j < pSWSf->w; j++)
	        {
	            *pMM = color;
	            pMM++;
	        }
	    }
	}
	else
	{
		unsigned int* pMM = (unsigned int*)getAllocTableAddr(pSWSf->nMMKey);
	    for (i = 0; i < pSWSf->h; i++)
	    {
	        for (j = 0; j < pSWSf->w; j++)
	        {
	            *pMM = color;
	            pMM++;
	        }
	    }
	}
		
	return E_SUCCESS;
}

int fb_getSurfaceAddr(int surface_id, unsigned char **addr)
{
	SWSurface_t * pSWSf = NULL;
	int i, j;
	
	pSWSf = getSurfaceInfo(surface_id);
	if( pSWSf == NULL)
	{
		SW_ERR("E_ERROR surface_id[%d]\n", surface_id);
		return E_ERROR;
	}
	
	*addr = (unsigned char *)getAllocTableAddr(pSWSf->nMMKey);
	
	return pSWSf->w*pSWSf->h*BYTES_PER_PIXEL;
}

int fb_blit(int dst_surface_id, int src_surface_id, Rect *src_rc, int dx, int dy, int flag)
{
	SWSurface_t * pSrcSf = NULL, *pDstSf = NULL;
	int i, j;
	
	pSrcSf = getSurfaceInfo(src_surface_id);
	if( pSrcSf == NULL)
	{
		SW_ERR("E_ERROR surface_id[%d]\n", src_surface_id);
		return E_ERROR;
	}
	
	pDstSf = getSurfaceInfo(dst_surface_id);
	if( pDstSf == NULL)
	{
		SW_ERR("E_ERROR surface_id[%d]\n", dst_surface_id);
		return E_ERROR;
	}

	if( BYTES_PER_PIXEL == 2 )
	{
		unsigned short* pSrc = (unsigned short*)getAllocTableAddr(pSrcSf->nMMKey);
		unsigned short* pDst = (unsigned short*)getAllocTableAddr(pDstSf->nMMKey);
		
		pDst += ((pDstSf->w*dy)+dx);
		pSrc += ((pSrcSf->w*src_rc->y)+src_rc->x);
		
		for (i = 0; i < src_rc->h; i++)
	    {
	        for (j = 0; j < src_rc->w; j++)
	        {
	        	if( flag == 0 ||  ((*(pSrc+j))>>24&0xff) != 0x00 )
	        		*(pDst+j) = *(pSrc+j);
	        }
	        pDst += pDstSf->w;
	        pSrc += pSrcSf->w;
	    }
	}
	else
	{
		unsigned int* pSrc = (unsigned int*)getAllocTableAddr(pSrcSf->nMMKey);
		unsigned int* pDst = (unsigned int*)getAllocTableAddr(pDstSf->nMMKey);
		
		pDst += ((pDstSf->w*dy)+dx);
		pSrc += ((pSrcSf->w*src_rc->y)+src_rc->x);
		
		for (i = 0; i < src_rc->h; i++)
	    {
	        for (j = 0; j < src_rc->w; j++)
	        {
	        	if( flag == 0 ||  ((*(pSrc+j))>>24&0xff) != 0x00 )
	        		*(pDst+j) = *(pSrc+j);
	        }
	        pDst += pDstSf->w;
	        pSrc += pSrcSf->w;
	    }
	}
	
	return E_SUCCESS;	
}

int fb_setColor(int surface_id, int cr_value)
{
	SWSurface_t * pSWSf = NULL;
	int i, j;
	
	pSWSf = getSurfaceInfo(surface_id);
	if( pSWSf == NULL)
	{
		SW_ERR("E_ERROR surface_id[%d]\n", surface_id);
		return E_ERROR;
	}
	
	pSWSf->nColor = cr_value;
	
	return E_SUCCESS;
}

int fb_getColor(int surface_id, int *cr_value)
{
	SWSurface_t * pSWSf = NULL;
	int i, j;
	
	pSWSf = getSurfaceInfo(surface_id);
	if( pSWSf == NULL)
	{
		SW_ERR("E_ERROR surface_id[%d]\n", surface_id);
		return E_ERROR;
	}
	
	cr_value = pSWSf->nColor;
	return E_SUCCESS;
}

int fb_fillRect(int surface_id, int x, int y, int w, int h, int flag, unsigned int color)
{
	SWSurface_t * pSWSf = NULL;
	int i, j;
	
	pSWSf = getSurfaceInfo(surface_id);
	if( pSWSf == NULL)
	{
		SW_ERR("E_ERROR surface_id[%d]\n", surface_id);
		return E_ERROR;
	}

	if( BYTES_PER_PIXEL == 2 )
	{
		unsigned short* pMM = (unsigned short*)getAllocTableAddr(pSWSf->nMMKey);
		
	    pMM += (SCREEN_WIDTH*y);
		for (i = y; i < y + h; i++)
	    {
	        for (j = x; j < x + w; j++)
	        {
	            *(pMM+j) = color;
	        }
	        pMM += pSWSf->w;
	    }
	}
	else
	{
		unsigned int* pMM = (unsigned int*)getAllocTableAddr(pSWSf->nMMKey);
		
		pMM += (SCREEN_WIDTH*y);
		for (i = y; i < y + h; i++)
	    {
	        for (j = x; j < x + w; j++)
	        {
	            *(pMM+j) = color;
	        }
	        pMM += pSWSf->w;
	    }
	}
	
	return E_SUCCESS;	
}

/*
int fb_drawRect(int surface_id, int x, int y, int w, int h)
{
	SWSurface_t * pSWSf = NULL;
	int i, j;
	
	pSWSf = getSurfaceInfo(surface_id);
	if( pSWSf == NULL)
	{
		SW_ERR("E_ERROR surface_id[%d]\n", surface_id);
		return E_ERROR;
	}
	
	return E_SUCCESS;	
}
*/


#define LEN_CHAR_W	20
#define LEN_CHAR_H	22
int iNumSurface;
int iABCSurface;

int char_load_surface(const char *pNumFile, char *pABCFile)
{
	int nfd_num = 0, nfd_abc = 0;
	char *pSurfaceAddr = NULL;
	char szData[4];
	int   nSurfaceSize = 0;
	int	 nReadSize = 0;
	int nSumReadSize = 0;
	
	nfd_num = open(pNumFile, O_RDONLY);
	if( nfd_num <= 0 )
	{
		SW_ERR("num img open failed!! \n");
	}
	
	nfd_abc = open(pABCFile, O_RDONLY);
	if( nfd_abc <= 0 )
	{
		SW_ERR("abc img open failed!! \n");
	}
	
	if( fb_createSurface(LEN_CHAR_W*10, LEN_CHAR_H, &iNumSurface) != E_SUCCESS )
	{
		SW_ERR("create surface failed!!\n");
		return -1;
	}
	
	if( fb_createSurface(LEN_CHAR_W*(26+26+4), LEN_CHAR_H, &iABCSurface) != E_SUCCESS )
	{
		SW_ERR("create surface failed!!\n");
		return -1;
	}
	
	if( nfd_num > 0 )
	{
		lseek(nfd_num, 54, SEEK_SET);
		
		nSurfaceSize = fb_getSurfaceAddr(iNumSurface, &pSurfaceAddr );
		nSumReadSize = 0;
		while( nSumReadSize < nSurfaceSize )
		{
			nReadSize = read( nfd_num, szData, 4);	
			if( !(szData[0] == 0xff && szData[1] == 0xff && szData[2] == 0xff ))
				szData[3] = 0xff;
			memcpy(pSurfaceAddr+nSumReadSize, szData, 4);
			nSumReadSize += nReadSize;
			
			if( nReadSize < 4 )
				break;
		}
		
		
		if( nSumReadSize != nSurfaceSize )
			SW_ERR("nSumReadSize[%d] nSurfaceSize[%d]num read warning!!\n",nSumReadSize,nSurfaceSize);
		
		close(nfd_num);
	}
	
	if( nfd_abc > 0 )
	{
		lseek(nfd_abc, 54, SEEK_SET);
		
		nSurfaceSize = fb_getSurfaceAddr(iABCSurface, &pSurfaceAddr );
		nSumReadSize = 0;
		while( nSumReadSize < nSurfaceSize )
		{
			nReadSize = read( nfd_abc, szData, 4);
			if( !(szData[0] == 0xff && szData[1] == 0xff && szData[2] == 0xff ))
				szData[3] = 0xff;
			memcpy(pSurfaceAddr+nSumReadSize, szData, 4);
			nSumReadSize += nReadSize;
			
			if( nReadSize < 4 )
				break;
		}
		
		if( nSumReadSize != nSurfaceSize )
			SW_ERR("nSumReadSize[%d] nSurfaceSize[%d]num read warning!!\n",nSumReadSize,nSurfaceSize);
		
		close(nfd_abc);
	}
		
	return 0;
}

int char_draw(const char *pText, int nTextSize, int dst_surface, int x, int y)
{
	int i = 0;
	int char_offset_x = 0;
	int sel_surface_id = 0;
	Rect char_rc;
	
	for(i = 0; i < nTextSize; i++)
	{
		if( pText[i] >= '0' && pText[i] <= '9')
		{
			char_offset_x = (pText[i]-'0')*LEN_CHAR_W;
			sel_surface_id = iNumSurface;
		}
		else if( pText[i] >= 'a' && pText[i] <= 'z')
		{
			char_offset_x = (pText[i]-'a')*LEN_CHAR_W;
			sel_surface_id = iABCSurface;
		}
		else
		{
			int base_offset = 26;
			
			if( pText[i] == ':' )
				char_offset_x = (base_offset+26)*LEN_CHAR_W;
			else if ( pText[i] == '.' )
				char_offset_x = (base_offset+27)*LEN_CHAR_W;
			else if ( pText[i] == '/' )
				char_offset_x = (base_offset+28)*LEN_CHAR_W;
			else if ( pText[i] == '_' )
				char_offset_x = (base_offset+29)*LEN_CHAR_W;
			else if( pText[i] >= 'A' && pText[i] <= 'Z')
				char_offset_x = (base_offset+pText[i]-'A')*LEN_CHAR_W;
			else 
				continue;
			sel_surface_id = iABCSurface;
		}
		
		//SW_ERR("char_offset_x[%d] sel_surface_id[%d] [x+(i*LEN_CHAR_W)=%d]\n",char_offset_x,sel_surface_id,x+(i*LEN_CHAR_W));
		
		char_rc.x = char_offset_x;
		char_rc.y = 0;
		char_rc.w = LEN_CHAR_W;
		char_rc.h = LEN_CHAR_H;
		
		fb_blit(dst_surface, sel_surface_id, &char_rc, x+(i*LEN_CHAR_W), y, 1);
	}
	
	return 0;
}


int bitmap_to_surface(const char *pFileName, int surface_id, int w, int h)
{
	// 선행조건, surface가 w,h 사이즈로 생성되어 있어야 한다.
	int nfd = 0;
	char *pSurfaceAddr = NULL;
	char szData[4];
	int   nSurfaceSize = 0;
	int	 nReadSize = 0;
	int nSumReadSize = 0;
	
	
	nfd = open(pFileName, O_RDONLY);
	if( nfd <= 0 )
	{
		SW_ERR("[%s] img open failed!! \n",pFileName);
		return  -1;
	}
	
	if( nfd > 0 )
	{
		// bitmap header skip
		lseek(nfd, 54, SEEK_SET);
		
		nSurfaceSize = fb_getSurfaceAddr(surface_id, &pSurfaceAddr );
		nSumReadSize = 0;
		
		while( nSumReadSize < nSurfaceSize )
		{
			nReadSize = read( nfd, szData, 4);
			
			//if( !(szData[0] == 0xff && szData[1] == 0xff && szData[2] == 0xff ))
				szData[3] = 0xbb;
			memcpy(pSurfaceAddr+nSumReadSize, szData, 4);
			nSumReadSize += nReadSize;
			
			if( nReadSize < 4 )
				break;
		}
		
		if( nSumReadSize != nSurfaceSize )
			SW_ERR("nSumReadSize[%d] nSurfaceSize[%d]num read warning!!\n",nSumReadSize,nSurfaceSize);
			
		close(nfd);
	}
	
	return 0;
}
