
/**
  @file		sw_fb_handle.h
  @author	Jeon min-jeong (luck@sewoo.tv)
  @section	Modification-history
  - 2013-09-13 luck	: sewoo modify
**/

#ifndef __GFX_HANDLE_H_
#define __GFX_HANDLE_H_
	
	#define GET_R_PIXEL_RGB564(pixel)	(((unsigned char)((pixel>>11)& 0x1f))<<3)
	#define GET_G_PIXEL_RGB564(pixel)	(((unsigned char)((pixel>>5)& 0x3f))<<2)
	#define GET_B_PIXEL_RGB564(pixel)	(((unsigned char)((pixel)& 0x1f))<<3)
	
	extern int BYTES_PER_PIXEL;
	extern int LINE_LENGHT;
	extern int SCREEN_WIDTH;
	extern int SCREEN_HEIGHT;
	extern unsigned int COLOR_KEY;
	
	
	typedef struct _Rect
	{
		int x;
		int y;
		int w;
		int h;
	}Rect;
	
	// fb 초기화.. 초기화시 primary surface를 리턴해준다.
	int fb_init(int *surface_id, int nScreenWidth, int nScreenHeight, int bpp, unsigned char alpha /* 0x00 -투명 0xff-불투명*/); // 0 이면 따로 세팅안하고 디콜트값으로 한다.
	int fb_deinit();
	
	// 작업할 영역 생성 및  기능작업
	int fb_createSurface(int w, int h, int *surface_id);
	int fb_destroySurface(int surface_id);
	int fb_blit(int dst_surface_id, int src_surface_id, Rect *src_rc, int dx, int dy, int flag); // flag == 1이면 color key 값은 적용안함
	int fb_fillRect(int surface_id, int x, int y, int w, int h, int flag, unsigned int color);
	int fb_clear(int surface_id, unsigned int color);
	int fb_setColor(int surface_id, int cr_value);
	int fb_getColor(int surface_id, int *cr_value);
	//int fb_drawRect(int surface_id, int x, int y, int w, int h, int flag);
	
	// primary surface의 영역을 gfx영역에 반영해준다. ( 즉 화면반영)
	int fb_flush( Rect *rc);
	
	// surface의 영역을 user가 직접 draw할때  사용
	int fb_getSurfaceAddr(int surface_id, unsigned char **addr);
	
	
	// 추가함수
	int char_draw(const char *pText, int nTextSize, int dst_surface, int x, int y);
	int char_load_surface(const char *pNumFile, char *pABCFile);
	int bitmap_to_surface(const char *pFileName, int surface_id, int w, int h);
	
#endif
