
/**
  @file		sw_fb_mm.h
  @author	Jeon min-jeong (luck@sewoo.tv)
  @section	Modification-history
  - 2013-09-13 luck    : Base ver. ( surface?? ?Ҵ??? ?޸𸮸? ?????Ѵ?. )
  
**/

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include "sw_debug.h"
#include "sw_fb_mm.h"

#define MM_DBG				0
#define	MAX_MM_TABLE_CNT	256

typedef struct 
{
	int nAddr;
	int nSize;
} mm_table_t;
	
mm_table_t 	g_mallocTable[MAX_MM_TABLE_CNT];
int		g_mallocKeyOrder[MAX_MM_TABLE_CNT];	// ???۹??? ??��?? key(mm_table?? index) ��?? ????

mm_table_t 	g_freeTable[MAX_MM_TABLE_CNT];
int		g_freeKeyOrder[MAX_MM_TABLE_CNT];	// ??�� ?뷮 ????????��?? ��??

void mm_initManagermentMemory(int nADDR, int nSize)
{
	int i = 0;
	
	if( nADDR == 0 )
	{
		// ?Ҵ??? ????
		memset(&g_mallocTable, 0, sizeof(g_mallocTable));
		memset(g_mallocKeyOrder, 0, sizeof(g_mallocKeyOrder));
		
		// ?Ҵ簡???? ????
		memset(&g_freeTable, 0, sizeof(g_freeTable));
		memset(g_freeKeyOrder, 0, sizeof(g_freeKeyOrder));
	}
	
	
	if( g_freeTable[i].nAddr != 0 )
	{
		i++;
	}
	
	g_freeTable[i].nAddr = nADDR;
	g_freeTable[i].nSize = nSize;
	g_freeKeyOrder[i] = i+1;
	
	SW_LDBG("nADDR = %d, nSize=%d \n",nADDR, nSize);
}

int mm_setAllocMMTable(int nADDR, int nSize, int *nKey)
{
	/*
	 * ?Ҵ??? ?????? ??��??  -1 , ?????̸? 0
	 */
	 
	int 	i = 0, index = 0, setIndex = 0;
	int 	tmpKeyOrder[MAX_MM_TABLE_CNT];
	
	*nKey = 0;
	
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_mallocTable[i].nAddr == 0 )
		{
			g_mallocTable[i].nAddr = nADDR;
			g_mallocTable[i].nSize = nSize;
			setIndex = i;
			*nKey = setIndex+1;
			//SW_LDBG("setIndex = %d \n", setIndex);
			break;
		}
	}
	if( setIndex >= MAX_MM_TABLE_CNT)
		return -1;
		
	// g_mallocKeyOrder order ��??
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_mallocKeyOrder[i] == 0 )
		{
			g_mallocKeyOrder[i] = setIndex+1;
			break;
		}
		
		index = g_mallocKeyOrder[i]-1;
		
		if( g_mallocTable[index].nAddr > nADDR )
		{
			
			// ?ڷ? ?з��? ?Ѵ?.
			int j = 0;
			memcpy(tmpKeyOrder,  g_mallocKeyOrder, sizeof(g_mallocKeyOrder));
			
			for( j = 0; j < (MAX_MM_TABLE_CNT-i-1-1); j++)
				g_mallocKeyOrder[i+1+j] = tmpKeyOrder[i+j];
			//memcpy( &g_mallocKeyOrder[i+1], &tmpKeyOrder[i], MAX_MM_TABLE_CNT-(i+1));
			
			// index?? ?????? ???̺?�� ?ִ´?.
			g_mallocKeyOrder[i] = setIndex+1;
			
			break;
		}
	}
			
	return 0;
}

int	mm_MemoryAllocation(int nSize, int *Key)
{
	int i = 0, index = 0;
	int setFreeIndex = -1, setFreeSize = 0;
	int TotalFreeSize = 0;
	unsigned char bMemoryAlloc = FALSE;
	int tmpFreeKeyOrder[MAX_MM_TABLE_CNT];
	
	if( g_freeKeyOrder[0] == 0  )
	{
		SW_ERR("not free memory!! \n");
		return E_ERROR;
	}
		
	for( i = 0; i < MAX_MM_TABLE_CNT; i++ )
	{
		if( g_freeKeyOrder[i] == 0 )	// ???????? ??? ?־? 0 ?̸? ?? ???Ŀ??? ????.
			break;
		
		index = g_freeKeyOrder[i]-1;
		TotalFreeSize += g_freeTable[index].nSize;
		if( g_freeTable[index].nSize >= nSize )
		{
			// ?Ҵ??? ?????? ��??~
			if( mm_setAllocMMTable(g_freeTable[index].nAddr,nSize, Key) == 0 )
			{
				int j = 0;
				
				bMemoryAlloc = TRUE;
				
				if( g_freeTable[index].nSize-nSize == 0 )
				{
					g_freeTable[index].nAddr = 0;
					g_freeTable[index].nSize = 0;
					setFreeIndex = -1;
					setFreeSize = 0;
				}
				else
				{
					g_freeTable[index].nAddr = g_freeTable[index].nAddr+nSize;
					g_freeTable[index].nSize = g_freeTable[index].nSize-nSize;
					setFreeIndex = index;
					setFreeSize = g_freeTable[index].nSize;
				}
				
				//??�� g_freeKeyOrder?? ???????´?.
				g_freeKeyOrder[i] = 0;
				for( j = 0; j < (MAX_MM_TABLE_CNT-1-i-1); j++)
					g_freeKeyOrder[i+j] = g_freeKeyOrder[i+1+j];
				//memcpy(&g_freeKeyOrder[i], &g_freeKeyOrder[i+1], (MAX_MM_TABLE_CNT-1));
				
				
						
				break;
			}
			else
			{
				SW_ERR("alloc failed! TotalFreeSize[%d], nSize[%d]\n",TotalFreeSize, nSize);
				return E_ERROR;
			}
		}
	}
	
	if( bMemoryAlloc == FALSE && TotalFreeSize > nSize) 
	{
		SW_ERR("memory defragment!! TotalFreeSize[%d], nSize[%d]\n",TotalFreeSize, nSize);
		return E_ERROR;
	}
	
	if( bMemoryAlloc == FALSE )
	{
		SW_ERR("memory defragment!! TotalFreeSize[%d], nSize[%d]\n",TotalFreeSize, nSize);
		return E_ERROR;
	}
		
	// free key order ??�� ?뷮 ???????? ��??
	if( setFreeIndex > -1 )
	{
		for( i = 0; i < MAX_MM_TABLE_CNT; i++ )
		{
			if( g_freeKeyOrder[i] == 0 )
			{
				g_freeKeyOrder[i] = setFreeIndex+1;
				break;
			}
			
			index = g_freeKeyOrder[i]-1;
			if( setFreeSize > 0 && g_freeTable[index].nSize > setFreeSize )
			{
				// ?ڷ? ?и???.
				int j = 0;
				memcpy(tmpFreeKeyOrder, g_freeKeyOrder, sizeof(g_freeKeyOrder));
				
				for( j = 0; j < (MAX_MM_TABLE_CNT-(i+1)); j++)
					g_freeKeyOrder[i+1+j] = tmpFreeKeyOrder[i+j];
				//memcpy(&g_freeKeyOrder[i+1], &tmpFreeKeyOrder[i], (MAX_MM_TABLE_CNT-(i+1)));
				
				g_freeKeyOrder[i] = setFreeIndex+1;
				break;
			}
		}
	}

#if MM_DBG
	SW_DBG("--- g_mallocTable Show ----- \n");
	SW_DBG("\t\tADDR\t\tSize \n");
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_mallocTable[i].nAddr != 0 )
		{
			SW_DBG("%c\t%08d\t %08d \n", ' ', g_mallocTable[i].nAddr,g_mallocTable[i].nSize);
		}
	}
	SW_DBG("g_mallocTable Key....");
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_mallocKeyOrder[i] == 0 )	break;
		printf("[%d] ", g_mallocKeyOrder[i]-1);
	}
	printf("\n");
#endif

#if MM_DBG
	SW_DBG("--- g_freeTable Show ----- \n");
	SW_DBG("\t\tADDR\t\tSize \n");
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_freeTable[i].nAddr != 0 )
		{
			SW_DBG("%c\t%08d\t %08d \n", i==setFreeIndex ? 'f':' ', g_freeTable[i].nAddr,g_freeTable[i].nSize);
		}
	}
	SW_DBG("g_freeKeyOrder Key....");
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_freeKeyOrder[i] == 0 )	break;
		printf("[%d] ", g_freeKeyOrder[i]-1);
	}
	printf("\n");
#endif	

	return E_SUCCESS;
	
}

int mm_disableAllocMMTable(int nKey)
{
	int 	i = 0, index = 0;
	
	index = nKey-1;
	g_mallocTable[index].nAddr = 0;
	g_mallocTable[index].nSize = 0;
	
	// g_mallocKeyOrder order ��??
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_mallocKeyOrder[i] == nKey)
		{
			// ??��?? ??��?´?.
			int j = 0;
			
			for( j =0; j < (MAX_MM_TABLE_CNT-i-1-1); j++)
				g_mallocKeyOrder[i+j] = g_mallocKeyOrder[i+1+j];
			//memcpy( &g_mallocKeyOrder[i], &g_mallocKeyOrder[i+1], (MAX_MM_TABLE_CNT-(i+1)));
			g_mallocKeyOrder[MAX_MM_TABLE_CNT-1] = 0;
			break;
		}
	}

	return 0;
}	

int  mm_MemoryRelease(int nKey)
{
	int i = 0, index = 0, ndefregmentIndex=-1;
	unsigned int nReleaseAddr=0, nReleaseSize = 0;
	
	index = nKey-1;
	nReleaseAddr = g_mallocTable[index].nAddr;
	nReleaseSize = g_mallocTable[index].nSize;
	mm_disableAllocMMTable(nKey);
#if MM_DBG
	SW_LDBG("nKey[%d] nReleaseAddr = %06d, nReleaseSize = %04d \n", nKey,nReleaseAddr, nReleaseSize);
#endif	
	// g_mallocTable ???̺���?? ��??
	for( i = 0; i < MAX_MM_TABLE_CNT; i++ )
	{
		if( g_freeKeyOrder[i] == 0 ) break;
		
		index = g_freeKeyOrder[i]-1;
		
		if((g_freeTable[index].nAddr+g_freeTable[index].nSize) == nReleaseAddr) // ???κа? ??????????
		{
			if( ndefregmentIndex > -1)
			{
#if MM_DBG
			SW_LDBG("end sum.. ndefregmentIndex [%d] del\n",ndefregmentIndex);
#endif	
				// Table?? ?????ش?. ?տ??? ????????, ?ٽ? ?????? ?????̴?.
				g_freeTable[ndefregmentIndex].nAddr = 0;
				g_freeTable[ndefregmentIndex].nSize = 0;
				//ndefregmentIndex = -1;
			}
			// ??ģ??.
			g_freeTable[index].nSize += nReleaseSize;
#if MM_DBG
			SW_LDBG("end sum.. g_freeTable[%d].nSize=%d, g_freeTable[%d].nAddr\n",
					index,g_freeTable[index].nSize,index,g_freeTable[index].nAddr);
#endif					
			
			nReleaseAddr = g_freeTable[index].nAddr;
			nReleaseSize = g_freeTable[index].nSize;
			ndefregmentIndex = index;
			i = -1;	// 0 ???? ?ٽ? for??�� ???? ?????ִ? ?κ?�� ?u???ش?.
		}	
		else if( g_freeTable[index].nAddr == (nReleaseAddr+ nReleaseSize) )		// ?պκа? ??????????
		{
			if( ndefregmentIndex > -1)
			{
#if MM_DBG
			SW_LDBG("end sum.. ndefregmentIndex [%d] del\n",ndefregmentIndex);
#endif	
				// Table?? ?????ش?. ?տ??? ????????, ?ٽ? ?????? ?????̴?.
				g_freeTable[ndefregmentIndex].nAddr = 0;
				g_freeTable[ndefregmentIndex].nSize = 0;
				//ndefregmentIndex = -1;
			}
			
			// ??ģ??.
			g_freeTable[index].nAddr = nReleaseAddr;
			g_freeTable[index].nSize += nReleaseSize;
			nReleaseAddr = g_freeTable[index].nAddr;
			nReleaseSize = g_freeTable[index].nSize;
			ndefregmentIndex = index;
			i = -1;	// 0 ???? ?ٽ? for??�� ???? ?????ִ? ?κ?�� ?u???ش?.
		}
	}
	
	if( ndefregmentIndex == -1 )
	{
#if MM_DBG
			SW_LDBG("free table add .. nReleaseAddr [%d] nReleaseSize [%d]\n",nReleaseAddr,nReleaseSize);
#endif	
		// release?? ????�� ??ĥ???? ??��?? ???̺��� ?űԷ? ?ִ´?.
		mm_setFreeMMTable( nReleaseAddr, nReleaseSize);
	}
	else
	{	
		// g_freeKeyOrder?? ��???Ѵ?.
		for( i = 0; i < MAX_MM_TABLE_CNT; i++ )
		{
			index = g_freeKeyOrder[i]-1;
			if( g_freeTable[index].nAddr == 0 )
			{
				// ??��?ζ?????.
				int j = 0;
				for( j = 0; j < (MAX_MM_TABLE_CNT-i-1-1); j++)
					g_freeKeyOrder[i+j] = g_freeKeyOrder[i+1+j];
				//memcpy( &g_freeKeyOrder[i], &g_freeKeyOrder[i+1], MAX_MM_TABLE_CNT-(i+1));
				g_freeKeyOrder[MAX_MM_TABLE_CNT-1] = 0;
			}
		}
	}
	
#if MM_DBG
	SW_DBG("--- g_mallocTable Show ----- \n");
	SW_DBG("\t\tADDR\t\tSize \n");
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_mallocTable[i].nAddr != 0 )
		{
			SW_DBG("%c\t%08d\t %08d \n", ' ', g_mallocTable[i].nAddr,g_mallocTable[i].nSize);
		}
	}
	SW_DBG("g_mallocTable Key....");
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_mallocKeyOrder[i] == 0 )	break;
		printf("[%d] ", g_mallocKeyOrder[i]-1);
	}
	printf("\n");
#endif
#if MM_DBG
	SW_DBG("--- g_freeTable Show ----- \n");
	SW_DBG("\t\tADDR\t\tSize \n");
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_freeTable[i].nAddr != 0 )
		{
			SW_DBG("%c\t%08d\t %08d \n", ' ', g_freeTable[i].nAddr,g_freeTable[i].nSize);
		}
	}
	SW_DBG("g_freeKeyOrder Key....");
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_freeKeyOrder[i] == 0 )	break;
		printf("[%d] ", g_freeKeyOrder[i]-1);
	}
	printf("\n");
#endif	

	
	return E_SUCCESS;
}
	


int mm_setFreeMMTable(int nADDR, int nSize)
{
	// g_freeTable ��??, g_freeKeyOrder ��??
	/*
	 * ?Ҵ??? ?????? ??��??  -1 , ?????̸? 0
	 */
	 
	int 	i = 0, index = 0, setIndex = 0;
	int 	tmpKeyOrder[MAX_MM_TABLE_CNT];
	
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_freeTable[i].nAddr == 0 )
		{
			g_freeTable[i].nAddr = nADDR;
			g_freeTable[i].nSize = nSize;
			setIndex = i;
			break;
		}
	}
	if( setIndex >= MAX_MM_TABLE_CNT)
		return -1;
		
	// g_freeKeyOrder order ��??
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_freeKeyOrder[i] == 0 )
		{
			g_freeKeyOrder[i] = setIndex+1;
			break;
		}
		
		index = g_freeKeyOrder[i]-1;
		
		if( g_freeTable[index].nSize > nSize )
		{
			// ?ڷ? ?з��? ?Ѵ?.
			int j = 0;
			memcpy(tmpKeyOrder,  g_freeKeyOrder, sizeof(g_freeKeyOrder));
			for( j = 0; j < (MAX_MM_TABLE_CNT-i-1-1); j++)
				g_freeKeyOrder[i+1+j] = tmpKeyOrder[i+j];
			//memcpy( &g_freeKeyOrder[i+1], &tmpKeyOrder[i], MAX_MM_TABLE_CNT-(i+1));
			
			// index?? ?????? ???̺?�� ?ִ´?.
			g_freeKeyOrder[i] = setIndex+1;
			break;
		}
	}
	
	
#if 0//MM_DBG
	SW_DBG("--- g_freeTable Show ----- \n");
	SW_DBG("\t\tADDR\t\tSize \n");
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_freeTable[i].nAddr != 0 )
		{
			SW_DBG("%c\t%08d\t %08d \n", ' ', g_freeTable[i].nAddr,g_freeTable[i].nSize);
		}
	}
	SW_DBG("Key....");
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_freeKeyOrder[i] == 0 )	break;
		printf("[%d] ", g_freeKeyOrder[i]-1);
	}
	printf("\n");
#endif
	
	return 0;
}

int getAllocTableAddr(int nKey)
{
	if( nKey <= MAX_MM_TABLE_CNT && g_mallocTable[nKey-1].nAddr > 0)
		return g_mallocTable[nKey-1].nAddr;
	else
		return E_ERROR;
		
}

int printMemoryTable()
{
	int i = 0;
	SW_DBG("--- g_freeTable Show ----- \n");
	SW_DBG("\tADDR\t\tSize \n");
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_freeTable[i].nAddr != 0 )
		{
			SW_DBG("%c\t%08d\t %08d \n", ' ', g_freeTable[i].nAddr,g_freeTable[i].nSize);
		}
	}
	SW_DBG("Key....");
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_freeKeyOrder[i] == 0 )	break;
		printf("[%d] ", g_freeKeyOrder[i]-1);
	}
	SW_DBG("\n");
	
	SW_DBG("--- g_mallocTable Show ----- \n");
	SW_DBG("\tADDR\t\tSize \n");
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_mallocTable[i].nAddr != 0 )
		{
			printf("%c\t%08d\t %08d \n", ' ', g_mallocTable[i].nAddr,g_mallocTable[i].nSize);
		}
	}
	SW_DBG("g_mallocTable Key....");
	for( i = 0; i < MAX_MM_TABLE_CNT; i++)
	{
		if( g_mallocKeyOrder[i] == 0 )	break;
		printf("[%d] ", g_mallocKeyOrder[i]-1);
	}
	SW_DBG("\n");
	
	return E_SUCCESS;
}
