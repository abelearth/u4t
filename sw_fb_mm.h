
/**
  @file		sw_fb_mm.h
  @author	Jeon min-jeong (luck@sewoo.tv)
  @section	Modification-history
  - 2013-09-13 luck    : Base ver. ( surface에 할당할 메모리를 관리한다. )
  
**/

#ifndef __SW_FB_MM_H_
#define __SW_FB_MM_H_

	void mm_initManagermentMemory(int nADDR, int nSize);
	int mm_setAllocMMTable(int nADDR, int nSize, int *nKey);
	int	mm_MemoryAllocation(int nSize, int *Key);
	int mm_disableAllocMMTable(int nKey);
	int  mm_MemoryRelease(int nKey);
	int mm_setFreeMMTable(int nADDR, int nSize);
	int getAllocTableAddr(int nKey);
	int printMemoryTable();
	
#endif
