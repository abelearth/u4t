
/**
  @file		sw_fb_surface.c
  @author	Jeon min-jeong (luck@sewoo.tv)
  @section	Modification-history
  - 2013-09-13 luck    : Base ver. ( 상위단의 suface개념 정보를 관리한다. )
  
**/

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "sw_debug.h"
#include "sw_fb_surface.h"

// global variable
SWSurface_t g_SWSurface[MAX_SURFACE_CNT];


inline SWSurface_t * getSurfaceInfo(int surface_id)
{
	if( surface_id < MIN_VALID_SURFACE_ID || surface_id >= MAX_VALID_SURFACE_ID )
		return NULL;
		
	if( g_SWSurface[surface_id-MIN_VALID_SURFACE_ID].nUse != 0 )
		return &g_SWSurface[surface_id-MIN_VALID_SURFACE_ID];
	else
		return NULL;
}

 SWSurface_t * getSurfaceInfUsable(int *surface_id)
{
	int i;
	
	for( i = 0; i < MAX_SURFACE_CNT; i++)
	{
		if( g_SWSurface[i].nUse == 0 )
		{
			memset( &g_SWSurface[i], 0, sizeof(SWSurface_t));
			*surface_id = i+MIN_VALID_SURFACE_ID;
			return &g_SWSurface[i];
		}
	}
	
	surface_id = 0;
	return NULL;
}


void initSurfaceInfo()
{
	
	memset( &g_SWSurface, 0, sizeof(g_SWSurface));
	
}

