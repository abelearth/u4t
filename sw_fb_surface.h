
/**
  @file		sw_fb_surface.h
  @author	Jeon min-jeong (luck@sewoo.tv)
  @section	Modification-history
  - 2013-09-13 luck    : Base ver. ( 상위단의 suface개념 정보를 관리한다. )
  
**/

#ifndef __SW_FD_SURFACE_H_
#define __SW_FD_SURFACE_H_

	#include <pthread.h>
	
	#define MAX_SURFACE_CNT			256
	#define MIN_VALID_SURFACE_ID	3
	#define MAX_VALID_SURFACE_ID	(MIN_VALID_SURFACE_ID+MAX_SURFACE_CNT)
	
	typedef struct _SWSurface
	{
		int					nUse;
		int 				nColor;
		int					x;
		int					y;
		int					w;
		int					h;
		int					size;
		int					nStatus;
		int					nMMKey;
	} SWSurface_t;
	
	void initSurfaceInfo();
	SWSurface_t * getSurfaceInfo(int surface_id);
	SWSurface_t * getSurfaceInfUsable(int *surface_id);


#endif
