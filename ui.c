#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <linux/fb.h>

#include "hifb.h"
#include "sw_fb_handle.h"
#include "sw_debug.h"
#include "ui.h"
#include "images.h"
#include "storage.h"
#include "config.h"

#define X_CONNECTORS_MENU 80
#define X_FPANEL_MENU X_CONNECTORS_MENU
#define X_TUNER_MENU X_CONNECTORS_MENU
#define X_PROGRAM_MENU 680
#define X_FIRMWARE_MENU X_PROGRAM_MENU

#define X_CONNECTORS_ITEM X_CONNECTORS_MENU
#define X_PROGRAM_ITEM X_PROGRAM_MENU
#define X_CONNECTORS_TEXT (X_CONNECTORS_ITEM + W_ITEM + W_SPACE)
#define X_PROGRAM_TEXT (X_PROGRAM_ITEM + W_ITEM + W_SPACE)
#define X_FPANEL_ITEM X_FPANEL_MENU
#define X_FPANEL_TEXT (X_FPANEL_ITEM + W_ITEM + W_SPACE)
#define X_TUNER_ITEM X_FPANEL_ITEM
#define X_TUNER_TEXT (X_TUNER_ITEM + W_ITEM + W_SPACE)
#define X_TUNER_OPTION (X_TUNER_TEXT + W_TUNER_TEXT + W_SPACE)
#define X_VERSION X_CONNECTORS_MENU
#define X_STATUS_TEXT (X_VERSION + W_VERSION + W_SPACE)

#define X_FIRMWARE_ITEM X_FIRMWARE_MENU
#define X_FIRMWARE_TEXT (X_FIRMWARE_ITEM + W_ITEM + W_SPACE)

#define Y_STAUTS 30
#define Y_CONNECTORS_MENU (Y_STAUTS + H_ITEM + H_MENU_SPACE)
#define Y_TUNER_MENU (Y_CONNECTORS_MENU + H_CONNECTORS_MENU + H_MENU_SPACE)
#define Y_FPANNEL_MENU  (Y_TUNER_MENU + H_TUNER_MENU + H_MENU_SPACE)
#define Y_PROGRAM_MENU Y_CONNECTORS_MENU
#define Y_FIRMWARE_MENU (Y_PROGRAM_MENU + H_PROGRAM_MENU + H_MENU_SPACE)

#define W_ITEM 140
#define W_VERSION 240
#define W_CONNECTORS_TEXT 300
#define W_FPANEL_TEXT W_CONNECTORS_TEXT
#define W_TUNER_TEXT (W_TUNER_MENU - W_ITEM - W_SPACE*2 - W_ICON)
#define W_PROGRAM_TEXT 410
#define W_FIRMWARE_TEXT W_PROGRAM_TEXT
#define W_SPACE 2
#define W_ICON 50
#define W_CONNECTORS_MENU (W_ITEM + W_CONNECTORS_TEXT + W_SPACE)
#define W_PROGRAM_MENU (W_ITEM + W_SPACE + W_PROGRAM_TEXT)
#define W_FIRMWARE_MENU W_PROGRAM_MENU
#define W_FPANEL_MENU W_CONNECTORS_MENU
#define W_TUNER_MENU W_CONNECTORS_MENU
#define W_STATUS_TEXT (X_PROGRAM_MENU + W_PROGRAM_MENU - X_STATUS_TEXT)
#define W_USB ((W_CONNECTORS_TEXT - W_SPACE * 2)/3)

#define H_ITEM 44
#define H_SMART_CARD (H_ITEM*2)
#define H_SPACE 2
#define H_MENU_SPACE 16
#define H_ICON 50
#define H_CONNECTORS_MENU ((H_ITEM + H_SPACE) * 6 - H_SPACE)
#define H_PROGRAM_MENU ((H_ITEM + H_SPACE) * 7 - H_SPACE)
#define H_FIRMWARE_MENU ((H_ITEM + H_SPACE) * 2 - H_SPACE)
#define H_FPANEL_MENU (H_ITEM + H_SPACE + H_ICON)
#define H_TUNER_MENU (H_ITEM + (H_SPACE + H_ICON) * 3)

#define BG_COLOR_ERROR 0xFFFF0000
#define BG_COLOR_GOOD 0xFF008000
#define BG_COLOR_BLANK 0xffF5F5F5
#define BG_COLOR_MENU 0xFFFFD700
#define BG_COLOR_PANEL 0xFF000000
#define BG_COLOR_MENU_INACTIVE 0xFF6C6C6C

static int iPrimarySurface = 0;
static Rect rc_full = { 0, 0, 1280, 720 };

void ui_update_icon(const char *name, const Rect * r)
{
	int new_sf;
	Rect src_rc;

	if (fb_createSurface(r->w, r->h, &new_sf) < 0)
		SW_ERR("fb_createSurface failed.\n");

	bitmap_to_surface(name, new_sf, r->w, r->h);
	src_rc.x = 0;
	src_rc.y = 0;
	src_rc.w = r->w;
	src_rc.h = r->h;

	fb_blit(iPrimarySurface, new_sf, &src_rc, r->x, r->y, 0);
	fb_flush(&rc_full);
	fb_destroySurface(new_sf);
}

void
ui_update_text_button(enum TEXT_STYLE style, const char *str, const Rect * r)
{
	int new_sf;
	Rect src_rc;
	unsigned int bg_color;

	if (fb_createSurface(r->w, r->h, &new_sf) < 0)
		SW_ERR("fb_createSurface failed.\n");

	switch (style) {
	case TS_GOOD:
		bg_color = BG_COLOR_GOOD;
		break;
	case TS_BAD:
		bg_color = BG_COLOR_ERROR;
		break;
	case TS_NORMAL:
		bg_color = BG_COLOR_BLANK;
		break;
	case TS_MENU:
		bg_color = BG_COLOR_MENU;
		break;
	case TS_INVISIBLE:
		bg_color = BG_COLOR_PANEL;
		break;
	case TS_INACTIVE:
		bg_color = BG_COLOR_MENU_INACTIVE;
		break;
	default:
		bg_color = BG_COLOR_BLANK;
		break;
	}

	fb_clear(new_sf, bg_color);

	if (str != NULL) {
		char_draw(str, strlen(str), new_sf, 10, 10);
	}

	src_rc.x = 0;
	src_rc.y = 0;
	src_rc.w = r->w;
	src_rc.h = r->h;

	fb_blit(iPrimarySurface, new_sf, &src_rc, r->x, r->y, 0);
	fb_flush(&rc_full);
	fb_destroySurface(new_sf);
}

void ui_update_cm_item(enum TEXT_STYLE style, const char *str)
{
	Rect r;

	r.x = X_PROGRAM_TEXT;
	r.y = Y_PROGRAM_MENU + (H_SPACE + H_ITEM) * 1;
	r.w = W_PROGRAM_TEXT;
	r.h = H_ITEM;

	ui_update_text_button(style, str, &r);
}

void ui_update_fs_item(enum TEXT_STYLE style, const char *str)
{
	Rect r;

	r.x = X_PROGRAM_TEXT;
	r.y = Y_PROGRAM_MENU + (H_SPACE + H_ITEM) * 2;
	r.w = W_PROGRAM_TEXT;
	r.h = H_ITEM;

	ui_update_text_button(style, str, &r);
}

void ui_update_device_id_item(enum TEXT_STYLE style, const char *str)
{
	Rect r;

	r.x = X_PROGRAM_TEXT;
	r.y = Y_PROGRAM_MENU + (H_SPACE + H_ITEM) * 3;
	r.w = W_PROGRAM_TEXT;
	r.h = H_ITEM;

	ui_update_text_button(style, str, &r);
}

void ui_update_mac_item(enum TEXT_STYLE style, const char *str)
{
	Rect r;

	r.x = X_PROGRAM_TEXT;
	r.y = Y_PROGRAM_MENU + (H_SPACE + H_ITEM) * 4;
	r.w = W_PROGRAM_TEXT;
	r.h = H_ITEM;

	ui_update_text_button(style, str, &r);
}

void ui_update_hdcp_item(enum TEXT_STYLE style, const char *str)
{
	Rect r;

	r.x = X_PROGRAM_TEXT;
	r.y = Y_PROGRAM_MENU + (H_SPACE + H_ITEM) * 5;
	r.w = W_PROGRAM_TEXT;
	r.h = H_ITEM;

	ui_update_text_button(style, str, &r);
}

void ui_update_ip_text(enum TEXT_STYLE style, const char *str)
{
	Rect r;

	r.x = X_CONNECTORS_TEXT;
	r.y = Y_CONNECTORS_MENU + (H_SPACE + H_ITEM) * 1;
	r.w = W_CONNECTORS_TEXT;
	r.h = H_ITEM;

	ui_update_text_button(style, str, &r);
}

void ui_update_rs232_text(enum TEXT_STYLE style, const char *str)
{
	Rect r;

	r.x = X_CONNECTORS_TEXT;
	r.y = Y_CONNECTORS_MENU + (H_SPACE + H_ITEM) * 2;
	r.w = W_CONNECTORS_TEXT;
	r.h = H_ITEM;

	ui_update_text_button(style, str, &r);
}

void ui_update_usb0_text(enum TEXT_STYLE style, const char *str)
{
	Rect r;

	r.x = X_CONNECTORS_TEXT;
	r.y = Y_CONNECTORS_MENU + (H_SPACE + H_ITEM) * 3;
	r.w = W_USB;
	r.h = H_ITEM;

	ui_update_text_button(style, str, &r);
}

void ui_update_usb1_text(enum TEXT_STYLE style, const char *str)
{
	Rect r;

	r.x = X_CONNECTORS_TEXT + (W_USB + W_SPACE);
	r.y = Y_CONNECTORS_MENU + (H_SPACE + H_ITEM) * 3;
	r.w = W_USB;
	r.h = H_ITEM;

	ui_update_text_button(style, str, &r);
}

void ui_update_usb2_text(enum TEXT_STYLE style, const char *str)
{
	Rect r;

	r.x = X_CONNECTORS_TEXT + (W_USB + W_SPACE) * 2;
	r.y = Y_CONNECTORS_MENU + (H_SPACE + H_ITEM) * 3;
	r.w = W_USB;
	r.h = H_ITEM;

	ui_update_text_button(style, str, &r);
}

void ui_update_smartcard_text(enum TEXT_STYLE style, const char *str)
{
	Rect r;

	if (str != NULL) {
		r.x = X_CONNECTORS_TEXT;
		r.y = Y_CONNECTORS_MENU + (H_ITEM + H_SPACE) * 4;
		r.w = W_CONNECTORS_TEXT;
		r.h = H_ITEM;
		ui_update_text_button(style, str, &r);
	}
}

void ui_update_client_status(enum TEXT_STYLE style, const char *str)
{
	Rect r;

	r.x = X_STATUS_TEXT;
	r.y = Y_STAUTS;
	r.w = W_STATUS_TEXT;
	r.h = H_ITEM;

	ui_update_text_button(style, str, &r);
}

void ui_update_lnb_text(int status)
{
	Rect r;

	r.x = X_TUNER_TEXT;
	r.y = Y_TUNER_MENU + (H_SPACE + H_ITEM);
	r.w = W_TUNER_TEXT;
	r.h = H_ITEM;

	SW_LDBG("ui_update_lnb_text status = %d\n", status);
	if (status == 0) {
		//0V
		ui_update_text_button(TS_NORMAL, "0V", &r);
	} else if (status == 1) {
		//13V
		ui_update_text_button(TS_NORMAL, "13V", &r);
	} else {
		//18V
		ui_update_text_button(TS_NORMAL, "18V", &r);
	}
}

void ui_update_22k_text(int status)
{
	Rect r;

	r.x = X_TUNER_TEXT;
	r.y = Y_TUNER_MENU + (H_SPACE + H_ITEM) * 2;
	r.w = W_TUNER_TEXT;
	r.h = H_ITEM;

	if (status == 0) {
		//22KHz off
		ui_update_text_button(TS_NORMAL, "Off", &r);
	} else {
		//on
		ui_update_text_button(TS_NORMAL, "On", &r);
	}
}

void ui_update_antenna_power_text(int status)
{
	Rect r;

	r.x = X_TUNER_TEXT;
	r.y = Y_TUNER_MENU + (H_SPACE + H_ITEM);
	r.w = W_TUNER_TEXT;
	r.h = H_ITEM;

	SW_LDBG("ui_update_antenna_power_text status = %d\n", status);
	if (status == 0) {
		//0V
		ui_update_text_button(TS_NORMAL, "0V", &r);
	} else {
		//18V
		ui_update_text_button(TS_NORMAL, "5V", &r);
	}
}

void ui_update_ch_text(char *name)
{
	Rect r;

	r.x = X_TUNER_TEXT;
	r.y = Y_TUNER_MENU + (H_SPACE + H_ITEM) * 3;
	r.w = W_TUNER_TEXT;
	r.h = H_ITEM;

	ui_update_text_button(TS_NORMAL, name, &r);
}

static int foucus = 1;		//{0, 1}. 0-Front panel test, 1-Tuner test
static int key_array[7] = { 0, 0, 0, 0, 0, 0, 0 };	//All inactive

void ui_fpanel_key_off(int index)
{
	Rect r;

	r.x = X_FPANEL_ITEM + (W_ICON + W_SPACE) * index;
	r.y = Y_FPANNEL_MENU + (H_ITEM + H_SPACE);
	r.w = W_ICON;
	r.h = H_ICON;

	switch (index) {
	case 0:
		ui_update_icon(IMAGE_POWER, &r);
		break;
	case 1:
		ui_update_icon(IMAGE_VOL_PLUS, &r);
		break;
	case 2:
		ui_update_icon(IMAGE_VOL_MINUS, &r);
		break;
	case 3:
		ui_update_icon(IMAGE_CH_PLUS, &r);
		break;
	case 4:
		ui_update_icon(IMAGE_CH_MINUS, &r);
		break;
	case 5:
		ui_update_icon(IMAGE_MENU, &r);
		break;
	case 6:
		ui_update_icon(IMAGE_OK, &r);
		break;
	default:
		break;

	}
}

void ui_fpanel_key_on(int index)
{
	Rect r;

	r.x = X_FPANEL_ITEM + (W_ICON + W_SPACE) * index;
	r.y = Y_FPANNEL_MENU + (H_ITEM + H_SPACE);
	r.w = W_ICON;
	r.h = H_ICON;

	switch (index) {
	case 0:
		ui_update_icon(IMAGE_CHECK_POWER, &r);
		break;
	case 1:
		ui_update_icon(IMAGE_CHECK_VOL_PLUS, &r);
		break;
	case 2:
		ui_update_icon(IMAGE_CHECK_VOL_MINUS, &r);
		break;
	case 3:
		ui_update_icon(IMAGE_CHECK_CH_PLUS, &r);
		break;
	case 4:
		ui_update_icon(IMAGE_CHECK_CH_MINUS, &r);
		break;
	case 5:
		ui_update_icon(IMAGE_CHECK_MENU, &r);
		break;
	case 6:
		ui_update_icon(IMAGE_CHECK_OK, &r);
		break;
	default:
		break;

	}
}

void ui_switch_fpanel_key(int index)
{
	if (key_array[index] == 0) {
		key_array[index] = 1;
	} else {
		key_array[index] = 0;
	}

	if (key_array[index] == 0) {
		ui_fpanel_key_off(index);
	} else {
		ui_fpanel_key_on(index);
	}
}

void ui_update_ir_text(enum TEXT_STYLE style, const char *str)
{
	Rect r;

	if (str != NULL) {
		r.x = X_CONNECTORS_TEXT;
		r.y = Y_CONNECTORS_MENU + (H_ITEM + H_SPACE) * 5;
		r.w = W_CONNECTORS_TEXT;
		r.h = H_ITEM;
		ui_update_text_button(style, str, &r);
	}
}

int ui_get_menu_foucus(void)
{
	return foucus;
}

void ui_switch_menu_foucus(void)
{
	Rect r;
	int i;

	if (foucus == 0) {
		foucus = 1;
	} else {
		foucus = 0;
	}

	if (foucus == 0) {
		//Foucus on front panel.
		r.x = X_FPANEL_MENU;
		r.y = Y_FPANNEL_MENU;
		r.w = W_FPANEL_MENU;
		r.h = H_ITEM;
		ui_update_text_button(TS_MENU, "Front Panel Test", &r);

		r.x = X_TUNER_MENU;
		r.y = Y_TUNER_MENU;
		r.w = W_TUNER_MENU;
		r.h = H_ITEM;
		ui_update_text_button(TS_INACTIVE, "Tuner Test", &r);
	} else {
		//Update keys in front panel menu
		for (i = 0; i < 7; i++) {
			key_array[i] = 0;
			ui_fpanel_key_off(i);
		}
		//Foucus on Tuner
		r.x = X_FPANEL_MENU;
		r.y = Y_FPANNEL_MENU;
		r.w = W_FPANEL_MENU;
		r.h = H_ITEM;
		ui_update_text_button(TS_INACTIVE, "Front Panel Test", &r);

		r.x = X_TUNER_MENU;
		r.y = Y_TUNER_MENU;
		r.w = W_TUNER_MENU;
		r.h = H_ITEM;
		ui_update_text_button(TS_MENU, "Tuner Test", &r);
	}
}

void ui_update_factory_data(const EEPROM_CONFIG * c)
{
	char status_msg[STATUS_MSG_MAX_LEN + 1];
	unsigned short customer;
	unsigned short model;
	unsigned char *mac;
	unsigned char *device_id;
	unsigned int hdcp_key_no;

	customer = c->customer;
	model = c->model;
	mac = c->mac;
	device_id = c->device_id;
	hdcp_key_no = c->hdcp_key_no;

	//Customer and Model
	sprintf(status_msg, "0x%04X 0x%04X", customer, model);
	if (customer == 0xFFFF && model == 0xFFFF) {
		ui_update_cm_item(TS_NORMAL, status_msg);
	} else if (customer != g_factory_conf.prog.customer
		   || model != g_factory_conf.prog.model) {
		ui_update_cm_item(TS_BAD, status_msg);
	} else {
		ui_update_cm_item(TS_GOOD, status_msg);
	}

	//Customer and Model
	sprintf(status_msg, "0x%04X 0x%04X", c->front_panel, c->skin);
	if (c->front_panel == 0xFFFF && c->skin == 0xFFFF) {
		ui_update_fs_item(TS_NORMAL, status_msg);
	} else if ((c->front_panel != 1 && c->front_panel != 2)
		   || (c->skin != 1 && c->skin != 2 && c->skin != 3)) {
		ui_update_fs_item(TS_BAD, status_msg);
	} else {
		ui_update_fs_item(TS_GOOD, status_msg);
	}

	//mac
	sprintf(status_msg, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0],
		mac[1], mac[2], mac[3], mac[4], mac[5]);

	//IDS OUI: 00:1F:0C; SMT OUI: 00:0E:EB
	if ((mac[0] == 0x00) && (mac[1] == 0x1F) && (mac[2] == 0x0C)) {
		ui_update_mac_item(TS_GOOD, status_msg);
    } else if ((mac[0] == 0x00) && (mac[1] == 0x0E) && (mac[2] == 0xEB)) {
		ui_update_mac_item(TS_GOOD, status_msg);
	} else if (1 == is_blank_data(mac, STOR_MAC_LEN)) {
		ui_update_mac_item(TS_NORMAL, "None");
	} else {
		ui_update_mac_item(TS_BAD, status_msg);
	}

	//device id
	if (device_id != NULL) {
		//device id is from serial number.
		//serial number format is: modnameYYMMXXXXX
		if ((device_id[0] >= '0' && device_id[0] <= '9')
		    || (device_id[0] >= 'a' && device_id[0] <= 'z')
		    || (device_id[0] >= 'A' && device_id[0] <= 'Z')) {
			//Verify device id by Year, 201x.
			if (device_id[7] == '1'
			    && (device_id[8] >= '0' && device_id[8] <= '9')) {
				ui_update_device_id_item(TS_GOOD, device_id);
			} else {
				ui_update_device_id_item(TS_BAD, device_id);
			}
		} else if (1 == is_blank_data(device_id, STOR_DEVICE_ID_LEN)) {
			ui_update_device_id_item(TS_NORMAL, "None");
		} else {
			SW_DBG("Wrong device ID!!!");
			sprintf(status_msg, "0x%02X%02X%02X%02X%02X%02X...",
				device_id[0], device_id[1], device_id[2],
				device_id[3], device_id[4], device_id[5]);
			ui_update_device_id_item(TS_BAD, status_msg);
		}
	}
#ifdef ENABLE_HDCP_PROGRAM
	//hdcp key no
	if (hdcp_key_no == 0) {
		ui_update_hdcp_item(TS_NORMAL, "None");
	} else if (hdcp_key_no <= 9999999) {
		sprintf(status_msg, "%07d", hdcp_key_no);
		ui_update_hdcp_item(TS_GOOD, status_msg);
	} else {
		sprintf(status_msg, "0x%08x", hdcp_key_no);
		ui_update_hdcp_item(TS_BAD, status_msg);
	}
#endif
}

#ifdef ENABLE_HDCP_PROGRAM
void ui_update_otp(int status)
{
	Rect r;

	r.x = X_PROGRAM_TEXT;
	r.y = Y_PROGRAM_MENU + (H_SPACE + H_ITEM) * 6;
	r.w = W_PROGRAM_TEXT;
	r.h = H_ITEM;

	if (0 == status) {
		ui_update_text_button(TS_NORMAL, "None", &r);
	} else if (1 == status) {
		ui_update_text_button(TS_GOOD, "Burned", &r);
	} else {
		ui_update_text_button(TS_BAD, "Wrong", &r);
	}
}
#endif

void
ui_update_firmware_info(const char *dvb, int v_dvb, const char *sdk,
			int v_sdk, int v_chunks, int v_files)
{
	Rect r;
	char status_msg[STATUS_MSG_MAX_LEN + 1];

	r.x = X_FIRMWARE_TEXT;
	r.y = Y_FIRMWARE_MENU + (H_SPACE + H_ITEM) * 1;
	r.w = W_FIRMWARE_TEXT;
	r.h = H_ITEM;
	if (v_dvb == 0) {
		ui_update_text_button(TS_GOOD, dvb, &r);
	} else if (v_dvb > 0) {
		ui_update_text_button(TS_NORMAL, dvb, &r);
	} else {
		ui_update_text_button(TS_BAD, "None", &r);
	}

	r.y = Y_FIRMWARE_MENU + (H_SPACE + H_ITEM) * 2;
	if (v_sdk == 0) {
		ui_update_text_button(TS_GOOD, sdk, &r);
	} else if (v_dvb > 0) {
		ui_update_text_button(TS_NORMAL, sdk, &r);
	} else {
		ui_update_text_button(TS_BAD, "None", &r);
	}

	r.y = Y_FIRMWARE_MENU + (H_SPACE + H_ITEM) * 3;
	if (0 == v_chunks && 0 == v_files) {
		ui_update_text_button(TS_GOOD, "OK", &r);
	} else if (0 < v_chunks || 0 < v_files) {
		sprintf(status_msg, "0x%x, 0x%x", v_chunks, v_files);
		ui_update_text_button(TS_NORMAL, status_msg, &r);
	} else {
		ui_update_text_button(TS_BAD, "Wrong", &r);
	}
}

void ui_init(void)
{
	int new_sf = 0;
	Rect r;

	fb_init(&iPrimarySurface, 1280, 720, 32, 0xff);
	SW_LDBG("LOCK iPrimarySurface = %d\n", iPrimarySurface);

	char_load_surface(IMAGE_num, IMAGE_abc);

	new_sf = 0;

	Rect src_rc = { 0, 0, 1280, 720 };

	/* version & message */
	r.x = X_VERSION;
	r.y = Y_STAUTS;
	r.w = W_VERSION;
	r.h = H_ITEM;
	ui_update_text_button(TS_MENU, SW_VERSION, &r);

	r.x = X_STATUS_TEXT;
	r.y = Y_STAUTS;
	r.w = W_STATUS_TEXT;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "", &r);

	/*H/W menu panel */
	r.x = X_CONNECTORS_MENU;
	r.y = Y_CONNECTORS_MENU;
	r.w = W_CONNECTORS_MENU;
	r.h = H_CONNECTORS_MENU;
	ui_update_text_button(TS_INVISIBLE, NULL, &r);

	/*H/W Check Menu */
	r.x = X_CONNECTORS_MENU;
	r.y = Y_CONNECTORS_MENU;
	r.w = W_CONNECTORS_MENU;
	r.h = H_ITEM;
	ui_update_text_button(TS_MENU, "Connectors Test", &r);

	/*IP Address */
	r.x = X_CONNECTORS_ITEM;
	r.y = Y_CONNECTORS_MENU + (H_ITEM + H_SPACE) * 1;
	r.w = W_ITEM;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "IP", &r);

	ui_update_ip_text(TS_NORMAL, "None");

	/*RS232 */
	r.x = X_CONNECTORS_ITEM;
	r.y = Y_CONNECTORS_MENU + (H_ITEM + H_SPACE) * 2;
	r.w = W_ITEM;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "RS232", &r);

	r.x = X_CONNECTORS_TEXT;
	r.y = Y_CONNECTORS_MENU + (H_ITEM + H_SPACE) * 2;
	r.w = W_CONNECTORS_TEXT;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "N/A", &r);

	/*USB Status */
	r.x = X_CONNECTORS_ITEM;
	r.y = Y_CONNECTORS_MENU + (H_ITEM + H_SPACE) * 3;
	r.w = W_ITEM;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "USB", &r);

	r.x = X_CONNECTORS_TEXT;
	r.y = Y_CONNECTORS_MENU + (H_ITEM + H_SPACE) * 3;
	r.w = W_USB;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "N/A", &r);

	r.x = X_CONNECTORS_TEXT + (W_USB + W_SPACE);
	r.y = Y_CONNECTORS_MENU + (H_ITEM + H_SPACE) * 3;
	r.w = W_USB;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "N/A", &r);

	r.x = X_CONNECTORS_TEXT + (W_USB + W_SPACE) * 2;
	r.y = Y_CONNECTORS_MENU + (H_ITEM + H_SPACE) * 3;
	r.w = W_USB;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "N/A", &r);

	/*SmartCard Status */
	r.x = X_CONNECTORS_ITEM;
	r.y = Y_CONNECTORS_MENU + (H_ITEM + H_SPACE) * 4;
	r.w = W_ITEM;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "SCI", &r);

	r.x = X_CONNECTORS_TEXT;
	r.y = Y_CONNECTORS_MENU + (H_ITEM + H_SPACE) * 4;
	r.w = W_CONNECTORS_TEXT;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "N/A", &r);

	/*ir Status */
	r.x = X_CONNECTORS_ITEM;
	r.y = Y_CONNECTORS_MENU + (H_ITEM + H_SPACE) * 5;
	r.w = W_ITEM;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "IR", &r);

	r.x = X_CONNECTORS_TEXT;
	r.y = Y_CONNECTORS_MENU + (H_ITEM + H_SPACE) * 5;
	r.w = W_CONNECTORS_TEXT;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "N/A", &r);

	/* Front panel Test Menu */
	//panel
	r.x = X_FPANEL_ITEM;
	r.y = Y_FPANNEL_MENU;
	r.w = W_FPANEL_MENU;
	r.h = H_FPANEL_MENU;
	ui_update_text_button(TS_INVISIBLE, NULL, &r);

	/*Front Check Menu */
	r.x = X_FPANEL_MENU;
	r.y = Y_FPANNEL_MENU;
	r.w = W_FPANEL_MENU;
	r.h = H_ITEM;
	ui_update_text_button(TS_INACTIVE, "Front Panel Test", &r);

	/*Front panel icons */
	r.x = X_FPANEL_ITEM;
	r.y = Y_FPANNEL_MENU + (H_ITEM + H_SPACE);
	r.w = W_ICON;
	r.h = H_ICON;
	ui_update_icon(IMAGE_POWER, &r);

	r.x = X_FPANEL_ITEM + (W_ICON + W_SPACE);
	r.y = Y_FPANNEL_MENU + (H_ITEM + H_SPACE);
	r.w = W_ICON;
	r.h = H_ICON;
	ui_update_icon(IMAGE_VOL_PLUS, &r);

	r.x = X_FPANEL_ITEM + (W_ICON + W_SPACE) * 2;
	r.y = Y_FPANNEL_MENU + (H_ITEM + H_SPACE);
	r.w = W_ICON;
	r.h = H_ICON;
	ui_update_icon(IMAGE_VOL_MINUS, &r);

	r.x = X_FPANEL_ITEM + (W_ICON + W_SPACE) * 3;
	r.y = Y_FPANNEL_MENU + (H_ITEM + H_SPACE);
	r.w = W_ICON;
	r.h = H_ICON;
	ui_update_icon(IMAGE_CH_PLUS, &r);

	r.x = X_FPANEL_ITEM + (W_ICON + W_SPACE) * 4;
	r.y = Y_FPANNEL_MENU + (H_ITEM + H_SPACE);
	r.w = W_ICON;
	r.h = H_ICON;
	ui_update_icon(IMAGE_CH_MINUS, &r);

	r.x = X_FPANEL_ITEM + (W_ICON + W_SPACE) * 5;
	r.y = Y_FPANNEL_MENU + (H_ITEM + H_SPACE);
	r.w = W_ICON;
	r.h = H_ICON;
	ui_update_icon(IMAGE_MENU, &r);

	r.x = X_FPANEL_ITEM + (W_ICON + W_SPACE) * 6;
	r.y = Y_FPANNEL_MENU + (H_ITEM + H_SPACE);
	r.w = W_ICON;
	r.h = H_ICON;
	ui_update_icon(IMAGE_OK, &r);

	/* Tuner test Menu */
	//panel
	r.x = X_TUNER_ITEM;
	r.y = Y_TUNER_MENU;
	r.w = W_TUNER_MENU;
	r.h = H_TUNER_MENU;
	ui_update_text_button(TS_INVISIBLE, NULL, &r);

	/*tuner check Menu */
	r.x = X_TUNER_MENU;
	r.y = Y_TUNER_MENU;
	r.w = W_TUNER_MENU;
	r.h = H_ITEM;
	ui_update_text_button(TS_MENU, "Tuner Test", &r);	//Inactive Menu

	/*LNB Status */
	r.x = X_TUNER_ITEM;
	r.y = Y_TUNER_MENU + (H_SPACE + H_ITEM);
	r.w = W_ITEM;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "Volt", &r);

	r.x = X_TUNER_TEXT;
	r.y = Y_TUNER_MENU + (H_SPACE + H_ITEM);
	r.w = W_TUNER_TEXT;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "0V", &r);

	r.x = X_TUNER_TEXT + W_TUNER_TEXT + W_SPACE;
	r.y = Y_TUNER_MENU + (H_SPACE + H_ITEM);
	r.w = W_ICON;
	r.h = H_ICON;
	ui_update_icon(IMAGE_VOL_PLUS, &r);

	/*22Khz Status */
	if (g_factory_conf.fun.fe == DVB_S2) {
		r.x = X_TUNER_ITEM;
		r.y = Y_TUNER_MENU + (H_SPACE + H_ITEM) * 2;
		r.w = W_ITEM;
		r.h = H_ITEM;
		ui_update_text_button(TS_NORMAL, "22KHz", &r);

		r.x = X_TUNER_TEXT;
		r.y = Y_TUNER_MENU + (H_SPACE + H_ITEM) * 2;
		r.w = W_TUNER_TEXT;
		r.h = H_ITEM;
		ui_update_text_button(TS_NORMAL, "Off", &r);

		r.x = X_TUNER_TEXT + W_TUNER_TEXT + W_SPACE;
		r.y = Y_TUNER_MENU + (H_SPACE + H_ITEM) * 2;
		r.w = W_ICON;
		r.h = H_ICON;
		ui_update_icon(IMAGE_VOL_MINUS, &r);
	}

	/*channel switch */
	r.x = X_TUNER_ITEM;
	r.y = Y_TUNER_MENU + (H_SPACE + H_ITEM) * 3;
	r.w = W_ITEM;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "CH", &r);

	r.x = X_TUNER_TEXT;
	r.y = Y_TUNER_MENU + (H_SPACE + H_ITEM) * 3;
	r.w = W_TUNER_TEXT;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "NULL", &r);

	r.x = X_TUNER_TEXT + W_TUNER_TEXT + W_SPACE;
	r.y = Y_TUNER_MENU + (H_SPACE + H_ITEM) * 3;
	r.w = W_ICON;
	r.h = H_ICON;
	ui_update_icon(IMAGE_CH_PLUS, &r);

	/* Program menu */
	/* panel */
	r.x = X_PROGRAM_MENU;
	r.y = Y_PROGRAM_MENU;
	r.w = W_PROGRAM_MENU;
	r.h = H_PROGRAM_MENU;
	ui_update_text_button(TS_INVISIBLE, NULL, &r);

	/*Program Menu */
	r.x = X_PROGRAM_MENU;
	r.y = Y_PROGRAM_MENU;
	r.w = W_PROGRAM_MENU;
	r.h = H_ITEM;
	ui_update_text_button(TS_MENU, "EEPROM Program", &r);

	/* Customer and Model */
	r.x = X_PROGRAM_ITEM;
	r.y = Y_PROGRAM_MENU + H_ITEM + H_SPACE;
	r.w = W_ITEM;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "C.M.", &r);

	/* Front Panel and Skin */
	r.x = X_PROGRAM_ITEM;
	r.y = Y_PROGRAM_MENU + (H_ITEM + H_SPACE) * 2;
	r.w = W_ITEM;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "F.S.", &r);

	/* Device ID */
	r.x = X_PROGRAM_ITEM;
	r.y = Y_PROGRAM_MENU + (H_ITEM + H_SPACE) * 3;
	r.w = W_ITEM;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "ID", &r);

	/*MAC Address */
	r.x = X_PROGRAM_ITEM;
	r.y = Y_PROGRAM_MENU + (H_ITEM + H_SPACE) * 4;
	r.w = W_ITEM;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "MAC", &r);

#ifdef ENABLE_HDCP_PROGRAM
	/*HDCP Key No */
	r.x = X_PROGRAM_ITEM;
	r.y = Y_PROGRAM_MENU + (H_ITEM + H_SPACE) * 5;
	r.w = W_ITEM;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "Key No", &r);

	/* OTP status */
	r.x = X_PROGRAM_ITEM;
	r.y = Y_PROGRAM_MENU + (H_ITEM + H_SPACE) * 6;
	r.w = W_ITEM;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "OTP", &r);
#endif

	/* Firmware menu */
	/* panel */
	r.x = X_FIRMWARE_MENU;
	r.y = Y_FIRMWARE_MENU;
	r.w = W_FIRMWARE_MENU;
	r.h = H_FIRMWARE_MENU;
	ui_update_text_button(TS_INVISIBLE, NULL, &r);

	/*Firmware Menu */
	r.x = X_FIRMWARE_MENU;
	r.y = Y_FIRMWARE_MENU;
	r.w = W_FIRMWARE_MENU;
	r.h = H_ITEM;
	ui_update_text_button(TS_MENU, "Firmware", &r);

	/* DVB */
	r.x = X_FIRMWARE_ITEM;
	r.y = Y_FIRMWARE_MENU + H_ITEM + H_SPACE;
	r.w = W_ITEM;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "DVB", &r);

	/* SDK */
	r.x = X_FIRMWARE_ITEM;
	r.y = Y_FIRMWARE_MENU + (H_ITEM + H_SPACE) * 2;
	r.w = W_ITEM;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "SDK", &r);

	/* EMMC */
	r.x = X_FIRMWARE_ITEM;
	r.y = Y_FIRMWARE_MENU + (H_ITEM + H_SPACE) * 3;
	r.w = W_ITEM;
	r.h = H_ITEM;
	ui_update_text_button(TS_NORMAL, "EMMC", &r);

}
