#ifndef _UI_H_
#define _UI_H_

#include "storage.h"

#define STATUS_MSG_MAX_LEN 256

#define TEXT_STAUS
enum TEXT_STYLE {
	TS_GOOD = 0,
	TS_BAD,
	TS_NORMAL,
	TS_INACTIVE,
	TS_INVISIBLE,
	TS_MENU,
};

void ui_init(void);
int ui_get_menu_foucus(void);
void ui_update_icon(const char *name, const Rect * r);
void ui_update_text_button(enum TEXT_STYLE style, const char *str, const Rect * r);
void ui_update_cm_item(enum TEXT_STYLE style, const char *str);
void ui_update_device_id_item(enum TEXT_STYLE style, const char *str);
void ui_update_hdcp_item(enum TEXT_STYLE style, const char *str);
void ui_update_ip_text(enum TEXT_STYLE style, const char *str);
void ui_update_rs232_text(enum TEXT_STYLE style, const char *str);
void ui_update_mac_item(enum TEXT_STYLE style, const char *str);
void ui_update_usb0_text(enum TEXT_STYLE style, const char *str);
void ui_update_usb1_text(enum TEXT_STYLE style, const char *str);
void ui_update_usb2_text(enum TEXT_STYLE style, const char *str);
void ui_update_smartcard_text(enum TEXT_STYLE style, const char *str);
void ui_update_client_status(enum TEXT_STYLE style, const char *str);
void ui_update_lnb_text(int status);
void ui_update_22k_text(int status);
void ui_update_antenna_power_text(int status);
void ui_update_ch_text(char* name);
void ui_switch_menu_foucus(void);
void ui_switch_fpanel_key(int index);
void ui_update_ir_text(enum TEXT_STYLE style, const char *str);
void ui_update_factory_data(const EEPROM_CONFIG* c);
#ifdef ENABLE_HDCP_PROGRAM
void ui_update_otp(int status);
#endif
void ui_update_firmware_info(const char* dvb, int v_dvb, const char* sdk, int v_sdk, int v_chunks, int v_files);

#endif
