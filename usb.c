#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <assert.h>
#include <linux/fb.h>
#include <fcntl.h>

#include "hi_unf_ecs.h"
#include "sw_debug.h"
#include "hifb.h"
#include "sw_fb_handle.h"
#include "ui.h"

HI_VOID *FS_TaskUSB(HI_VOID * args)
{
	char disklist[][40] = {
		"/dev/block/sda1",
		"/dev/block/sda",
		"/dev/block/sdb1",
		"/dev/block/sdb",
		"/dev/block/sdc1",
		"/dev/block/sdc",
		"null",
	};
	int usb0 = 0;
	int usb1 = 0;
	int usb2 = 0;

	while (1) {
		sleep(1);

		if ((access(disklist[0], F_OK) == 0) || (access(disklist[1], F_OK) == 0)) {
			if (usb0 == 0) {
				usb0 = 1;
				ui_update_usb0_text(TS_GOOD, "OK");
			}
		} else {
			if (usb0 == 1) {
				usb0 = 0;
				ui_update_usb0_text(TS_NORMAL, "Out"); 
			}
		}

		if ((access(disklist[2], F_OK) == 0) || (access(disklist[3], F_OK) == 0)) {
			if (usb1 == 0) {
				usb1 = 1;
				ui_update_usb1_text(TS_GOOD, "OK");
			}
		} else {
			if (usb1 == 1) {
				usb1 = 0;
				ui_update_usb1_text(TS_NORMAL, "Out");
			}
		}

		if ((access(disklist[4], F_OK) == 0) || (access(disklist[5], F_OK) == 0)) {
			if (usb2 == 0) {
				usb2 = 1;
				ui_update_usb2_text(TS_GOOD, "OK");
			}
		} else {
			if (usb2 == 1) {
				usb2 = 0;
				ui_update_usb2_text(TS_NORMAL, "Out");
			}
		}
	}
}

